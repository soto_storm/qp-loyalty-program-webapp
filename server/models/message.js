'use strict';

let mongoose = require('mongoose');
let db = require('../libs/db')();

module.exports = (function() {

  let Message = new mongoose.Schema({
    retailer: {
      type : mongoose.Schema.ObjectId, ref : 'Retailer'
    },
    toManager: {
      type : mongoose.Schema.ObjectId, ref : 'Manager'
    },
    rechargePoints: {
      type: Number
    },
    status: {
      type: String,
      default: "ACTIVE"
    },
    message: String,
    subject: String,
    topic: String,
    type: String,
    answer: String,
    response: Object,
    createdAt: Date,
    updatedAt: Date
  });

  // Plugin to treat the mongo error
  Message.plugin(require('mongoose-unique-validator'));

  // Execute before each user.save() call
  Message.pre('save', function(callback) {
    let message = this;

    let now = new Date();
    message.updatedAt = now;

    if (!message.createdAt) {
      message.createdAt = now;
    }

    callback();

  });

  return mongoose.model('Message', Message);

})();
