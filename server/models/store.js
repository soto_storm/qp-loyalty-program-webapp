'use strict';

let mongoose = require('mongoose');
let db = require('../libs/db')();
let CronJob = require('cron').CronJob;
let StoreModel;

module.exports = (function () {

  let Store = new mongoose.Schema({
    storeName: {
      type: String,
      default: ''
    },
    description: String,
    cnpj: {
      type: String,
      unique: 'CNPJ já cadastrado!'
    },
    manager: {
      type: mongoose.Schema.ObjectId, ref: 'Manager'
    },
    retailer: {
      type: mongoose.Schema.ObjectId, ref: 'Retailer'
    },
    phone: {
      type: String,
      default: ''
    },
    address: Object,
    revenue: {
      type: Array,
      default: [],
      select: false
    },
    clients: {
      type: Array,
      default: []
    },
    giveDotsAmount: {
      type: Number,
      default: 0
    },
    clientsActions: {
      type: Number,
      default: 0
    },
    checkoutUniqueToken: {
      type: String,
      select: false
    },
    lastReleaseDate: Date,
    createdAt: Date,
    updatedAt: Date
  });

  // Plugin to treat the mongo error
  Store.plugin(require('mongoose-unique-validator'));
  Store.plugin(require('mongoose-deep-populate')(mongoose));

  // Execute before each user.save() call
  Store.pre('save', function (callback) {
    let store = this;

    let now = new Date();
    this.updatedAt = now;

    if (!this.createdAt) {
      this.createdAt = now;
      this.checkoutUniqueToken = makeUniqueToken();
    }

    callback();

  });

  StoreModel = mongoose.model('Store', Store);

  StoreModel.find({}).select('+checkoutUniqueToken').exec((err, stores) => {
    if (err) {
      throw err
    }

    stores.forEach((store) => {
      store.checkoutUniqueToken = makeUniqueToken();
      store.save(err => {
        if (err) throw err
      })

    })
  });

  return StoreModel;

})();

let job = new CronJob('00 00 00 * * 0-6', () => {

    StoreModel.find({}).select('+checkoutUniqueToken').exec((err, stores) => {
      if (err) {
        throw err
      }

      stores.forEach((store) => {
        store.checkoutUniqueToken = makeUniqueToken();
        store.save(err => {
          if (err) throw err
        })
      })

    });

  }, () => {
    /* This function is executed when the job stops */
  },
  true /* Start the job right now */
);

function makeUniqueToken() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 40; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


