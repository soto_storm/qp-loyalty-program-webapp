'use strict';
let mongoose = require('mongoose');
let db = require('../libs/db')();

module.exports = function () {

  let Place = new mongoose.Schema({
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Client'
    },
    store: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Store'
    },
    retailer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Retailer'
    },
    voucher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Voucher'
    },
    points: Number,
    type: String, /*UPDATE_DOTS; USE_VOUCHER*/
    createdAt: Date
  });

  // Plugin to treat the mongo error
  Place.plugin(require('mongoose-unique-validator'));

  // Execute before each user.save() call
  Place.pre('save', function (callback) {
    this.createdAt = new Date();
    callback();

  });

  return mongoose.model('Place', Place);

}();
