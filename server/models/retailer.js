'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const db = require('../libs/db')();
const salt = require('../config').salt;
let CronJob = require('cron').CronJob;
let RetailerModel;

module.exports = (function () {

  let Retailer = new mongoose.Schema({
    name: String,
    manager: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Manager'
    },
    email: {
      type: String,
      trim: true,
      // unique: 'E-mail already exists',
      match: [/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/, 'Invalid e-mail']
    },
    password: {
      type: String,
      select: false
    },
    points: {
      type: Number,
      default: 0
    },
    stores: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Store'
    }],
    rewards: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Reward'
    }],
    clients: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Client'
    }],
    createdAt: Date,
    updatedAt: Date,
    lastLogout: Date,
    lastAccess: Date,
    sendEmailStatistic: {
      quantity: Number,
      lastReset: Date
    },
    lastInteractionWithClient: {
      type: Date,
      select: false,
    },
    role: {
      type: String,
      default: 'RETAILER',
      set: function () {
        return this.role
      }
    },
  });

  // Plugin to treat the mongo error
  Retailer.plugin(require('mongoose-unique-validator', {message: 'Error, expected {PATH} to be unique.'}));

  // Execute before each Retailer.save() call
  Retailer.pre('save', function (callback) {
    var retailer = this;

    let now = new Date();
    this.updatedAt = now;

    if (!this.createdAt) {
      this.createdAt = now;
    }

    // Break out if the password hasn't changed
    if (!retailer.isModified('password')) return callback();

    // Password changed so we need to hash it
    // bcrypt.genSalt(5, function (err, salt) {
    //   if (err) return callback(err);
    //
    //   bcrypt.hash(retailer.password, salt, null, function (err, hash) {
    //     if (err) return callback(err);
    //     retailer.password = hash;
    //     callback();
    //   });
    // });

    //Use static salt because we do hash in client http://security.stackexchange.com/questions/93395/how-to-do-client-side-hashing-of-password-using-bcrypt
    bcrypt.hash(retailer.password, salt, null, function (err, hash) {
      if (err) return callback(err);
      retailer.password = hash;
      callback();
    });

  });

  Retailer.methods.comparePassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
    });
  };

  RetailerModel = mongoose.model('Retailer', Retailer);
  return RetailerModel;

})();

let job = new CronJob('00 00 00 1 * 0-6', () => {
    RetailerModel.find({}, (err, retailers) => {
      if (err) {
        throw err
      }


      retailers.forEach((retailer) => {
        let today = new Date();
        retailer.sendEmailStatistic.quantity = 0;
        retailer.sendEmailStatistic.lastReset = today;

        retailer.markModified('sendEmailStatistic');

        retailer.save(err => {
          if (err) throw err;
        })
      })
    });

  }, () => {
    /* This function is executed when the job stops */
  },
  true /* Start the job right now */
);

(function () {
  RetailerModel.find({}, (err, retailers) => {
    if (err) {
      throw err
    }

    retailers.forEach((retailer) => {
      if (!retailer.sendEmailStatistic.lastReset) {
        return
      }

      let today = new Date();
      let lastReset = new Date(retailer.sendEmailStatistic.lastReset);

      if (today.getFullYear() > lastReset.getFullYear() || today.getMonth() > lastReset.getMonth()) {
        retailer.sendEmailStatistic.quantity = 0;
        retailer.sendEmailStatistic.lastReset = today;

        retailer.markModified('sendEmailStatistic');

        retailer.save(err => {
          if (err) throw err;
        })
      }

    })
  });
}());
