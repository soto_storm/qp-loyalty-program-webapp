'use strict';

let mongoose = require('mongoose');
let db = require('../libs/db')();
const salt = require('../config').salt;
const bcrypt = require('bcrypt-nodejs');

module.exports = (function() {

  let Manager = new mongoose.Schema({
    name: String,
    email: String,
    retailers: Array,
    createdAt: Date,
    updatedAt: Date,
    lastLogout: Date,
    lastAccess: Date,
    password: {
      type: String,
      select: false
    },
    role: {
      type: String,
      default: 'MANAGER',
      set: function () {
        return this.role
      }
    },
    //brandLogo: {
    //  type: String
    //},
    //retailerName: {
    //  type: String
    //},
    //client : {
    //  type: mongoose.Schema.Types.ObjectId,
    //  ref: 'Client'
    //},
    //points: {
    //    type: Number,
    //    default: 0
    //},
    //totalPoints: {
    //    type: Number,
    //    default: 0
    //},
    //createdAt: Date,
    //updatedAt: Date
  });

  // Plugin to treat the mongo error
  Manager.plugin(require('mongoose-unique-validator'));

  // Execute before each user.save() call
  Manager.pre('save', function(callback) {
    var manager = this;

    let now = new Date();
    this.updatedAt = now;

    if (!this.createdAt) {
      this.createdAt = now;
    }

    if (!manager.isModified('password')) return callback();

    //Use static salt because we do hash in client http://security.stackexchange.com/questions/93395/how-to-do-client-side-hashing-of-password-using-bcrypt
    bcrypt.hash(manager.password, salt, null, function (err, hash) {
      if (err) return callback(err);
      manager.password = hash;
      callback();
    });
  });

  Manager.methods.comparePassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
    });
  };

  return mongoose.model('Manager', Manager);

})();
