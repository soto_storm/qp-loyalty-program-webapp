'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const db = require('../libs/db')();
const salt = require('../config').salt;

module.exports = (function () {

  let Client = new mongoose.Schema({
    name: String,
    cpf: {
      type: String,
      index: true,
      trim: true,
      unique: true
    },
    birthday: Date,
    email: {
      type: String,
      trim: true,
      // unique: 'E-mail already exists',
      match: [/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/, 'Invalid e-mail']
    },
    emailConfirm: {
      type: Boolean,
      default: false
    },
    password: {
      type: String,
      select: false
    },
    pointsOfRetailers: {
      type: Object,
      select: false,
      default: {}
    },
    points: {
      type: Number,
      default: 0
    },
    pointsInVouchers: {
      type: Number,
      default: 0
    },
    storeVisitedCount: {
      type: Number,
      default: 0
    },
    role: {
      type: String,
      default: 'CLIENT',
      set: function () {
        return this.role
      }
    },
    gender: String,
    vouchers: {
      emitted: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Voucher'
      }],
      concluded: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Voucher'
      }],
      expired: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Voucher'
      }]
    },
    createdAt: Date,
    updatedAt: Date,
    lastLogout: Date,
    hasPassword: {
      type: Boolean,
      default: false
    },
    resetPasswordExpires: {
      type: Date,
      select: false
    },
    resetPasswordToken:  {
      type: String,
      select: false
    }
  }, {minimize: false});

  // Plugin to treat the mongo error
  Client.plugin(require('mongoose-unique-validator', {message: 'Error, expected {PATH} to be unique.'}));

  // Execute before each client.save() call
  Client.pre('save', function (callback) {
    var client = this;

    let now = new Date();
    this.updatedAt = now;

    if (!this.createdAt) {
      this.createdAt = now;
    }

    // Break out if the password hasn't changed
    if (!client.isModified('password')) return callback();

    // Password changed so we need to hash it
    // bcrypt.genSalt(5, function (err, salt) {
    //   if (err) return callback(err);

    //   bcrypt.hash(client.password, salt, null, function (err, hash) {
    //     if (err) return callback(err);
    //     client.password = hash;
    //     client.hasPassword = true;
    //     callback();
    //   });
    // });

    //Use static salt because we do hash in client http://security.stackexchange.com/questions/93395/how-to-do-client-side-hashing-of-password-using-bcrypt
    bcrypt.hash(client.password, salt, null, function (err, hash) {
      if (err) return callback(err);
      client.password = hash;
      client.hasPassword = true;
      callback();
    });

  });

  Client.methods.comparePassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
    });
  };

  return mongoose.model('Client', Client);

})();
