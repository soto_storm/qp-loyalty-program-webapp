'use strict';
let Constants = require('../libs/constants.js');
let mongoose = require('mongoose');
let db = require('../libs/db')();
let Client = require('./client');
let CronJob = require('cron').CronJob;
let VoucherModel;

module.exports = function () {

  let Voucher = new mongoose.Schema({
    code: {
      type: String,
      unique: 'Código já criado.'
    },
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Client'
    },
    title: String,
    retailer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Retailer'
    },
    reward: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Reward'
    },
    status: {
      type: Number,
      default: Constants.VOUCHER_EMITTED
    },
    fromEmail: Boolean,
    points: Number,
    createdAt: Date,
    updatedAt: Date,
    expiredAt: Date
  });

  // Plugin to treat the mongo error
  Voucher.plugin(require('mongoose-unique-validator'));

  Voucher.methods.expireVoucher = function () {
    if (this.status === Constants.VOUCHER_EMITTED) {
      this.status = Constants.VOUCHER_EXPIRED;
      this.expiredAt = new Date();

      Client.findOneAndUpdate(
        {_id: this.client},
        {
          $inc: {'points': +this.points, 'pointsInVouchers': -this.points},
          $pull: {'vouchers.emitted': this._id},
          $push: {'vouchers.expired': this._id}
        },
        (err) => {
          if (err) {
            throw err
          }
          this.save()
        });
    }
  };

  // Execute before each user.save() call
  Voucher.pre('save', function (callback) {
    let voucher = this;

    let now = new Date();
    voucher.updatedAt = now;

    if (!this.createdAt) {
      voucher.createdAt = now;
    }

    callback();

  });

  VoucherModel = mongoose.model('Voucher', Voucher);

  return VoucherModel;

}();

let job = new CronJob('0 0 0-23 * * *', () => {

    const timestamp = new Date().getTime() - (30 * 24 * 60 * 60 * 1000);

    VoucherModel.find({status: Constants.VOUCHER_EMITTED}, (err, vouchers) => {

      vouchers.forEach((voucher) => {
        if (voucher.createdAt < timestamp) {
          voucher.expireVoucher()
        }
      })

    })

  }, () => {
    /* This function is executed when the job stops */
  },
  true /* Start the job right now */
);

