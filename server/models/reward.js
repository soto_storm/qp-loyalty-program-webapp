'use strict';

let mongoose = require('mongoose');
let db = require('../libs/db')();

module.exports = (function() {

  let Reward = new mongoose.Schema({
    title: String,
    points: {
      type: Number
    },
    retailer: {
       type : mongoose.Schema.ObjectId, ref : 'Retailer'
    },
    createdAt: Date,
    updatedAt: Date
    //manager: {
    //  type : mongoose.Schema.ObjectId, ref : 'Manager'
    //},
    //discount: {
    //  type: Number
    //},
  });

  // Plugin to treat the mongo error
  Reward.plugin(require('mongoose-unique-validator'));

  // Execute before each user.save() call
  Reward.pre('save', function(callback) {
    let reward = this;

    let now = new Date();
    reward.updatedAt = now;

    if (!reward.createdAt) {
      reward.createdAt = now;
    }

    callback();

  });

  return mongoose.model('Reward', Reward);

})();
