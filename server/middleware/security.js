'use strict';

var helmet = require('helmet');

module.exports = app => {

  // HTTP header security
  app.use(helmet.frameguard('deny'));
  app.use(helmet.hidePoweredBy());
  app.use(helmet.noSniff());
  app.use(helmet.xssFilter());
  app.use(helmet.ieNoOpen());

  /*app.use(csrf());
  app.use((req, res, next) => {
    var token = req.csrfToken();
    res.cookie('XSRF-TOKEN', token);
    res.locals._csrf = token;
    next();
  });*/

};
