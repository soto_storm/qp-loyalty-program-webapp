'use strict';

const debug = require('debug')('qp:middleware');
const logger = require('../middleware/logger.js');

// error handlers
module.exports = app => {

  // Error handling
  app.use((req, res, next) => {
    console.error(req.url);
    let err = new Error('Not Found');
    err.status = 404;
    debug(err.stackTrace);
    next(err);
  });

  app.use((err, req, res, next) => {

    err.url = req.url;

    if (!err.status) {
      err.status = 500
    }

    logger.error(err);

    debug(err);
    let error = {};

    // mongodb errors
    if (err.errors) {
      let firstItem = Object.keys(err.errors)[0];
      let errObj = err.errors[firstItem];
      error = {
        message: errObj.message,
        field: errObj.path
      };
    }

    let json = (err.errors && error) || {message: err.message || (err.length && err[0].Error)};
    res.status(err.status || 500).json(json);
  });

};

