'use strict';
const url = require('url');
const path = require('path')

module.exports = (req, res, next) => {

  let token = req.query.token;

  if (!token && req.headers.referer) {
    let referer = url.parse(req.headers.referer, true);
    token = referer.query.token;
  } else {
    if (path.extname(req.url).toString() === '.map') {
      return next()
    }
  }
 next();
 /* if (token == req.session.accessToCheckoutToken && req.session.accessToCheckoutExpiredAt > +(new Date())) {
    next();
  } else {
    res.end('don,t have permission');
  }*/

};

