'use strict';

var fs = require('fs');
var winston = require('winston');
let config = require('../config');

if (!fs.existsSync('logs')) {
  fs.mkdirSync('logs');
}

module.exports = new winston.Logger({
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: 'logs/app-' + (config.env || 'production') + '.log',
      maxsize: 1048576,
      maxFiles: 100,
      colorize: true,
    })
  ]
});
