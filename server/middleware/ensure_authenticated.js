'use strict';

const debug = require('debug')('qp:middleware:ensureAuthenticated');

function ensureAuthenticated(option){
  return (req, res, next) => {
    let ensureRole = option ? option === req.session.role : true;

    if (req.isAuthenticated() && ensureRole){
      next();
    } else {
      res.redirect('/authentication');
    }

  };
}

module.exports = ensureAuthenticated;

