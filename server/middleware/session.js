'use strict';

var session = require('express-session');
const RedisStore = require('connect-redis')(session);
const passport = require('passport');
const flash = require('connect-flash');
const sessionAge = require('../config').session.sessionAge;

require('../libs/passport')(passport);

module.exports = app => {

  // Setup Redis sesion
  app.use(session({
    secret: 'thisIsMySecret',
    cookie: {
      maxAge: sessionAge
    },
    store: new RedisStore({
      host: 'localhost',
      port: 6379,
      ttl: sessionAge
    }),
    saveUninitialized: false, // don't create session until something stored,
    resave: false // don't save session if unmodified
  }));

  // required for passport
  app.use(passport.initialize());
  app.use(passport.session()); // persistent login sessions
  app.use(flash()); // use connect-flash for flash messages stored in session

  // Verify if Redis is running
  app.use((req, res, next) => {
    if (!req.session) {
      return next(new Error('Redis is down'));
    }
    next();
  });

};
