function hideClientsCpf(clients) {
  if (!clients || clients.length < 1) {
    return clients
  }

  clients.forEach(client => {
    if (client.cpf) {
      client.cpf = '********' + client.cpf.slice(8)
    }
  });
  return clients
}

module.exports = {hideClientsCpf};
