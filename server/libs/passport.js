const LocalStrategy = require('passport-local');
const FacebookStrategy = require('passport-facebook').Strategy;
const Client = require('../models/client');
const Retailer = require('../models/retailer');
const Manager = require('../models/manager');
const configAuth = require('../config').social; // use this one for testing

module.exports = function (passport) {

  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  // used to deserialize the user
  passport.deserializeUser(function (user, done) {
    switch (user.role) {
      case 'CLIENT':
        Client.findById(user._id, function (err, client) {
          if (err) {
            done(err, null);
          }
          done(err, client);
        });
        break;
      case 'RETAILER':
        Retailer.findById(user._id, function (err, retailer) {
          if (err) {
            done(err, null);
          }
          done(err, retailer);
        });
        break;
      case 'MANAGER':
        Manager.findById(user._id, function (err, manager) {
          if (err) {
            done(err, null);
          }
          done(err, manager);
        });
    }

  });

  // =========================================================================
  // LOCAL SIGNIN FOR CLIENT==================================================
  // =========================================================================

  passport.use('local-client-login', new LocalStrategy.Strategy(
    {
      usernameField: 'cpf',
      passwordField: 'password',
      passReqToCallback: true
    },
    function (req, cpf, password, done) {
      process.nextTick(function () {
        Client.findOne({cpf: cpf}).select('+password').exec((err, client) => {
          if (err) {
            return done(err);
          }

          if (!client || !client.email) {
            return done(null, false, req.flash('loginMessage', 'WRONG_CPF'));
          }

          client.comparePassword(password, function (err, isMatch) {
            if (err) {
              return done(err);
            }
            if (!isMatch) {
              return done(null, false, req.flash('loginMessage', 'WRONG_PASSWORD'));
            }

            return done(null, client);
          });

        });
      });
    }
  ));

  // =========================================================================
  // LOCAL SIGNIN FOR RETAILER================================================
  // =========================================================================

  passport.use('local-retailer-login', new LocalStrategy.Strategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    function (req, email, password, done) {
      process.nextTick(function () {
        Retailer.findOne({email: email}).select('+password').exec((err, retailer) => {
          if (err) {
            return done(err);
          }

          if (!retailer) {
            return done(null, false, req.flash('loginMessage', 'WRONG_EMAIL'));
          }

          retailer.comparePassword(password, function (err, isMatch) {
            if (err) {
              return done(err);
            }
            if (!isMatch) {
              return done(null, false, req.flash('loginMessage', 'WRONG_PASSWORD'));
            }

            return done(null, retailer);
          });

        });
      });
    }
  ));

  // =========================================================================
  // LOCAL SIGNIN FOR MANAGER=================================================
  // =========================================================================

  passport.use('local-manager-login', new LocalStrategy.Strategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    function (req, email, password, done) {
      process.nextTick(function () {
        Manager.findOne({email: email}).select('+password').exec((err, manager) => {
          if (err) {
            return done(err);
          }

          if (!manager) {
            return done(null, false, req.flash('loginMessage', 'WRONG_EMAIL'));
          }

          manager.comparePassword(password, function (err, isMatch) {
            if (err) {
              return done(err);
            }
            if (!isMatch) {
              return done(null, false, req.flash('loginMessage', 'WRONG_PASSWORD'));
            }

            return done(null, manager);
          });

        });
      });
    }
  ));

};
