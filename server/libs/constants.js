module.exports = Object.freeze({
    VOUCHER_EMITTED: 1,
    VOUCHER_CONCLUDED: 2,
    VOUCHER_EXPIRED: 3
});
