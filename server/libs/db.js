'use strict';

var mongoose = require('mongoose');
var config = require('../config');
let singleConnection = false;

module.exports = function() {

  if (!singleConnection) {

    singleConnection = mongoose.connect(config.mongodb.uri);
    singleConnection.connection.on('error', (err) => console.error(err.message));
    singleConnection.connection.once('open', () => console.log('mongodb connection open'));

    return singleConnection;
  }

};

