'use strict';

let Retailer = require('../models/retailer');
let Voucher = require('../models/voucher');
let Reward = require('../models/reward');
let Client = require('../models/client');
let Message = require('../models/message');
let debug = require('debug')('qp:controller:retailer');
var _ = require('lodash');

module.exports = {
  createRetailer,
  getBy,
  getBestRetailers,
  updateRetailer,
  rechargeDots,
  changePassword,
  addSupportMessage,
  exchangedRewardsToVouchers,
  getLastActivity
};

function createRetailer(req, res, next) {
  let retailer = new Retailer(req.body);

  retailer.save((err, retailer) => {
    if (err) {
      return next(err);
    }

    if (req.query.signup) {

      req.logIn(retailer, function (err) {
        if (err) {
          return next(err);
        }
        req.session.user = retailer;
        req.session.role = 'retailer';

        res.json({user: retailer, redirectTo: "/retailer"})
      });

      return;
    }

    res.json(retailer);
  });
}

function getBy(req, res, next) {
  debug('query', req.query);

  var query = {};

  if (req.query.email) {
    return getByEmail(req, res, next);
  }

  if (req.query.id) {
    return getById(req, res, next);
  }

  Retailer.find(query, (err, retailer) => {
    if (err) {
      return next(err);
    }
    res.json(retailer);
  });
}

function getByEmail(req, res, next) {
  Retailer.findOne({
    email: req.query.email
  }, (err, retailer) => {
    if (err) {
      return next(err);
    }

    res.json(retailer);
  });
}

function getById(req, res, next) {
  Retailer.findOne({
    _id: req.query.id
  }, (err, retailer) => {
    if (err) {
      return next(err);
    }

    res.json(retailer);
  });
}

function getBestRetailers(req, res, next) {
  if (req.query.quantity) {
    var quantity = parseFloat(req.query.quantity);

    if (req.query.quantity.indexOf('%') !== -1) {
      var percentage = true;
    }
  }

  Retailer.find({}).exec()
    .then(retailers => {
      var responseData = _.sortBy(retailers, function (retailer) {
        return -retailer.points;
      });

      if (!isNaN(quantity)) {
        if (percentage) {
          quantity = Math.ceil(retailers.length * (quantity / 100));
        }

        if (quantity < retailers.length) {
          responseData.splice(quantity);
        }
      }

      res.json(responseData);
    })
    .catch(err => {
      next(err)
    });
}

function updateRetailer(req, res, next) {
  if (!req.params.retailerId) {
    return next(new Error('retailerId is require for updateRetailer'))
  }

  Retailer.findOne(
    {_id: req.params.retailerId},
    (err, retailer) => {
      if (err) {
        return next(err);
      }
      if (!retailer) {
        return next(new Error('Retailer is not found'))
      }

      if (req.body.name) retailer.name = req.body.name;
      if (req.body.email) retailer.email = req.body.email;
      if (req.body.manager) retailer.manager = req.body.manager;

      retailer.save(err => {
        if (err) {
          return next(err)
        }

        res.json(retailer);
      });
    });
}

function rechargeDots(req, res, next) {
  if (!req.params.retailerId) {
    return next(new Error('retailerId is require for rechargeDots'))
  }

  if (!req.query.messageId) {
    return next(new Error('messageId is require for rechargeDots'))
  }

  if (!req.body.points) {
    return next(new Error('points is require for rechargeDots'))
  }

  if (!req.body.pointsPrice) {
    return next(new Error('message is require for rechargeDots'))
  }

  var newRetailer;

  Retailer.findOne({_id: req.params.retailerId}).exec()
    .then(retailer => {
      if (!retailer) {
        next(new Error('Retailer is not found'));
        return
      }

      retailer.points += +req.body.points;

      return retailer.save()
    })
    .then(retailer => {
      newRetailer = retailer;

      return Message.findOne({_id: req.query.messageId}).exec()
    })
    .then(message => {
      message.status = 'CLOSE';
      message.response = {
        createdAt: new Date(),
        points: +req.body.points,
        pointsPrice: +req.body.pointsPrice
      };

      return message.save()
    })
    .then(message => {
      var resp = {
        message: message,
        retailer: newRetailer
      };
      res.json(resp)
    })
    .catch(err => next(err));
}

function changePassword(req, res, next) {
  if (!req.params.retailerId) {
    return next(new Error('retailerId is required'))
  }
  if (!req.body.password) {
    return next(new Error('Current password is required'))
  }
  if (!req.body.newPassword) {
    return next(new Error('New password is required'))
  }

  Retailer.findOne({_id: req.params.retailerId})
    .select('+password').exec((err, retailer) => {
    if (err) {
      return next(err);
    }

    if (!retailer) {
      return next(new Error('retailer not found'));
    }

    retailer.comparePassword(req.body.password, function (err, isMatch) {
      if (err) {
        return next(err)
      }

      if (!isMatch) {
        return res.status(403).send({
          error: "WRONG_PASSWORD"
        });
      }

      retailer.password = req.body.newPassword;

      retailer.save((err, retailer) => {
        if (err) {
          return next(err)
        }
        res.json(retailer);
      })
    });

  });
}

function addSupportMessage(req, res, next) {
  if (!req.params.retailerId) {
    return next(new Error('retailerId is required'))
  }
  if (!(req.body.type === 'SUPPORT_MESSAGE' || req.body.type === 'RECHARGE_DOTS')) {
    return next(new Error('type is not correct'))
  }

  var messageData = _.clone(req.body);
  messageData.retailer = req.params.retailerId;

  let message = new Message(messageData);

  message.save()
    .then(message => {
      res.json(message);
    })
    .catch(err => {
      next(err)
    })
}

function exchangedRewardsToVouchers(req, res, next) {
  if (!req.query.retailerId) {
    return next(new Error('retailerId is required'))
  }

  let dataToSend = [];

  Voucher.find({retailer: req.query.retailerId}, (err, vouchers) => {
    if (err) {
      return next(err)
    }

    let loadStatus = {
      rewards: false,
      clients: false
    };
    let allRewardIds = [];
    let allClients = [];

    vouchers.forEach(voucher => {
      if (allRewardIds.indexOf(voucher.reward.toString()) === -1) {
        allRewardIds.push(voucher.reward.toString());
      }

      if (allClients.indexOf(voucher.client.toString()) === -1) {
        allClients.push(voucher.client.toString());
      }

      dataToSend.push({
        voucher: voucher
      })
    });

    Reward.find({_id: {$in: allRewardIds}}, (err, rewards) => {
      if (err) {
        return next(err)
      }

      dataToSend.forEach((data, i) => {
        dataToSend[i].reward = findObjectById(rewards, data.voucher.reward);
      });

      loadStatus.rewards = true;

      if (loadStatus.rewards && loadStatus.clients) sendResponse()
    });

    Client.find({_id: {$in: allClients}}, (err, clients) => {
      if (err) {
        return next(err)
      }

      dataToSend.forEach((data, i) => {
        dataToSend[i].client = findObjectById(clients, data.voucher.client)
      });

      loadStatus.clients = true;

      if (loadStatus.rewards && loadStatus.clients) sendResponse()
    })

  });

  function sendResponse() {
    res.json(dataToSend)
  }

  function findObjectById(collections, id) {
    for (let i = 0, len = collections.length; i < len; i++) {
      if (collections[i]._id.toString() === id.toString()) {
        return collections[i]
      }
    }
  }
}

function getLastActivity(req, res, next) {
  if (!req.params.retailerId) {
    return next(new Error('retailerId is required'))
  }

  Retailer.findOne({_id: req.params.retailerId}).select('+lastInteractionWithClient').exec((err, retailer) => {
    if (err) {
      return next(err)
    }

    let data = {
      lastAccess: retailer.lastAccess,
      lastInteractionWithClient: retailer.lastInteractionWithClient
    };

    res.json(data)
  })
}
