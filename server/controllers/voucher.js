'use strict';

var Voucher = require('../models/voucher');
var Client = require('../models/client');
var Store = require('../models/store');
var Place = require('../models/place');
var Reward = require('../models/reward');
var Constants = require('../libs/constants');
var voucher_codes = require('voucher-code-generator');

module.exports = {
  create,
  update,
  getVoucherByCode,
  updateVoucherStatus,
  getVoucherByClient,
  getVoucherByStore,
  emmitVoucherByReward,
  isCanBeUsed
};

/**
 * @api {POST} /api/voucher
 * @apiName create
 * @apiGroup Voucher
 *
 * @apiDescription
 * Create a new Voucher
 *
 * @apiParam {String} points Points for the Voucher, may come from Template from Store.
 * @apiParam {String} store Unique Unique Id From Store.
 * @apiParam {String} user Unique Unique Id From User.
 *
 * @apiSuccess (200) {String} points Points of the Voucher.
 * @apiSuccess (200) {id} store Id From Store.
 * @apiSuccess (200) {id} user Id From User.
 */

function create(req, res, next) {
  if (!req.body.client) {
    return next(new Error('Client is a require field'));
  }

  if (!req.body.reward) {
    return next(new Error('Reward is a require field'));
  }

  Client.findOne({_id: req.body.client}, (err, client) => {
    if (err) {
      return next(err);
    }

    if (!client) {
      return next(new Error('Can\'t create voucher - client not found'));
    }

    Reward.findOne({_id: req.body.reward}, (err, reward) => {
      if (err) {
        return next(err);
      }

      if (!reward) {
        return next(new Error('Can\'t create voucher - reward not found'));
      }

      if (client.points < reward.points) {
        return next(new Error('Lack of point to buy this voucher'));
      }

      let voucher = new Voucher({
        client: req.body.client,
        reward: reward._id,
        title: reward.title,
        points: reward.points,
        retailer: reward.retailer,
        code: generatingVoucherCode(client.vouchers)
      });

      voucher.save((err, voucher) => {
        if (err) {
          return next(err);
        }

        client.vouchers.emitted.push(voucher._id);
        client.points -= voucher.points;
        client.pointsInVouchers += voucher.points;

        client.save((err) => {
          if (err) {
            return next(err)
          }

          res.json(voucher);
        })

      });
    });
  });
}

/**
 * @api {patch} /api/voucher/:code
 * @apiName update
 * @apiGroup Voucher
 *
 * @apiDescritpion
 * Update a Voucher
 *
 */
function update(req, res, next) {
  Voucher.findOneAndUpdate({code: req.params.code},
    req.body,
    (err, voucher) => {
      if (err) {
        return next(err);
      }
      Client.findOne({_id: req.body.client}, (err, client) => {
        if (err) {
          return next(err);
        }

        if (client.points < voucher.points) {
          return res.send({error: "Seus pontos não são suficientes para vincular o voucher a você"});
        }

        Client.findOneAndUpdate(
          {_id: req.body.Client},
          {$inc: {'points': (-1 * voucher.points)}},
          (err, data) => {
            if (err) {
              return next(err);
            }
            res.json(voucher);
          });
      })
    });
}

/**
 * @api {GET} /api/voucher/:code
 * @apiName getVoucherByCode
 * @apiGroup Voucher
 *
 * @apiDescritpion
 * Get Voucher By The Code Generated
 *
 * @apiParam {String} code Code for the Voucher.
 *
 */
function getVoucherByCode(req, res, next) {
  Voucher.findOne({code: req.params.code}, (err, voucher) => {
    if (err) {
      return next(err);
    }

    res.json(voucher);
  });
}

/**
 * @api {GET} /api/voucher/:userId
 * @apiName getVoucherByClient
 * @apiGroup Voucher
 *
 * @apiDescritpion
 * Get Voucher By The Code Generated
 *
 * @apiParam {String} Id of the User.
 *
 */
function getVoucherByClient(req, res, next) {

  Voucher.find({'client': req.params.clientId}).exec((err, vouchers) => {
    if (err) {
      return next(err);
    }

    res.json(vouchers);
  });
}

function getVoucherByStore(req, res, next) {

  Voucher
    .find({'store': req.params.store})
    .populate('client')
    .exec((err, vouchers) => {
      if (err || !vouchers) {
        return next(err);
      }

      res.json(vouchers);
    });
}
/**
 * @api {PATCH} /api/voucher/:cnpj
 * @apiName updateVoucherStatus
 * @apiGroup Voucher
 *
 * @apiDescritpion Update a Status for a Voucher
 *
 * @apiParam {Number} status Status of the Voucher .
 *
 */
function isCanBeUsed(req, res, next) {
  if (!req.params.code || !req.params.client || !req.params.retailer) {
    return next(new Error('Bad request'))
  }

  Voucher.findOne(
    {
      code: req.params.code,
      client: req.params.client,
      status: 1,
      retailer: req.params.retailer
    },
    (err, voucher) => {
      if (err) {
        return next(err);
      }

      if (!voucher) {
        res.json({value: false});
      } else {
        res.json({value: true});
      }

    });
}

function updateVoucherStatus(req, res, next) {
  var status = req.body.status;
  if (+status !== 2 && +status !== 3 && status !== 'CONCLUDED' && status !== 'EXPIRED') {
    return next(new Error("Update status of voucher must be 'CONCLUDED || 2' or 'EXPIRED || 3'"));
  }

  if (!req.body.store) { // add action to history
    return next(new Error('Store is required for update dots'));
  }
  if (!req.body.retailer) { // add action to history
    return next(new Error('Retailer is required for update dots'));
  }

  if (status === 'CONCLUDED') status = 2;
  if (status === 'EXPIRED') status = 3;

  let newVoucher;
  let client;

  Voucher.findOneAndUpdate(
    {code: req.params.code, client: req.body.client, status: 1},
    {status: +status},
    {new: true}).exec()
    .then(voucher => {
      if (!voucher) {
        return next(new Error("This voucher not exist or already use"));
      }

      newVoucher = voucher;

      if (voucher.status === Constants.VOUCHER_EXPIRED) {

        return Client.findOneAndUpdate(
          {_id: voucher.client},
          {
            $inc: {
              'points': voucher.fromEmail ? 0 : +voucher.points,
              'pointsInVouchers': -voucher.points
            },
            $pull: {'vouchers.emitted': voucher._id},
            $push: {'vouchers.expired': voucher._id}
          }).exec();

      } else if (voucher.status === Constants.VOUCHER_CONCLUDED) {

        return Client.findOneAndUpdate(
          {_id: voucher.client},
          {
            $pull: {'vouchers.emitted': voucher._id},
            $push: {'vouchers.concluded': voucher._id},
            $inc: {'pointsInVouchers': -voucher.points}
          }).exec();

      } else {
        res.json(newVoucher);
      }
    })
    .then((clientData) => {
      if (newVoucher.status === Constants.VOUCHER_EXPIRED) {
        res.json(newVoucher);
      } else {
        client = clientData;
        let place = new Place({
          store: req.body.store,
          client: req.body.client,
          points: newVoucher.points,
          retailer: req.body.retailer,
          type: 'USE_VOUCHER'
        });

        return place.save()
      }
    })
    .then(() => {
      return Store.findOneAndUpdate(
        {_id: req.body.store},
        {
          $addToSet: {clients: client._id},
          $inc: {clientsActions:1}
        }).exec();
    })
    .then(() => {
      res.json(newVoucher);
    })
    .catch(err => {
      return next(err)
    });
}

function generatingVoucherCode(vouchers) {
  let code = voucher_codes.generate({
    pattern: "###-###-###",
    charset: "0123456789"
  })[0];

  let ifExistInExpired = vouchers.expired.indexOf(code.toString()) >= 0;
  let ifExistInConcluded = vouchers.concluded.indexOf(code.toString()) >= 0;
  let ifExistInEmitted = vouchers.emitted.indexOf(code.toString()) >= 0;

  if (ifExistInExpired || ifExistInConcluded || ifExistInEmitted) {
    generatingVoucherCode(vouchers)
  }

  return code;
}

function emmitVoucherByReward(req, res, next) {
  Reward.findOne({
    _id: req.params.reward
  }, (err, reward) => {
    if (err) {
      return next(err);
    }
    Voucher.findOne({
      _id: req.body.voucher
    }, (err, voucher) => {
      if (err) {
        return next(err);
      }
      var points = voucher.points - reward.points;


      if (points < 0 && !voucher.client) {
        res.json({
          error: "erro"
        });
      } else {
        voucher.satatus = Constants.VOUCHER_EMITTED;
        if (points > 0) {

          Client.findOneAndUpdate({
              _id: voucher.client
            }, {
              $inc: {
                'points': points
              }
            },
            (err, client) => {
              if (err) {
                return next(err);
              }

              voucher.save((err) => {
                if (err) {
                  return next(err);
                }

                res.json(voucher);
              });
            });


        } else {
          voucher.save((err) => {
            if (err) {
              return next(err);
            }

            res.json(voucher);
          });
        }

      }

    });

  });
}
