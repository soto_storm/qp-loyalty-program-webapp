'use strict';

var pkg = require('../../package.json');

module.exports = {
  health,
  session,
  version
};


// Health check
function health(req, res) {
  res.json({
    status: 'OK',
    pid: process.pid,
    memory: process.memoryUsage(),
    uptime: process.uptime()
  });
}

// Session 
function session(req, res) {
  res.json(req.session);
}

// Version
function version(req, res) {
  res.json({
    version: pkg.version
  });
}

