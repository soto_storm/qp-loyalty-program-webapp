'use strict';

let Reward = require('../models/reward');
let Retailer = require('../models/retailer');
const LIMIT_REWARD = 6;

module.exports = {
  getBy,
  create,
  update,
  remove
};

function getBy(req, res, next) {
  let query = {};

  if (req.query.rewardId) return getById(req, res, next);
  if (req.query.retailerId) query.retailer = req.query.retailerId;

  Reward.find(query, (err, rewards) => {
    if (err) {
      return next(err);
    }
    res.json(rewards);
  });
}

function getById(req, res, next) {
  Reward.findOne({_id: req.query.rewardId}, (err, reward) => {
    if (err) {
      return next(err);
    }
    res.json(reward);
  });
}

function create(req, res, next) {
  if (!req.body.retailer) {
    return next(new Error('retailer is require'))
  }
  if (!req.body.title) {
    return next(new Error('title is require'))
  }
  if (!req.body.points) {
    return next(new Error('points is require'))
  }

  Retailer.findById(req.body.retailer, (err, retailer) => {
    if (err) {
      return next(err)
    }

    if (!retailer) {
      return next(new Error('Retailer is not exist'))
    }

    if (retailer.rewards.length >= LIMIT_REWARD) {
      let err = new Error('REWARDS_LIMIT');
      err.status = 403;
      return next(err);
    }

    let reward = new Reward({
      title: req.body.title,
      points: +req.body.points,
      retailer: req.body.retailer
    });

    reward.save((err, reward) => {
      if (err) {
        return next(err);
      }

      retailer.rewards.push(reward._id);

      retailer.save(err => {
        if (err) {
          return next(err)
        }

        res.json(reward);
      })

    });

  });

}

function update(req, res, next) {

  let editReward = {};

  if (req.body.title) editReward.title = req.body.title;
  if (req.body.points) editReward.points = req.body.points;

  Reward.findOneAndUpdate(
    {_id: req.params.rewardId},
    editReward,
    {new: true},
    (err, reward) => {
      if (err) {
        return next(err);
      }

      if (!reward) {
        return next(new Error('Reward not found'));
      }

      res.json(reward);

    });
}


function remove(req, res, next) {
  Reward.findByIdAndRemove(req.params.reward, (err) => {
    if (err) {
      return next(err);
    }

    res.json({
      message: 'Reward removed'
    });
  });
}
