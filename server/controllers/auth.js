'use strict';

const Client = require('../models/client');
const Retailer = require('../models/retailer');
const Manager = require('../models/manager');
const passport = require('passport');
const sessionAge = require('../config').session.sessionAge;

module.exports = {
  client,
  retailer,
  manager,
  logout,
  getCurrentUser
};

function client(req, res, next) {
  passport.authenticate('local-client-login', function (err, client, info) {
    if (err) {
      return next(err)
    }
    if (!client) {
      return res.json({message: req.flash('loginMessage')})
    }

    req.logIn(client, function (err) {
      if (err) {
        return next(err);
      }

      client.lastLogout = +(new Date()) + sessionAge;

      client.save(err=> {
        if (err) {
          return next(err);
        }

        req.session.user = client;
        req.session.role = 'client';
       console.log(client);
        return res.json({user: client, redirectTo: "/client"})
      })
    });

  })(req, res, next);
}

function retailer(req, res, next) {
  passport.authenticate('local-retailer-login', function (err, retailer, info) {
    if (err) {
      return next(err)
    }
    if (!retailer) {
      return res.json({message: req.flash('loginMessage')})
    }

    req.logIn(retailer, function (err) {
      if (err) {
        return next(err);
      }

      retailer.lastLogout = +(new Date()) + sessionAge;
      retailer.lastAccess = +(new Date());

      retailer.save(err => {
        if (err) {
          return next(err);
        }

        req.session.user = retailer;
        req.session.role = 'retailer';
        return res.json({user: retailer, redirectTo: "/retailer"})
      })
    });

  })(req, res, next);
}

function manager(req, res, next) {
  passport.authenticate('local-manager-login', function (err, manager, info) {
    if (err) {
      return next(err)
    }
    if (!manager) {
      return res.json({message: req.flash('loginMessage')})
    }

    req.logIn(manager, function (err) {
      if (err) {
        return next(err);
      }

      manager.lastLogout = +(new Date()) + sessionAge;
      manager.lastAccess = +(new Date());

      manager.save(err => {
        if (err) {
          return next(err);
        }

        req.session.user = manager;
        req.session.role = 'manager';
        return res.json({user: manager, redirectTo: "/manager"})
      })
    });

  })(req, res, next);
}

function logout(req, res, next) {

  switch (req.session.role) {
    case 'client':
      Client.findOne({_id: req.session.user._id}, (err, client) => {
        client.lastLogout = new Date();

        client.save(err => {
          if (err) {
            return next(err)
          }
          getLogout()
        })
      });
      break;
    case 'retailer':
      Retailer.findOne({_id: req.session.user._id}, (err, retailer) => {
        retailer.lastLogout = new Date();

        retailer.save(err => {
          if (err) {
            return next(err)
          }
          getLogout()
        })
      });
      break;
    case 'manager':
      Manager.findOne({_id: req.session.user._id}, (err, manager) => {
        manager.lastLogout = new Date();

        manager.save(err => {
          if (err) {
            return next(err)
          }
          getLogout()
        })
      });
      break;
    default:
      getLogout();
      break;
  }

  function getLogout() {
    req.logout();
    req.session.destroy();

    return res.json({
      message: 'Logout success',
      redirectTo: '/authentication'
    });
  }

}

function getCurrentUser(req, res, next) {
  if (!req.session.user) {
    return next(new Error('User not signIn'))
  }

  switch (req.session.role) {
    case 'client':
      Client.findOne(
        {_id: req.session.user._id},
        (err, client) => {
          if (err) {
            return next(err)
          }

          res.json(client)
        });
      break;
    case 'retailer':
      Retailer.findOne(
        {_id: req.session.user._id},
        (err, retailer) => {
          if (err) {
            return next(err)
          }

          res.json(retailer)
        });
      break;
    case 'manager':
      Manager.findOne(
        {_id: req.session.user._id},
        (err, manager) => {
          if (err) {
            return next(err)
          }

          res.json(manager)
        });
      break;
    default:
      next(new Error('role is not parse'));
      break;
  }

}
