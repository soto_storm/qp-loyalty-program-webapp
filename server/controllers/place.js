'use strict';

let Places = require('../models/place');

module.exports = {
  getBy
};

function getBy(req, res, next) {
  if (!req.query) {
    return next(new Error('Places not found(no search params)'));
  }

  let query = {};

  if (req.query.clientId) query.client = req.query.clientId;
  if (req.query.retailerId) query.retailer = req.query.retailerId;

  Places.find(query, (err, places) => {
    if (err) {
      return next(err);
    }

    res.json(places);
  });
}
