'use strict';

let Client = require('../models/client');
let Retailer = require('../models/retailer');
let Place = require('../models/place');
let Store = require('../models/store');
let Utils = require('../libs/utils');
let debug = require('debug')('qp:controller:client');
let mandrill = require('mandrill-api/mandrill');
let mandrillClient = new mandrill.Mandrill('KTk7d86ztJx565LBh2wbew');
const config = require('../config');
var _ = require('lodash');

module.exports = {
  create,
  update,
  getBy,
  updatePoint,
  changePassword,
  resetPassword
};

/**
 * @api {POST} /api/client
 * @apiName create
 * @apiGroup Client
 *
 * @apiDescription Create a new Client
 *
 * @apiParam {String} name Client's name.
 * @apiParam {String} email Client's email.
 * @apiParam {String} cpf Client's unique CPF.
 * @apiParam {Date} birthday Client's birthday.
 * @apiParam {String} gender Client's gender('M' or 'F').
 * @apiParam {String} password Client's password.
 *
 * @apiSuccess (200) {Object} user Created client.
 * @apiSuccess (200) {String} redirectTo Must redirect to this address.
 */

function create(req, res, next) {
  if (!req.body.email) {
    return next(new Error('Email is required for create Client'))
  }

  Client.findOne({cpf: req.body.cpf}, (err, client) => {
    if (err) {
      return next(err)
    }
    if (client) {

      if (client.email) {
        return next(new Error('Client already exist'))
      }

      client.name = req.body.name;
      client.email = req.body.email;
      client.birthday = req.body.birthday;
      client.gender = req.body.gender;
      client.password = req.body.password;

      client.save((err, client) => {
        saveClient(err, req, client, (err, client) => {
          if (err) return next(err);
          return res.json({user: client, redirectTo: "/client"})
        });
      });

    } else {
      let newClient = new Client(req.body);

      newClient.save((err, newClient) => {
        saveClient(err, req, newClient, (err, client) => {
          if (err) return next(err);
          return res.json({user: client, redirectTo: "/client"})
        });
      });
    }
  })
}

/**
 * @api {GET} /api/client
 * @apiName get clients
 * @apiGroup Client
 *
 * @apiDescription Get a clients for query params.
 *
 * @apiParam {String} cpf Client's CPF.
 * @apiParam {Id} retailer All clients of Retailer.
 *
 * @apiSuccess (200) {Object} Client If CPF query.
 * @apiSuccess (200) {Array} Clients If retailer query.
 */

function getBy(req, res, next) {
  if (req.query.cpf) {
    return getByCPF(req, res, next);
  }

  var query = {email: {$exists: true}};

  if (req.query.isFilter) return getFilterUsers(req, res, next);

  if (req.query.retailer)return getClientsByRetailer(req, res, next);

  Client.find(query, (err, clients) => {
    if (err) {
      return next(err);
    }

    Utils.hideClientsCpf(clients);

    res.json(clients);
  });
}

function getByCPF(req, res, next) {
  Client.findOne({
    cpf: req.query.cpf
  }, (err, client) => {
    if (err) {
      return next(err);
    }
    res.json(client);
  });
}

/**
 * @api {GET} /api/client?:retailer
 * @apiName get clients
 * @apiGroup Client
 *
 * @apiDescription Get all retailer's clients and allows write some params(see in Params).
 *
 * @apiParam {Number} onlyLength If you want to get quantity of retailer's client.
 * @apiParam {Boolean} genderGraphData Return gender statistic for Graph.
 * @apiParam {Boolean} ageGroupGraphData Return age statistic for Graph.
 */

function getClientsByRetailer(req, res, next) {

  if (req.query.retailer === 'ALL') {
    getClients()
  } else {
    Retailer.findOne({_id: req.query.retailer}, (err, retailer) => {
      if (err) {
        return next(err)
      }

      if (!retailer) {
        return next(new Error('Retailer is not find'))
      }

      getClients(retailer.clients)
    });
  }

  function getClients(clientsIds) {
    var selector = {
      email: {$exists: true}
    };

    if (clientsIds) {
      selector._id = {$in: clientsIds}
    }

    Client.find(selector,
      (err, clients) => {
        if (err) {
          return next(err);
        }

        if (req.query.onlyLength) {

          res.json({
            value: clients.length
          });

        } else if (req.query.genderGraphData) {

          let genderInfo = {
            male: 0,
            female: 0
          };

          clients.forEach(client => {
            if (client.gender.toUpperCase() === 'M') {
              genderInfo.male++
            } else {
              genderInfo.female++
            }
          });

          res.json(genderInfo);

        } else if (req.query.ageGroupGraphData) {

          let ageGroupGraphData = {
            'TO_17': 0,
            'FROM_18_TO_25': 0,
            'FROM_26_TO_30': 0,
            'FROM_31_TO_40': 0,
            'FROM_41': 0
          };

          clients.forEach(client => {
            let today = new Date();
            let todayYear = today.getFullYear();

            if (client.birthday < today.setFullYear(todayYear - 40)) {
              ageGroupGraphData['FROM_41']++;
            } else if (client.birthday >= today.setFullYear(todayYear - 40) && client.birthday < today.setFullYear(todayYear - 30)) {
              ageGroupGraphData['FROM_31_TO_40']++;
            } else if (client.birthday >= today.setFullYear(todayYear - 30) && client.birthday < today.setFullYear(todayYear - 25)) {
              ageGroupGraphData['FROM_26_TO_30']++;
            } else if (client.birthday >= today.setFullYear(todayYear - 25) && client.birthday < today.setFullYear(todayYear - 17)) {
              ageGroupGraphData['FROM_18_TO_25']++;
            } else {
              ageGroupGraphData['TO_17']++;
            }
          });

          res.json(ageGroupGraphData);

        } else {
          Utils.hideClientsCpf(clients);
          res.json(clients);
        }

      });
  }
}

/**
 * @api {PUT} /api/client/:clientId
 * @apiName update client
 * @apiGroup Client
 *
 * @apiDescription Update client for clientId.
 *
 * @apiParam {String} name Client's name.
 * @apiParam {String} email Client's email.
 * @apiParam {String} cpf Client's unique CPF.
 * @apiParam {Date} birthday Client's birthday.
 * @apiParam {String} gender Client's gender('M' or 'F').
 * @apiParam {String} password Client's password.
 *
 * @apiSuccess (200) {Object} Client Updated client.
 */

function update(req, res, next) {
  if (!req.params.clientId) {
    return next(new Error('clientId is require for update'))
  }

  Client.findOne(
    {_id: req.params.clientId},
    (err, client) => {
      if (err) {
        return next(err);
      }
      if (!client) {
        return next(new Error('Client is not found'))
      }

      if (req.body.name) client.name = req.body.name;
      if (req.body.email) client.email = req.body.email;
      if (req.body.gender) client.gender = req.body.gender;
      if (req.body.password) client.password = req.body.password;

      client.birthday = req.body.birthday || null;

      client.save(err => {
        if (err) {
          return next(err)
        }

        res.json(client);
      });
    });
}

/**
 * @api {PATCH} /api/client/:cpf/points/update
 * @apiName update client's points
 * @apiGroup Client
 *
 * @apiDescription Update client's points for CPF.
 *
 * @apiParam {Number} points Points to update.
 * @apiParam {Id} store Store Id where updated points.
 * @apiParam {Id} retailer Retailer Id.
 *
 * @apiSuccess (200) {Object} Client Updated client.
 */

function updatePoint(req, res, next) {
  if (!+req.body.points) {/*If points don't convert to number throw error*/
    return next(new Error('Points don\'t convert to number'));
  }
  if (!req.body.store) { // add action to history
    return next(new Error('Store is required for update dots'));
  }
  if (!req.body.retailer) { // add action to history
    return next(new Error('Retailer is required for update dots'));
  }

  Client.findOne({cpf: req.params.cpf}).select('+pointsOfRetailers').exec((err, client) => {
    if (err) {
      return next(err);
    } else if (!client) {
      client = new Client({cpf: req.params.cpf});
    }

    client.points = +client.points + Math.round(+req.body.points);

    if (client.pointsOfRetailers && client.pointsOfRetailers[req.body.retailer]) {
      client.pointsOfRetailers[req.body.retailer] = +client.pointsOfRetailers[req.body.retailer] + +req.body.points;
    } else if (client.pointsOfRetailers) {
      client.pointsOfRetailers[req.body.retailer] = +req.body.points;
    } else {
      client.pointsOfRetailers = {};
      client.pointsOfRetailers[req.body.retailer] = +req.body.points;
    }

    client.markModified('pointsOfRetailers');

    client.save((err, client) => {
      if (err) {
        return next(err);
      }

      Retailer.findOneAndUpdate(
        {_id: req.body.retailer},
        {
          $inc: {points: -req.body.points},
          $addToSet: {clients: client._id},
          $set: {lastInteractionWithClient: new Date()}
        },
        {new: true},
        (err, retailer) => {
          if (err) {
            return next(err)
          }

          Store.findOne(
            {_id: req.body.store},
            (err, store) => {
              if (err) {
                return next(err);
              }

              let today = new Date();
              today.setHours(0, 0, 0, 0);
              let dateExist = false;

              if (!store.revenue) {
                store.revenue = [];
              }

              store.revenue.forEach((dayInfo, i) => {
                if (+dayInfo.date === +today) {
                  dateExist = true;
                  store.revenue[i].points += +req.body.points;
                  store.markModified('revenue')
                }
              });

              if (!dateExist) {
                store.revenue.push({
                  date: today,
                  points: +req.body.points
                })
              }

              if (store.clients.indexOf(client._id) === -1) {
                store.clients.push(client._id)
              }

              store.clientsActions++;
              store.giveDotsAmount = store.giveDotsAmount + +req.body.points;

              store.lastReleaseDate = new Date();

              store.save(err => {
                if (err) {
                  return next(err)
                }

                let place = new Place({
                  store: req.body.store,
                  client: client._id,
                  points: req.body.points,
                  retailer: req.body.retailer,
                  type: 'UPDATE_DOTS'
                });

                place.save((err) => {
                  if (err) {
                    return next(err);
                  }

                  res.json(client);
                });
              })

            });

        });

    });

  });
}

function changePassword(req, res, next) {
  if (!req.params.clientId) {
    return next(new Error('clientId is required'))
  }
  if (!req.body.password) {
    return next(new Error('Current password is required'))
  }
  if (!req.body.newPassword) {
    return next(new Error('New password is required'))
  }

  Client.findOne({_id: req.params.clientId})
    .select('+password').exec((err, client) => {
    if (err) {
      return next(err);
    }

    if (!client) {
      return next(new Error('client not found'));
    }

    client.comparePassword(req.body.password, function (err, isMatch) {
      if (err) {
        return next(err)
      }

      if (!isMatch) {
        return res.status(403).send({
          error: "WRONG_PASSWORD"
        });
      }

      client.password = req.body.newPassword;

      client.save((err, client) => {
        if (err) {
          return next(err)
        }
        res.json(client);
      })
    });

  });
}

function saveClient(err, req, client, callback) {
  if (err) {
    return next(err);
  }

  if (req.query.signup) {

    let params = {
      "template_name": "Mail_Confirmation",
      "template_content": [],
      "message": {
        "subject": "Confirmation email",
        "from_email": "queropontos@" + config.email.mandrill.domain,
        "from_name": "Quero Pontos",
        "to": [{email: client.email}],
        "headers": {
          "Reply-To": "noreply@" + config.email.mandrill.domain
        },
        "inline_css": true,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [
          {
            "name": "CLIENT_NAME",
            "content": client.name
          },
          {
            "name": "CONFIRM_LINK",
            "content": 'http://' + req.headers.host + '/api/mail/confirmation/' + client._id
          }
        ]
      }
    };

    mandrillClient.messages.sendTemplate(params, function (result) {

      req.logIn(client, function (err) {
        if (err) {
          return next(err);
        }
        req.session.user = client;
        req.session.role = 'client';
        callback(null, client)
      });

    }, function (e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
      callback(e)
    });

  } else {
    callback(null, client)
  }
}

function getFilterUsers(req, res, next) {

  if (req.query.retailer && req.query.retailer !== 'ALL') {
    Retailer.findOne({_id: req.query.retailer}, (err, retailer) => {
      if (err) {
        return next(err)
      }

      if (!retailer) {
        return next(new Error('Retailer is not find'))
      }

      filterClients(retailer.clients)
    });
  } else {
    filterClients()
  }

  function filterClients(clientsIds) {

    let aggregationArray = [];

    var match = {
      $match: {
        email: {$exists: true}
      }
    };

    if (clientsIds) {
      match.$match._id = {$in: clientsIds}
    }

    aggregationArray.push(match);

    if (req.query.birthday) {

      let existBirthday = {
        $match: {
          birthday: {$exists: true}
        }
      };

      let today = new Date();
      today.setHours(0, 0, 0, 0);

      let p1 = {
        "$project": {
          "name": 1,
          "birthday": 1,
          "points": 1,
          "pointsInVouchers": 1,
          "gender": 1,
          "email": 1,
          "lastLogout": 1,
          "pointsOfRetailers": 1,
          "todayDayOfYear": {"$dayOfYear": today},
          "leap": {
            "$or": [
              {"$eq": [0, {"$mod": [{"$year": "$birthday"}, 400]}]},
              {
                "$and": [
                  {"$eq": [0, {"$mod": [{"$year": "$birthday"}, 4]}]},
                  {"$ne": [0, {"$mod": [{"$year": "$birthday"}, 100]}]}]
              }]
          },
          "dayOfYear": {"$dayOfYear": "$birthday"}
        }
      };

      let p2 = {
        "$project": {
          "name": 1,
          "birthday": 1,
          "points": 1,
          "pointsInVouchers": 1,
          "gender": 1,
          "email": 1,
          "lastLogout": 1,
          "pointsOfRetailers": 1,
          "daysTillBirthday": {
            "$subtract": [
              {
                "$add": [
                  "$dayOfYear",
                  {"$cond": [{"$lt": ["$dayOfYear", "$todayDayOfYear"]}, 365, 0]}
                ]
              },
              "$todayDayOfYear"
            ]
          }
        }
      };

      let p1p = {
        "$project": {
          "name": 1,
          "birthday": 1,
          "points": 1,
          "pointsInVouchers": 1,
          "gender": 1,
          "email": 1,
          "lastLogout": 1,
          "pointsOfRetailers": 1,
          "todayDayOfYear": 1,
          "dayOfYear": {
            "$subtract": [
              "$dayOfYear",
              {"$cond": [{"$and": ["$leap", {"$gt": ["$dayOfYear", 59]}]}, 1, 0]}]
          }
        }
      };

      let query = {
        daysTillBirthday: {
          "$lt": 31
        }
      };

      if (Number.isInteger(+req.query.birthday) && +req.query.birthday > 0) {
        query.daysTillBirthday.$lt = +req.query.birthday
      }

      let m = {"$match": query};

      aggregationArray.push(existBirthday, p1, p1p, p2, m)

    }

    if (req.query.ageFrom && Number.isInteger(+req.query.ageFrom) && +req.query.ageFrom > 0) {
      let today = new Date();
      let todayYear = today.getFullYear();
      let lessThanDate = today.setFullYear(todayYear - req.query.ageFrom);
      let ageFromMatch = {
        $match: {
          birthday: {$lt: new Date(lessThanDate)}
        }
      };

      aggregationArray.push(ageFromMatch)
    }

    if (req.query.ageTo && Number.isInteger(+req.query.ageTo) && +req.query.ageTo > 0) {
      let today = new Date();
      let todayYear = today.getFullYear();
      let greateThanDate = today.setFullYear(todayYear - req.query.ageTo);

      let ageToMatch = {
        $match: {
          birthday: {$gt: new Date(greateThanDate)}
        }
      };

      aggregationArray.push(ageToMatch)
    }

    if (req.query.gender && (req.query.gender.toUpperCase() == 'M' || req.query.gender.toUpperCase() == 'F')) {
      let genderMatch = {
        $match: {
          gender: req.query.gender.toUpperCase()
        }
      };

      aggregationArray.push(genderMatch)
    }

    if (req.query.inactive) {
      var inactiveDay = new Date();
      inactiveDay.setDate(inactiveDay.getDate() - 90);

      let inactiveMatch = {
        $match: {
          lastLogout: {
            $exists: true,
            $lt: new Date(inactiveDay)
          }
        }
      };

      aggregationArray.push(inactiveMatch)
    }

    if (req.query.name) {
      let nameMatch = {
        $match: {
          name: new RegExp(req.query.name, 'i')
        }
      };

      aggregationArray.push(nameMatch)
    }

    Client.aggregate(aggregationArray, (err, clients) => {
      if (err) {
        return next(err)
      }

      if (req.query.profitable) {

        let sortClients = _.sortBy(clients, function (client) {
          if(req.query.retailer && req.query.retailer !== 'ALL'){
            return -client.pointsOfRetailers[req.query.retailer];
          } else {
            return -client.points;
          }
        });

        if (Number.isInteger(+req.query.profitable) && +req.query.profitable > 0) {

          sortClients = sortClients.slice(0, +req.query.profitable);

          Utils.hideClientsCpf(sortClients);
          res.json(sortClients)

        } else {

          Client.count({_id: {$in: clients}}, (err, numOfClients) => {
            if (err) {
              return next(err)
            }

            let quantityClient = Math.ceil(numOfClients * 0.2);
            sortClients = sortClients.slice(0, quantityClient);

            Utils.hideClientsCpf(sortClients);
            res.json(sortClients)
          });

        }

        return
      }

      if (req.query.storeVisited) {

        let sortClientsByVisited = _.sortBy(clients, function (client) {
          return -client.storeVisitedCount;
        });

        if (Number.isInteger(+req.query.storeVisited) && +req.query.storeVisited > 0) {

          sortClientsByVisited = sortClientsByVisited.slice(0, +req.query.profitable);

          Utils.hideClientsCpf(sortClientsByVisited);
          res.json(sortClientsByVisited)

        } else {

          Client.count({_id: {$in: clients}}, (err, numOfClients) => {
            if (err) {
              return next(err)
            }

            let quantityClient = Math.ceil(numOfClients * 0.2);
            sortClientsByVisited = sortClientsByVisited.slice(0, quantityClient);

            Utils.hideClientsCpf(sortClientsByVisited);
            res.json(sortClientsByVisited)
          });

        }
        return;
      }

      Utils.hideClientsCpf(clients);
      res.json(clients)
    });
  }
}

function resetPassword(req, res, next) {
  if (!req.query.token) {
    return next(new Error('clientId is required'))
  }
  if (!req.body.password) {
    return next(new Error('Current password is required'))
  }

  Client.findOne({
    resetPasswordToken: req.query.token,
    resetPasswordExpires: {$gt: Date.now()}
  }).select('+password').exec()
    .then(client => {

      if (!client) return next(new Error('client not found'));

      client.password = req.body.password;

      return client.save()
    })
    .then(client => res.json(client))
    .catch(err => next(err))
}
