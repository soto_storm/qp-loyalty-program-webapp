'use strict';

let debug = require('debug')('qp:controllers:mail');
let mandrill = require('mandrill-api/mandrill');
let mandrillClient = new mandrill.Mandrill('KTk7d86ztJx565LBh2wbew');
var voucher_codes = require('voucher-code-generator');
const config = require('../config');

let Retailer = require('../models/retailer');
let Reward = require('../models/reward');
let Client = require('../models/client');
let Voucher = require('../models/voucher');

module.exports = {
  sendToClientsGroup,
  confirmClientEmail,
  forgotPassword,
  sendConfirmEmail
};

function sendToClientsGroup(req, res, next) {
  if (!req.body.clients || req.body.clients.length === 0) {
    return next(new Error('Clients is required'))
  }

  if (!req.body.retailer) {
    return next(new Error('Retailer is required'))
  }

  if (!req.body.template) {
    return next(new Error('template is required'))
  } else {
    var template = req.body.template.toLowerCase();

    if (template === 'voucher') template = 'reward';

    if (!(template == 'birthday' || template == 'christmas' || template == 'reward')) {
      return next(new Error('template must be birthday or christmas or reward'))
    }
  }

  if (!req.body.reward) {
    return next(new Error('reward is required'))
  }

  let senderRetailer;

  Retailer.findById(req.body.retailer).exec()
    .then(retailer => {
      if (retailer.sendEmailStatistic.quantity && +retailer.sendEmailStatistic.quantity > 1) {
        throw new Error('Limit sending email. Max send is 2 emails')
      }

      if (retailer.sendEmailStatistic.quantity === undefined) {
        retailer.sendEmailStatistic.quantity = 0
      }

      retailer.sendEmailStatistic.quantity++;

      retailer.markModified('sendEmailStatistic');

      return retailer.save()
    })
    .then(retailer => {

      senderRetailer = retailer;
      return Reward.findOne({_id: req.body.reward}).exec()
    })
    .then(reward => {
      if (!reward) {
        return next(new Error('Reward is not found'))
      }

      let clients = req.body.clients;
      let emailStatus = {};


      let creatingVouchers = 0;
      let sendData = [];

      clients.forEach(client => {

        emailStatus[client._id] = false;

        createVoucher(client, reward, senderRetailer, template, (err, resp) => {
          if (err) {
            throw err
          }

          sendData.push(resp);
          creatingVouchers++;

          if (creatingVouchers === clients.length) {
            //  all clients email has been send
            let sendInfo = {
              sendClients: creatingVouchers,
              status: 'OK',
              sendData: sendData
            };

            res.json(sendInfo)
          }
        })
      })

    })
    .catch(err => {
      next(err)
    });
}

function createVoucher(clientData, reward, retailer, template, callback) {

  Client.findOne({_id: clientData._id}, (err, client) => {
    if (err) {
      return callback(err);
    }

    if (!client) {
      return callback(new Error('Can\'t create voucher - client not found'));
    }

    let voucherCode = generatingVoucherCode(client.vouchers);

    let voucher = new Voucher({
      client: client._id,
      fromEmail: true,
      reward: reward._id,
      title: reward.title,
      points: reward.points,
      retailer: reward.retailer,
      code: voucherCode
    });

    voucher.save((err, voucher) => {
      if (err) {
        return callback(err);
      }

      client.vouchers.emitted.push(voucher._id);
      client.pointsInVouchers = +client.pointsInVouchers + +reward.points;

      client.save((err) => {
        if (err) {
          return callback(err)
        }

        sendEmailToUser(client, voucher, retailer, template, callback)
      })

    });
  });
}

function sendEmailToUser(client, voucher, retailer, templateName, callback) {

  var params = getMailParams(client, voucher, retailer, templateName)

  mandrillClient.messages.sendTemplate(params, function (result) {
    callback(null, result)
  }, function (e) {
    // Mandrill returns the error as an object with name and message keys
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    callback(e)
  });

}

function generatingVoucherCode(vouchers) {
  let code = voucher_codes.generate({
    pattern: "###-###-###",
    charset: "0123456789"
  })[0];

  let ifExistInExpired = vouchers.expired.indexOf(code.toString()) >= 0;
  let ifExistInConcluded = vouchers.concluded.indexOf(code.toString()) >= 0;
  let ifExistInEmitted = vouchers.emitted.indexOf(code.toString()) >= 0;

  if (ifExistInExpired || ifExistInConcluded || ifExistInEmitted) {
    generatingVoucherCode(vouchers)
  }

  return code;
}

function getMailParams(client, voucher, retailer, templateName) {
  const templateMap = {
    'BIRTHDAY': 'Birthday_template',
    'CHRISTMAS': 'Christmas_template',
    'REWARD': 'Generic_reward_template'
  };
  const subjectMap = {
    'BIRTHDAY': 'Feliz aniversário',
    'CHRISTMAS': 'Feliz natal',
    'REWARD': 'You have new Reward'
  };

  let params = {
    "template_content": [],
    "message": {
      "from_email": "queropontos@" + config.email.mandrill.domain,
      "from_name": "Quero Pontos",
      "to": [{}],
      "headers": {
        "Reply-To": "noreply@" + config.email.mandrill.domain
      },
      "inline_css": true,
      "merge": true,
      "merge_language": "mailchimp",
      "global_merge_vars": [
        {
          "name": "CLIENT_NAME",
          "content": client.name
        },
        {
          "name": "CLIENT_DOTS",
          "content": client.points
        },
        {
          "name": "RETAILER_NAME",
          "content": retailer.name
        },
        {
          "name": "REWARD_NAME",
          "content": voucher.title
        },
        {
          "name": "REWARD_CODE",
          "content": voucher.code
        }
      ]
    }
  };

  params.template_name = templateMap[templateName.toUpperCase()];
  params.message.subject = subjectMap[templateName.toUpperCase()];
  params.message.to[0].email = client.email;

  return params;
}

function confirmClientEmail(req, res, next) {
  if (!req.params.clientId) {
    return next(new Error('clientId is required'))
  }

  Client.findById(req.params.clientId, (err, client) => {
    if (err) {
      return next(err)
    }

    if (client.emailConfirm) {
      res.redirect('/client');
    } else {
      client.emailConfirm = true;

      client.save(err => {
        if (err) {
          return next(err)
        }

        req.logIn(client, function (err) {
          if (err) {
            return next(err);
          }
          req.session.user = client;
          req.session.role = 'client';
          res.redirect('/client');
        });

      })
    }

    let params = {
      "template_name": "Welcome_to_QP",
      "template_content": [],
      "message": {
        "subject": "Bem-vindo ao Quero Pontos",
        "from_email": "queropontos@" + config.email.mandrill.domain,
        "from_name": "Quero Pontos",
        "to": [{email: client.email}],
        "headers": {
          "Reply-To": "noreply@" + config.email.mandrill.domain
        },
        "inline_css": true,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [
          {
            "name": "CLIENT_NAME",
            "content": client.name
          },
          {
            "name": "CLIENT_DOTS",
            "content": client.points
          },
          {
            "name": "PROFILE_LINK",
            "content": 'http://' + req.headers.host + '/client/#/profile'
          }
        ]
      }
    };

    mandrillClient.messages.sendTemplate(params, function (result) {
    }, function (e) {
      // Mandrill returns the error as an object with name and message keys
      console.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    });
  })
}


function forgotPassword(req, res, next) {

  if (!req.query.cpf) {
    return next(new Error('Cpf is required'))
  }

  Client.findOne({cpf: req.query.cpf}).exec()
    .then(client => {

      if (!client) {
        res.status(200).send({
          error: "Client with this cpf is not exist"
        });
        return;
      }

      client.resetPasswordToken = makeUniqueToken();
      client.resetPasswordExpires = Date.now() + 3600000; // 1 hour

      return client.save();
    })
    .then(client => {
      if (!client) return;

      let params = {
        "template_name": "ForgotPassword",
        "template_content": [],
        "message": {
          "subject": "Bem-vindo ao Quero Pontos",
          "from_email": "queropontos@" + config.email.mandrill.domain,
          "from_name": "Quero Pontos",
          "to": [{email: client.email}],
          "headers": {
            "Reply-To": "noreply@" + config.email.mandrill.domain
          },
          "inline_css": true,
          "merge": true,
          "merge_language": "mailchimp",
          "global_merge_vars": [
            {
              "name": "FORGOT_PASSWORD_LINK",
              "content": 'http://' + req.headers.host + '/authentication/#/resetPassword?token=' + client.resetPasswordToken
            }
          ]
        }
      };

      mandrillClient.messages.sendTemplate(params, function (result) {
        return res.json(result)
      }, function (e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
        return next(new Error(e.message))
      });
    })
    .catch(err => {
      next(err)
    })

}

function sendConfirmEmail(req, res, next) {
  if (!req.query.clientId) {
    return next(new Error('ClientId is required'))
  }

  Client.findOne({_id: req.query.clientId}, (err, client) => {
    if (err) return next(err);
    if (!client) return next(new Error('Client is not found'));

    let params = {
      "template_name": "Mail_Confirmation",
      "template_content": [],
      "message": {
        "subject": "Confirmation email",
        "from_email": "queropontos@" + config.email.mandrill.domain,
        "from_name": "Quero Pontos",
        "to": [{email: client.email}],
        "headers": {
          "Reply-To": "noreply@" + config.email.mandrill.domain
        },
        "inline_css": true,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [
          {
            "name": "CLIENT_NAME",
            "content": client.name
          },
          {
            "name": "CONFIRM_LINK",
            "content": 'http://' + req.headers.host + '/api/mail/confirmation/' + client._id
          }
        ]
      }
    };

    mandrillClient.messages.sendTemplate(params, function (result) {
      res.json(result)
    }, function (e) {
      // Mandrill returns the error as an object with name and message keys
      console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
      next(e)
    });

  })
}

function makeUniqueToken() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 40; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


