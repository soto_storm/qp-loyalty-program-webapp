'use strict';

var Store = require('../models/store');
var _ = require('lodash');

module.exports = {
  create,
  getBy,
  getBestStores,
  getPlacesNearby,
  getUniqueToken,
  getRevenue,
  update
};


/**
 * @api {post} /api/store
 * @apiName create
 * @apiGroup Store
 *
 * @apiDefine Create
 * @apiDescritpion
 * Create a new Store
 *
 * @apiParam {String} storeName Name for the Store.
 * @apiParam {String} cnpj Unique CNPJ for the Store.
 * @apiParam {String} managerReponsible Name of the Manager for the Store.
 * @apiParam {String} managerRegistration Register of the Manager for the Store.
 * @apiParam {String} phone Phone number of the Manager for the Store.
 *
 * @apiSuccess (200) {storeName} storeName Name for the Store.
 * @apiSuccess (200) {String} cnpj Unique CNPJ for the Store.
 * @apiSuccess (200) {String} managerReponsible Name of the Manager for the Store.
 * @apiSuccess (200) {String} managerRegistration Register of the Manager for the Store.
 * @apiSuccess (200) {String} phone Phone number of the Manager for the Store.
 */
function create(req, res, next) {

  if (!req.body.retailer) {
    return next(new Error('retailer is require'))
  }
  if (!req.body.cnpj) {
    return next(new Error('cnpj is require'))
  }
  if (!req.body.address) {
    return next(new Error('address is require'))
  }
  if (!req.body.storeName) {
    return next(new Error('storeName is require'))
  }

  let storeData = {
    cnpj: req.body.cnpj,
    address: req.body.address,
    storeName: req.body.storeName,
    description: req.body.description,
    retailer: req.body.retailer
  };

  if (req.body.phone) storeData.phone = req.body.phone;
  if (req.body.manager) storeData.manager = req.body.manager;

  let store = new Store(storeData);

  store.save((err) => {
    if (err) {
      return next(err);
    }

    res.json(store);
  });
}

function getBy(req, res, next) {
  let query = {};

  if (req.query.storeId) {
    return getByStoreId(req, res, next);
  }

  if (req.query.retailerId) query.retailer = req.query.retailerId;

  Store.find(query, (err, stores) => {
    if (err) {
      return next(err);
    }

    res.json(stores);
  });
}

function getBestStores(req, res, next) {
  if (req.query.quantity) {
    var quantity = parseFloat(req.query.quantity);

    if (req.query.quantity.indexOf('%') !== -1) {
      var percentage = true;
    }
  }

  Store.find({}).exec()
    .then(stores => {

      var responseData = _.sortBy(stores, function (store) {
        return -store.giveDotsAmount;
      });

      if (!isNaN(quantity)) {
        if (percentage) {
          quantity = Math.ceil(stores.length * (quantity / 100));
        }

        if (quantity < stores.length) {
          responseData.splice(quantity);
        }
      }

      res.json(responseData);
    })
    .catch(err => {
      next(err)
    });
}

function getByStoreId(req, res, next) {
  Store.findOne({_id: req.query.storeId}, (err, store) => {
    if (err) {
      return next(err);
    }

    res.json(store);
  });
}

function getPlacesNearby(req, res, next) {
  if (!req.query.nelat || !req.query.nelng || !req.query.swlat || !req.query.swlng) {
    return next(new Error('Wrong coordinates'));
  }

  let query = {
    $and: [
      {$and: [{'address.locationLat': {$lte: +req.query.nelat}}, {'address.locationLat': {$gte: +req.query.swlat}}]},
      {$and: [{'address.locationLng': {$lte: +req.query.nelng}}, {'address.locationLng': {$gte: +req.query.swlng}}]}
    ]
  };

  Store.find(query, (err, stores) => {
    if (err) {
      return next(err);
    }

    res.json(stores)
  });
}

function getUniqueToken(req, res, next) {
  if (!req.session || !req.session.user || req.session.role !== 'retailer') {
    return res.status(403).json({
      error: 'PERMISSION_DENIED'
    })
  }

  if (!req.query.storeId) {
    return next(new Error('StoreId is require'))
  }

  Store.findOne({_id: req.query.storeId}).select('+checkoutUniqueToken').exec((err, store) => {
    if (err) {
      return next(err);
    }

    if (store.retailer != req.session.user._id) {
      return res.status(403).json({
        error: 'PERMISSION_DENIED'
      })
    }

    res.json({
      token: store.checkoutUniqueToken
    })
  });
}

function getRevenue(req, res, next) {
  if (!req.query.retailerId) {
    return next(new Error('retailerId is require'))
  }

  Store.find(
    {retailer: req.query.retailerId},
    {storeName: 1, revenue: 1})
    .select('+revenue').exec((err, stores) => {

    if (err) {
      return next(err)
    }

    let revenue = {
      totalRevenue: {
        months: [],
        points: []
      },
      storesRevenue: {
        months: [],
        stores: [],
        points: []
      }
    };

    if (!stores || stores.length === 0) {
      return res.json(revenue)
    }

    let minDate = null;

    stores.forEach(storeData => {
      for (var i = 0, len = storeData.revenue.length; i < len; i++) {
        if (!minDate || +new Date(storeData.revenue[i].date) < +minDate) {
          minDate = new Date(storeData.revenue[i].date);
        }
      }
    });

    if (!minDate) {
      return res.json(revenue);
    }

    let firstDay = new Date(minDate.getFullYear(), minDate.getMonth(), 1);

    let today = new Date();
    while (firstDay < today) {
      revenue.totalRevenue.months.push(firstDay);
      revenue.storesRevenue.months.push(firstDay);
      firstDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 1);
    }

    let monthsLength = revenue.totalRevenue.months.length;

    revenue.totalRevenue.points = Array.apply(null, Array(monthsLength)).map(Number.prototype.valueOf, 0);

    for (var i = 0, len = stores.length; i < len; i++) {
      revenue.storesRevenue.points.push(Array.apply(null, Array(monthsLength)).map(Number.prototype.valueOf, 0));
    }

    stores.forEach((storeData, index) => {
      revenue.storesRevenue.stores.push(storeData.storeName);

      for (var i = 0, len = storeData.revenue.length; i < len; i++) {
        let storeRevenueDate = new Date(storeData.revenue[i].date);
        let storeRevenuePoints = +storeData.revenue[i].points;

        for (let j = revenue.totalRevenue.months.length; j >= 0; j--) {

          if (+storeRevenueDate >= +revenue.totalRevenue.months[j]) {
            revenue.totalRevenue.points[j] += storeRevenuePoints;
            revenue.storesRevenue.points[index][j] += storeRevenuePoints;
            break
          }

        }

      }

    });

    res.json(revenue);
  })
}

/**
 * @api {patch} /api/store/:cnpj
 * @apiName update
 * @apiGroup Store
 *
 * @apiDescritpion
 * Update a Store
 *
 * @apiParam {String} storeName Name for the Store.
 * @apiParam {String} cnpj Unique CNPJ for the Store.
 * @apiParam {String} managerReponsible Name of the Manager for the Store.
 * @apiParam {String} managerRegistration Register of the Manager for the Store.
 * @apiParam {String} phone Phone number of the Manager for the Store.
 *
 * @apiSuccess (200) {storeName} storeName Name for the Store.
 * @apiSuccess (200) {String} cnpj Unique CNPJ for the Store.
 * @apiSuccess (200) {String} managerReponsible Name of the Manager for the Store.
 * @apiSuccess (200) {String} managerRegistration Register of the Manager for the Store.
 * @apiSuccess (200) {String} phone Phone number of the Manager for the Store.
 */

function update(req, res, next) {
  if (!req.params.storeId) {
    return next(new Error('storeId is require for update'))
  }

  Store.findOne({_id: req.params.storeId}, (err, store) => {
    if (err) {
      return next(err);
    }
    if (!store) {
      return next(new Error('store is not found'))
    }

    if (req.body.cnpj) store.cnpj = req.body.cnpj;
    if (req.body.address) store.address = req.body.address;
    if (req.body.phone) store.phone = req.body.phone;
    if (req.body.storeName) store.storeName = req.body.storeName;
    if (req.body.description) store.description = req.body.description;
    if (req.body.manager) store.manager = req.body.manager;

    store.save(err => {
      if (err) {
        return next(err)
      }

      res.json(store);
    });
  });
}
