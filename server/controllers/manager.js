'use strict';

const Manager = require('../models/manager');
const Retailer = require('../models/retailer');
const Client = require('../models/client');
const Message = require('../models/message');

module.exports = {
  getBy,
  updateManager,
  getMessages,
  saveAnswer,
  changePassword,
  getPointsInSystem
};

function getBy(req, res, next) {
  if (req.query.managerId) {
    return getById(req, res, next);
  }

  var query = {};

  Manager.find(query, (err, manager) => {
    if (err || !manager) {
      return next(err);
    }

    res.json(manager);
  });
}

function updateManager(req, res, next) {
  if (!req.params.managerId) {
    return next(new Error('managerId is require for updateManager'))
  }

  Manager.findOne(
    {_id: req.params.managerId},
    (err, manager) => {
      if (err) {
        return next(err);
      }
      if (!manager) {
        return next(new Error('manager is not found'))
      }

      if (req.body.name) manager.name = req.body.name;
      if (req.body.email) manager.email = req.body.email;

      manager.save(err => {
        if (err) {
          return next(err)
        }

        res.json(manager);
      });
    });
}

function getById(req, res, next) {
  Manager.findById(req.query.managerId,
    (err, manager) => {
      if (err) {
        return next(err);
      }

      res.json(manager);
    });
}

function getMessages(req, res, next) {
  var query = {};

  if (req.query.type) query.type = req.query.type;

  Message.find(query,
    (err, messages) => {
      if (err) {
        return next(err);
      }

      res.json(messages);
    });
}

function saveAnswer(req, res, next) {
  if (!req.query.messageId) {
    return next('MessageId is required')
  }
  if (!req.body.answer) {
    return next('answer is required')
  }

  Message.findOneAndUpdate(
    {_id: req.query.messageId},
    {
      answer: req.body.answer,
      status: 'ANSWER'
    },
    {new: true},
    (err, messages) => {
      if (err) {
        return next(err);
      }

      res.json(messages);
    });
}

function changePassword(req, res, next) {
  if (!req.params.managerId) {
    return next(new Error('managerId is required'))
  }
  if (!req.body.password) {
    return next(new Error('Current password is required'))
  }
  if (!req.body.newPassword) {
    return next(new Error('New password is required'))
  }

  Manager.findOne({_id: req.params.managerId})
    .select('+password').exec((err, manager) => {
    if (err) {
      return next(err);
    }

    if (!manager) {
      return next(new Error('manager not found'));
    }

    manager.comparePassword(req.body.password, function (err, isMatch) {
      if (err) {
        return next(err)
      }

      if (!isMatch) {
        return res.status(403).send({
          error: "WRONG_PASSWORD"
        });
      }

      manager.password = req.body.newPassword;

      manager.save((err, manager) => {
        if (err) {
          return next(err)
        }
        res.json(manager);
      })
    });

  });
}

function getPointsInSystem(req, res, next) {
  var totalPoints = 0;
  Retailer.aggregate({
    $group: {
      _id: null,
      totalRetailerPoints: {$sum: "$points"}
    }
  }).exec()
    .then(pointsData => {
      if (pointsData && pointsData.length > 0) {
        totalPoints += pointsData[0].totalRetailerPoints;
      }

      return Client.aggregate({
        $group: {
          _id: null,
          totalPoints: {$sum: "$points"},
          totalPointsInVoucher: {$sum: "$pointsInVoucher"}
        }
      }).exec()
    })
    .then(pointsData => {
      if (pointsData && pointsData.length > 0) {
        totalPoints += pointsData[0].totalPoints;
        totalPoints += pointsData[0].totalPointsInVoucher;
      }

      res.json({
        value: totalPoints
      })
    })
    .catch(err => next(err))
}
