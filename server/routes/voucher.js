'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes');
let voucherCtrl = require('../controllers/voucher');
let config = require('../config');

router
  .post('/', voucherCtrl.create)
  .get('/:code', voucherCtrl.getVoucherByCode)
  .get('/client/:clientId', voucherCtrl.getVoucherByClient)
  .get('/store/:store', voucherCtrl.getVoucherByStore)
  .get('/isCanBeUsed/:code/:client/:retailer', voucherCtrl.isCanBeUsed)
  .patch('/:code', voucherCtrl.updateVoucherStatus)
  .patch('reward/:reward', voucherCtrl.emmitVoucherByReward)
  .put('/:code', voucherCtrl.update);

module.exports = router;
