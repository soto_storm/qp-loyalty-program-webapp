'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes:mail');
let mailCtrl = require('../controllers/mail');

router
  .post('/sendToClientsGroup', mailCtrl.sendToClientsGroup)
  .get('/confirmation/:clientId', mailCtrl.confirmClientEmail)
  .get('/forgotPassword', mailCtrl.forgotPassword)
  .get('/sendConfirmEmail', mailCtrl.sendConfirmEmail);

module.exports = router;
