'use strict';

let config = require('../config');
let router = require('express').Router();
let debug = require('debug')('qp:routes');
let indexCtrl = require('../controllers/index');
let ensureAuthenticated = require('../middleware/ensure_authenticated');

router
  .get('/health', indexCtrl.health)
  .get('/version', indexCtrl.version);

if (config.isDev) {
  router.get('/session', indexCtrl.session);
}

router.use('/auth', require('./auth'));
router.use('/goToCheckout', require('./checkout'));

//router.use('/api', ensureAuthenticated);

router
  .use('/api/client', require('./client'))
  .use('/api/retailer', require('./retailer'))
  .use('/api/voucher', require('./voucher'))
  .use('/api/store', require('./store'))
  .use('/api/place', require('./place'))
  .use('/api/manager', require('./manager'))
  .use('/api/reward', require('./reward'))
  .use('/api/mail', require('./mail'));

module.exports = router;
