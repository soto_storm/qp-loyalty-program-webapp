'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes');
let managerCtrl = require('../controllers/manager');

router
  .get('/', managerCtrl.getBy)
  .get('/getMessages', managerCtrl.getMessages)
  .post('/saveAnswer', managerCtrl.saveAnswer)
  .put('/:managerId', managerCtrl.updateManager)
  .post('/changePassword/:managerId', managerCtrl.changePassword)
  .get('/getPointsInSystem', managerCtrl.getPointsInSystem);

module.exports = router;
