'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes');
let passport = require('passport');
let authCtrl = require('../controllers/auth');

router
  .get('/logout', authCtrl.logout)
  .get('/current', authCtrl.getCurrentUser)
  .post('/client', authCtrl.client)
  .post('/retailer', authCtrl.retailer)
  .post('/manager', authCtrl.manager);

module.exports = router;
