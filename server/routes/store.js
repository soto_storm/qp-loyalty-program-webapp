'use strict';

let router = require('express').Router();
let storeCtrl = require('../controllers/store');

router
  .post('/', storeCtrl.create)
  .put('/:storeId', storeCtrl.update)
  .get('/', storeCtrl.getBy)
  .get('/getUniqueToken', storeCtrl.getUniqueToken)
  .get('/placesNearby', storeCtrl.getPlacesNearby)
  .get('/revenue', storeCtrl.getRevenue)
  .get('/getBestStores', storeCtrl.getBestStores);

module.exports = router;
