/* global . */
'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes');
let rewardCtrl = require('../controllers/reward');
let config = require('../config');

router
  .post('/', rewardCtrl.create)
  .put('/:rewardId', rewardCtrl.update)
  .get('/', rewardCtrl.getBy)
  .delete('/:reward', rewardCtrl.remove);

module.exports = router;
