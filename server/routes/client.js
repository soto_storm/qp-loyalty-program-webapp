'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes');
let multer = require('multer');
let config = require('../config');

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    let extension = file.originalname.split(".") ;
    extension = extension[extension.length -1];

    cb(null, 'qp' + Date.now() + '.' + extension);
  }
});

let upload = multer({ storage: storage });
let clientCtrl = require('../controllers/client');

router
  .post('/', clientCtrl.create)
  .post('/changePassword/:clientId', clientCtrl.changePassword)
  .post('/resetPassword', clientCtrl.resetPassword)
  .get('/', clientCtrl.getBy)
  .put('/:clientId', clientCtrl.update)
  .patch('/:cpf/points/update', clientCtrl.updatePoint);

module.exports = router;
