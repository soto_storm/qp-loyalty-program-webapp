'use strict';

let router = require('express').Router();
var Store = require('../models/store');

router
  .get('/', (req, res, next) => {
    if (!req.query.token) {
      next(new Error('Token is required'));
      return;
    }

    Store.findOne({checkoutUniqueToken: req.query.token}).exec()
      .then(store => {
        if (!store || !req.session || req.session.user._id != store.retailer) {
          return res.status(403).json({
            error: 'PERMISSION_DENIED'
          })
        }

        req.session.accessToCheckoutExpiredAt = +(new Date()) + (1000 * 60 * 60 * 24); // 1 day can access
        req.session.accessToCheckoutToken = req.query.token; // 1 day can access

        res.redirect('/checkout?token=' + req.session.accessToCheckoutToken);
      })
      .catch(err => {
        next(err)
      })

  })
  .get('/getData', (req, res, next) => {
    if (!req.query.token) {
      next(new Error('Token is required'));
      return;
    }

    Store.findOne({checkoutUniqueToken: req.query.token}).exec()
      .then(store => {
        if (!store || !req.session || req.session.user._id != store.retailer) {
          return res.status(403).json({
            error: 'PERMISSION_DENIED'
          })
        }

        res.json({
          storeId: store._id,
          retailerId: req.session.user._id
        })
      })
      .catch(err => {
        next(err)
      })
  });

module.exports = router;
