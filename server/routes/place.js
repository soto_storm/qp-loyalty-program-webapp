'use strict';

let router = require('express').Router();

let placeCtrl = require('../controllers/place');

router
  .get('/', placeCtrl.getBy);

module.exports = router;
