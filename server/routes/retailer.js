'use strict';

let router = require('express').Router();
let debug = require('debug')('qp:routes');
let multer = require('multer');
let config = require('../config');

let retailerCtrl = require('../controllers/retailer');

router
  .post('/', retailerCtrl.createRetailer)
  .get('/', retailerCtrl.getBy)
  .get('/bestRetailers', retailerCtrl.getBestRetailers)
  .put('/:retailerId', retailerCtrl.updateRetailer)
  .post('/rechargeDots/:retailerId', retailerCtrl.rechargeDots)
  .post('/changePassword/:retailerId', retailerCtrl.changePassword)
  .post('/support/:retailerId', retailerCtrl.addSupportMessage)
  .get('/exchangedRewardsToVouchers', retailerCtrl.exchangedRewardsToVouchers)
  .get('/lastActivity/:retailerId', retailerCtrl.getLastActivity);

module.exports = router;
