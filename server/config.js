'use strict';

const env = process.env.NODE_ENV;
const isDev = env === 'development';
const src = `${__dirname}/public/`;
const dest = `${__dirname}/dist/`;

const appPort = process.env.PORT ? process.env.PORT.trim() : (isDev ? 3000 : 3001);
const mongodbPort = process.env.MONGODB_PORT ? process.env.MONGODB_PORT.trim() : 27017;
const mongodbHost = process.env.MONGODB_HOST ? process.env.MONGODB_HOST.trim() : 'localhost';

let config = {

  isDev: isDev,
  env: env,
  port: appPort,

  salt: '$2a$05$kgGwMBu9DBx2QGRNTA7q9u',

  mongodb: {
    host: mongodbHost,
    port: mongodbPort,
    name: 'qp'
  },

  redis: {
    host: 'localhost',
    port: 6379
  },

  paths: {
    src: src,
    dest: dest,
    views: `${__dirname}/views/`,

    js: {
      src: `${src}js/`,
      dest: `${dest}js/`,
    },
    css: {
      src: `${src}css/`,
      dest: `${dest}css/`,
    }
  }

};

// MongoDB
config.mongodb.uri = `mongodb://${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.name}`;

// Auth
config.social = {
  facebook: {
    clientID: '1150118331715314',
    clientSecret: '41836a5852ae3392e611f466f56fb9eb',
    callbackURL: '/auth/facebook/callback'
  }
};

config.email = {
  mandrill: {
    domain: 'queropontos.com.br'
  }
};

config.session = {
  sessionAge: 3600 * 1000 * 24 //One day
};

module.exports = config;

