let supertest = require('supertest');
let chai = require('chai');
let app = require('../index.js');

global.app = app;
global.request = supertest(app);
global.expect = chai.expect;
