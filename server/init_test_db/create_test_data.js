'use strict';

var fs = require('fs');
const path = require('path');
var voucher_codes = require('voucher-code-generator');
let md5 = require('blueimp-md5');

var fileName = './test_data.json';
let testData = require(fileName);

let newTestData = {};

let startPartId = '575721d172e91884177e';
let lastPartId = 1200;
let phoneNumber = 12312312312;

let names = ['John', 'Bob', 'Clint', 'Jack', 'Oliver', 'Petro', 'Aaron', 'Liam', 'Logan', 'Aiden', 'Luke', 'Owen', 'Ryan'];
let cpf = ['03062801590', '10288487303', '90964697718', '59012034884', '15885317240', '14930588430', '72863337980', '37470732713', '43778732617', '97129333340', '07448719211', '13238596756', '38846237374'];
let roles = {
  'MANAGER': 'Manager',
  'RETAILER': 'Retailer',
  'CLIENT': 'Client'
};
let rewardTitles = ['Discount 20%', 'Discount 10%', 'Sale 50%', 'Second item as gift', 'Free drinks on Friday', 'Each third item as gift'];
let storesName = ['Adidas', 'Nike', 'MackDonald\'s', 'MamaMia', 'Colin', 'Mango', 'Salateira', 'Reabok', 'Clarcks', 'NewYork Shop'];
let cnpj = ['41461361000112', '27175421000100', '32227807000158', '17157436000191', '95668431000137', '25765655000181', '59027323000111', '83864657000124', '88581812000119', '22637939000102'];
let adresses = [{
  "address": "Marataízes, Caratinga - State of Minas Gerais, Brazil",
  "locationLat": -19.4704956,
  "locationLng": -42.45483279999996,
  "postalCode": "",
  "longCity": "Sanford",
  "shortCity": "Sanford",
  "longState": "Minas Gerais",
  "shortState": "MG",
  "country": "Brazil",
  "countryCode": "BR"
}, {
  "address": "R. Pontes Correia - Andaraí, Rio de Janeiro - State of Rio de Janeiro, Brazil",
  "locationLat": -22.9223427,
  "locationLng": -43.24597849999998,
  "postalCode": "",
  "longCity": "Rio de Janeiro",
  "shortCity": "Rio de Janeiro",
  "longState": "Espírito Santo",
  "shortState": "ES",
  "country": "Brazil",
  "countryCode": "BR"
}, {
  "address": "Rua Uruguai - Andaraí, Rio de Janeiro - State of Rio de Janeiro, Brazil",
  "locationLat": -22.9287554,
  "locationLng": -43.243535399999985,
  "postalCode": "",
  "longCity": "Rio de Janeiro",
  "shortCity": "Rio de Janeiro",
  "longState": "Espírito Santo",
  "shortState": "ES",
  "country": "Brazil",
  "countryCode": "BR"
}, {
  "address": "Rio de Janeiro - Glória - State of Rio de Janeiro, Brazil",
  "locationLat": -22.917499,
  "locationLng": -43.174357799999996,
  "postalCode": "",
  "longCity": "Rio de Janeiro",
  "shortCity": "Rio de Janeiro",
  "longState": "State of Rio de Janeiro",
  "shortState": "RJ",
  "country": "Brazil",
  "countryCode": "BR"
}, {
  "countryCode": "BR",
  "country": "Brazil",
  "shortState": "ES",
  "longState": "Espírito Santo",
  "shortCity": "São Paulo",
  "longCity": "São Paulo",
  "postalCode": "",
  "locationLng": -46.64023780000002,
  "locationLat": -23.5736228,
  "address": "Rua Vergueiro - Liberdade, São Paulo - State of São Paulo, Brazil"
}, {
  "countryCode": "BR",
  "country": "Brazil",
  "shortState": "ES",
  "longState": "Espírito Santo",
  "shortCity": "São Paulo",
  "longCity": "São Paulo",
  "postalCode": "",
  "locationLng": -46.65118849999999,
  "locationLat": -23.542441,
  "address": "Rua Dona Veridiana - Higienópolis, São Paulo - State of São Paulo, Brazil"
}, {
  "countryCode": "BR",
  "country": "Brazil",
  "shortState": "ES",
  "longState": "Espírito Santo",
  "shortCity": "São Paulo",
  "longCity": "São Paulo",
  "postalCode": "",
  "locationLng": -46.65456919999997,
  "locationLat": -23.55810499999999,
  "address": "Rua Dona Adma Jafet - Bela Vista, São Paulo - State of São Paulo, Brazil"
}, {
  "address": "Riacho Fundo I - QS 14, Brasilia - Federal District, Brazil",
  "locationLat": -15.8860794,
  "locationLng": -48.01390170000002,
  "country": "Brazil",
  "countryCode": "BR"
}, {
  "address": "Avenida Braz de Pina - Vila da Penha, Rio de Janeiro - State of Rio de Janeiro, Brazil",
  "locationLat": -22.8407847,
  "locationLng": -43.3039526,
  "country": "Brazil",
  "countryCode": "BR",
  "longCity": "Rio de Janeiro",
  "shortCity": "Rio de Janeiro"
}, {
  "address": "Avenida Braz de Pina - Vila Vitoria, Mogi das Cruzes - State of São Paulo, Brazil",
  "locationLat": -23.5305659,
  "locationLng": -46.20294309999997,
  "country": "Brazil",
  "countryCode": "BR",
  "longCity": "Mogi das Cruzes",
  "shortCity": "Mogi das Cruzes"
}];

console.log('managers');
newTestData.managers = createManagers(names, 3);
console.log('retailers');
newTestData.retailers = createRetailers(names, 5)
console.log('stores');
newTestData.stores = createStores(storesName);
console.log('rewards');
newTestData.rewards = createRewards(rewardTitles);
console.log('clients');
newTestData.clients = createClients(names);
console.log('places');
newTestData.places = createPlaces();
console.log('vouchers');
newTestData.vouchers = createVouchers();

fs.writeFile((path.join(__dirname, fileName)), JSON.stringify(newTestData), function (err) {
  console.log(JSON.stringify(testData))
});

function createManagers(namesOrigin, quantity) {
  // let names = clone(namesOrigin);
  // if (quantity) {
  //   names.splice(quantity);
  // }
  //
  // let managers = [];
  // names.forEach((name) => {
  //   let manager = {};
  //
  //   manager._id = getId();
  //   manager.name = name + ' ' + roles['MANAGER'];
  //   manager.email = name.toLowerCase() + '-r@mail.com';
  //   manager.retailers = [];
  //
  //   managers.push(manager)
  // });
  //
  // return managers;

  return [{
      _id: getId(),
      name: 'Quero Pontos RM',
      email: 'quero-pontos-rm@mail.com',
      password: md5('11111'),
      retailers: []
  }]
}

function createRetailers(namesOrigin, quantity) {
  let names = clone(namesOrigin);
  if (quantity) {
    names.splice(quantity);
  }

  let retailers = [];
  names.forEach((name) => {
    let retailer = {};

    retailer._id = getId();
    retailer.name = name + ' ' + roles['RETAILER'];
    retailer.email = name.toLowerCase() + '-r@mail.com';
    retailer.password = md5('11111');
    retailer.manager = getRandomIdFromModels(newTestData.managers);
    retailer.clients = [];
    retailer.stores = [];
    retailer.rewards = [];
    retailer.points = getRandomInt(50000, 500000, 1000);
    retailer.lastLogout = randomDate(new Date(2016, 3, 1), new Date());

    getModelsForId(newTestData.managers, retailer.manager).retailers.push(retailer._id);

    retailers.push(retailer)
  });

  return retailers;
}

function createStores(namesOrigin, quantity) {
  let names = clone(namesOrigin);
  if (quantity) {
    names.splice(quantity);
  }

  let stores = [];
  names.forEach((name, i) => {
    let store = {};

    store._id = getId();
    store.storeName = name;
    store.giveDotsAmount = 0;
    store.clientsActions = 0;
    store.description = name + '\'s description';
    store.cnpj = cnpj[i];
    store.clients = [];
    store.retailer = getRandomIdFromModels(newTestData.retailers);
    store.manager = getModelsForId(newTestData.retailers, store.retailer).manager;
    store.address = adresses[i];
    store.revenue = [];
    store.phone = getPhoneNumber();

    getModelsForId(newTestData.retailers, store.retailer).stores.push(store._id);

    stores.push(store)
  });

  return stores;
}

function createRewards(namesOrigin, quantity) {
  let names = clone(namesOrigin);

  if (quantity) {
    names.splice(quantity);
  }

  let rewards = [];
  names.forEach((name, i) => {
    let reward = {};

    reward._id = getId();
    reward.title = name;
    reward.retailer = getRandomIdFromModels(newTestData.retailers);
    reward.manager = getModelsForId(newTestData.retailers, reward.retailer).manager;
    reward.points = getRandomInt(100, 500, 10);

    getModelsForId(newTestData.retailers, reward.retailer).rewards.push(reward._id);

    rewards.push(reward)
  });

  return rewards;
}

function createClients(namesOrigin, quantity) {
  let names = clone(namesOrigin);

  if (quantity) {
    names.splice(quantity);
  }

  let clients = [];
  names.forEach((name, i) => {
    let client = {};

    client._id = getId();
    client.name = name + ' ' + roles['CLIENT'];
    client.email = name.toLowerCase() + '-c@mail.com';
    client.emailConfirm = true;
    client.storeVisitedCount = 0;
    client.password = md5('11111');
    client.cpf = cpf[i];
    client.gender = getRandomInt(1, 3) > 1 ? 'F' : 'M';
    client.birthday = randomDate(new Date(1970, 3, 1), new Date(2000, 1, 1));
    client.hasPassword = true;
    client.vouchers = {
      emitted: [],
      concluded: [],
      expired: []
    };
    client.points = getRandomInt(2000, 5000, 10);
    client.pointsInVouchers = getRandomInt(1000, 4000, 10);
    client.pointsOfRetailers = {};
    client.lastLogout = randomDate(new Date(2016, 3, 1), new Date());

    clients.push(client)
  });

  return clients;
}

function createPlaces() {
  let places = [];
  newTestData.clients.forEach(client => {

    let clientPoints = client.points + client.pointsInVouchers;

    while (clientPoints > 0) {

      let updateDots = getRandomInt(500, 2000, 10);
      let randomDateValue = randomDate(new Date(2016, 3, 1), new Date());
      let store = getRandomIdFromModels(newTestData.stores, true);
      let retailer = getModelsForId(newTestData.retailers, store.retailer);

      if ((clientPoints - updateDots) > 0) {
        client.pointsOfRetailers[store.retailer] = (client.pointsOfRetailers[store.retailer] || 0) + updateDots;
        store.revenue.push({
          date: randomDateValue,
          points: updateDots
        });

        store.giveDotsAmount = store.giveDotsAmount + updateDots;
        store.lastReleaseDate = randomDateValue;
        addPlace(places, randomDateValue, client, store, retailer, updateDots, 'UPDATE_DOTS');

        clientPoints -= updateDots;

      } else {
        client.pointsOfRetailers[store.retailer] = (client.pointsOfRetailers[store.retailer] || 0) + clientPoints;
        store.revenue.push({
          date: randomDateValue,
          points: clientPoints
        });

        store.giveDotsAmount = store.giveDotsAmount + clientPoints;
        store.lastReleaseDate = randomDateValue;
        addPlace(places, randomDateValue, client, store, retailer, clientPoints, 'UPDATE_DOTS');

        clientPoints = 0;

      }

      client.storeVisitedCount++;
      if (retailer.clients.indexOf(client._id) === -1) {
        retailer.clients.push(client._id)
      }

      if (store.clients.indexOf(client._id) === -1) {
        store.clients.push(client._id)
      }

      store.clientsActions++;

    }
  });

  return places;
}

function createVouchers() {
  let vouchers = [];
  newTestData.clients.forEach(client => {

    let pointsInVouchers = client.pointsInVouchers;

    while (pointsInVouchers > 0) {

      let updateDischarge = getRandomInt(300, 1000, 10);
      let randomDateValue = randomDate(new Date(2016, 3, 1), new Date());

      if ((pointsInVouchers - updateDischarge) > 0) {
        let reward = getRandomIdFromModels(newTestData.rewards, true);
        let retailer = getModelsForId(newTestData.retailers, reward.retailer);

        let voucher = addVoucher(randomDateValue, client, reward, retailer, updateDischarge, 1);

        client.vouchers.emitted.push(voucher._id);

        pointsInVouchers -= updateDischarge;

      } else {
        let reward = getRandomIdFromModels(newTestData.rewards, true);
        let retailer = getModelsForId(newTestData.retailers, reward.retailer);

        let voucher = addVoucher(randomDateValue, client, reward, retailer, pointsInVouchers, 1);

        client.vouchers.emitted.push(voucher._id);

        pointsInVouchers = 0;

      }

    }

    let randomExpiredVoucher = getRandomInt(2, 7);
    let randomConcludedVoucher = getRandomInt(5, 15);

    for(let i = 0; i < randomExpiredVoucher; i++){
      let randomDateValue = randomDate(new Date(2016, 3, 1), new Date());
      let price = getRandomInt(3000, 1000, 10);
      let reward = getRandomIdFromModels(newTestData.rewards, true);
      let retailer = getModelsForId(newTestData.retailers, reward.retailer);

      let voucher = addVoucher(randomDateValue, client, reward, retailer, price, 3);

      client.vouchers.expired.push(voucher._id);
    }

    for(let i = 0; i < randomConcludedVoucher; i++){
      let randomDateValue = randomDate(new Date(2016, 3, 1), new Date());
      let price = getRandomInt(3000, 1000, 10);
      let reward = getRandomIdFromModels(newTestData.rewards, true);
      let retailer = getModelsForId(newTestData.retailers, reward.retailer);

      if(retailer.stores.length === 0){
        continue
      }

      let voucher = addVoucher(randomDateValue, client, reward, retailer, price, 2);

      client.vouchers.concluded.push(voucher._id);
    }
  });

  return vouchers;

  function addVoucher(date, client, reward, retailer, updateDots, status) {
    let voucher = {
      _id: getId(),
      createdAt: date,
      fromEmail: false,
      points: updateDots,
      client: client._id,
      retailer: retailer._id,
      reward: reward._id,
      title: reward.title,
      code: generatingVoucherCode(client.vouchers),
      status: status
    };

    if(status === 2){
      let store = getRandomStoreForRetailer(retailer);

      if (store.clients.indexOf(client._id) === -1) {
        store.clients.push(client._id)
      }

      client.storeVisitedCount++;
      addPlace(newTestData.places, date, client, store, retailer, updateDots, 'USE_VOUCHER');
    }
    vouchers.push(voucher);
    return voucher
  }
}

function addPlace(places, date, client, store, retailer, updateDots, type) {
  places.push({
    _id: getId(),
    createdAt: date,
    client: client._id,
    store: store._id,
    retailer: retailer._id,
    points: updateDots,
    type: type
  })
}

//--------------------------------------------------------------------

function getId() {
  return startPartId + lastPartId++;
}

function getRandomIdFromModels(array, needAll) {
  let randomIndex = Math.floor(Math.random() * (array.length));

  if (needAll) {
    return array[randomIndex];
  } else {
    return array[randomIndex]._id;
  }

}

function getRandomStoreForRetailer(retailer) {
  let randomIndex = Math.floor(Math.random() * (retailer.stores.length));
  let store = getModelsForId(newTestData.stores, retailer.stores[randomIndex]);

  return store;
}

function getModelsForId(array, id) {
  for (let i = 0, len = array.length; i < len; i++) {
    if (array[i]._id == id) {
      return array[i]
    }
  }
}

function getRandomInt(min, max, roundTo) {
  let int = Math.floor(Math.random() * (max - min)) + min;

  if (roundTo) {
    int = Math.round(int / roundTo) * roundTo
  }
  return int;
}

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function getPhoneNumber() {
  return phoneNumber++;
}

function clone(obj) {
  var copy;

  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) return obj;

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (var i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
    }
    return copy;
  }

  throw new Error("Unable to copy obj! Its type isn't supported.");
}

function generatingVoucherCode(vouchers) {
  let code = voucher_codes.generate({
    pattern: "###-###-###",
    charset: "0123456789"
  })[0];

  let ifExistInExpired = vouchers.expired.indexOf(code.toString()) >= 0;
  let ifExistInConcluded = vouchers.concluded.indexOf(code.toString()) >= 0;
  let ifExistInEmitted = vouchers.emitted.indexOf(code.toString()) >= 0;

  if(ifExistInExpired || ifExistInConcluded || ifExistInEmitted){
    generatingVoucherCode(vouchers)
  }

  return code;
}
