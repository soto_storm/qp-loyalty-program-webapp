'use strict';

const Manager = require('../models/manager');
const Retailer = require('../models/retailer');
const Store = require('../models/store');
const Reward = require('../models/reward');
const Client = require('../models/client');
const Voucher = require('../models/voucher');
const Place = require('../models/place');
const testData = require('./test_data.json');

let async = require('async');
let mongoose = require('mongoose');
var config = require('../config');

mongoose.set('debug', true);

async.series([
  open,
  dropDatabase,
  createManagers,
  createRetailers,
  createStores,
  createRewards,
  createClients,
  createPlaces,
  createVouchers
], function (err) {
  mongoose.disconnect();
  process.exit(err ? 255 : 0);

});

function open(callback) {
  mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
  var db = mongoose.connection.db;
  db.dropDatabase(callback);
}

function createManagers(callback) {
  var managers = testData.managers;

  async.each(managers, function (managerData, callback) {
    var manager = new Manager(managerData);
    manager.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}

function createRetailers(callback) {
  var retailers = testData.retailers;

  async.each(retailers, function (retailerData, callback) {
    var retailer = new Retailer(retailerData);
    retailer.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}

function createStores(callback) {
  var stores = testData.stores;

  async.each(stores, function (storeData, callback) {
    var store = new Store(storeData);
    store.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}

function createRewards(callback) {
  var rewards = testData.rewards;

  async.each(rewards, function (rewardData, callback) {
    var reward = new Reward(rewardData);
    reward.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}

function createClients(callback) {
  var clients = testData.clients;

  async.each(clients, function (clientData, callback) {
    var client = new Client(clientData);
    client.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}

function createPlaces(callback) {
  var places = testData.places;

  async.each(places, function (placeData, callback) {
    var place = new Place(placeData);
    place.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}

function createVouchers(callback) {
  var vouchers = testData.vouchers;

  async.each(vouchers, function (voucherData, callback) {
    var voucher = new Voucher(voucherData);
    voucher.save(function () {
      setTimeout(callback, 500)
    })
  }, callback);
}
