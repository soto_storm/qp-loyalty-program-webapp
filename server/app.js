'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const compression = require('compression');
const cors = require('cors');
const morgan = require('morgan');
const config = require('./config');
const logger = require('./middleware/logger.js');
const ensureAuthenticated = require('./middleware/ensure_authenticated');
const ensureCheckoutAccess = require('./middleware/ensure_checkout_access');
const debug = require('debug')('qp:server');
const app = express();
const path = require('path');


// Enable cross-origin resource sharing
// app.use(cors({credentials: true, origin: true}));

if (process.env.PROXY) {/*If need to use PROXY for api. Ip address set in package.gson*/
  let proxy = require('http-proxy-middleware');
  app.use('/api', proxy({target: 'http://' + process.env.PROXY, changeOrigin: true}));
}

app.set('json spaces', 4);
app.set('port', config.port);

// Body parsing
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Gzip compression
app.use(compression());

app.use((req, res, next) => {
  delete req.body.id;
  next();
});

// Log
app.use(morgan('common', {
  stream: {
    write: (message) => {
      logger.info(message);
    },
  },
}));

// Setup static files

require('./middleware/session')(app);
require('./middleware/security')(app);

app.use("/retailer", ensureAuthenticated('retailer'));
app.use("/retailer", express.static(path.join(__dirname, '../public/retailer/static')));

app.use("/client", ensureAuthenticated('client'));
app.use("/client", express.static(path.join(__dirname, '../public/client/static')));

app.use("/manager", ensureAuthenticated('manager'));
app.use("/manager", express.static(path.join(__dirname, '../public/manager/static')));

app.use("/checkout", ensureCheckoutAccess);
app.use("/checkout", express.static(path.join(__dirname, '../public/checkout/static')));

app.use("/authentication", express.static(path.join(__dirname, '../public/authentication/static')));

app.get('/', (req, res, next) => {
  if (req.session.role === 'client') {
    return res.redirect('/client');
  } else if (req.session.role === 'retailer') {
    return res.redirect('/retailer');
  } else {
    return res.redirect('/authentication');
  }

});

app.use('/', require('./routes'));

require('./middleware/error_handling')(app);

module.exports = app;
