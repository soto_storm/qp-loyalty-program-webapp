#!/bin/bash

case "$(uname -s)" in
  Darwin)
    echo 'Mac OS X'
  ;;
  Linux)
    echo 'Linux'
  ;;
  *)
    echo 'Unsupported OS'
    exit 1
esac

case "$1" in
  simulate)
    export DEBUG=qp:*

    rm -r public/bower_components/
    rm -r dist/
    rm ./public/assets/styles/bootstrap.css
    rm ./public/assets/styles/app.css

    npm run build

    node dist/server/bin/www
  ;;
  karma)
    ./node_modules/karma/bin/karma start --browsers PhantomJS ./test/angular/karma.conf.js
  ;;
  test)
    export DEBUG=qp:*
    ./node_modules/istanbul/lib/cli.js cover ./node_modules/mocha/bin/_mocha test/e2e/*
  ;;
  start)
    export DEBUG=qp:*
    npm-run-all --parallel nodemon-server gulp-watch #karma
  ;;
  build)
    bower install
    rm -rf dist/
    gulp
  ;;

  *)
    echo "Usage: {simulate|karma|test|start|build}"
    exit 1
  ;;
esac
