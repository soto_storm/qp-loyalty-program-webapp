'use strict';

const gulp = require('gulp'),
  gutil = require('gulp-util'),
  concat = require('gulp-concat'),
  cssnano = require('gulp-cssnano'),
  htmlreplace = require('gulp-html-replace'),
  uglify = require('gulp-uglify'),
  eslint = require('gulp-eslint'),
  livereload = require('gulp-livereload'),
  autoprefixer = require('autoprefixer-stylus'),
  wiredep = require('wiredep'),
  stylus = require('gulp-stylus'),
  ngAnnotate = require('gulp-ng-annotate'),
  bootstrap = require('bootstrap-styl'),
  changed = require('gulp-changed'),
  mergeStream = require('merge-stream');

let WIREDEP_CONFIG_CLIENT = require('./wiredep_conf/wiredep_client');
let WIREDEP_CONFIG_RETAILER = require('./wiredep_conf/wiredep_retailer');
let WIREDEP_CONFIG_CHECKOUT = require('./wiredep_conf/wiredep_checkout');
let WIREDEP_CONFIG_AUTHENTICATION = require('./wiredep_conf/wiredep_authentication');
let WIREDEP_CONFIG_MANAGER = require('./wiredep_conf/wiredep_manager');

WIREDEP_CONFIG_CLIENT.bowerJson = require('./public/client/bower.json');
WIREDEP_CONFIG_RETAILER.bowerJson = require('./public/retailer/bower.json');
WIREDEP_CONFIG_CHECKOUT.bowerJson = require('./public/checkout/bower.json');
WIREDEP_CONFIG_AUTHENTICATION.bowerJson = require('./public/authentication/bower.json');
WIREDEP_CONFIG_MANAGER.bowerJson = require('./public/manager/bower.json');

const DEPENDENCIES_CLIENT = wiredep(WIREDEP_CONFIG_CLIENT);
const DEPENDENCIES_RETAILER = wiredep(WIREDEP_CONFIG_RETAILER);
const DEPENDENCIES_CHECKOUT = wiredep(WIREDEP_CONFIG_CHECKOUT);
const DEPENDENCIES_MANAGER = wiredep(WIREDEP_CONFIG_MANAGER);
const DEPENDENCIES_AUTHENTICATION = wiredep(WIREDEP_CONFIG_AUTHENTICATION);

const REV = `${new Date().getTime()}`;

gulp.task('stylus:custom', () => {
  let config = {
    use: [autoprefixer()]
  };

  let retailer = gulp.src('public/retailer/static/assets/styles/app.styl')
    .pipe(changed('public/retailer/static/assets/styles/**/*.*'))
    .pipe(stylus(config))
    .pipe(gulp.dest('public/retailer/static/assets/styles/'))
    .pipe(livereload());

  let client = gulp.src('public/client/static/assets/styles/app.styl')
    .pipe(changed('public/client/static/assets/styles/**/*.*'))
    .pipe(stylus(config))
    .pipe(gulp.dest('public/client/static/assets/styles/'))
    .pipe(livereload());

  let checkout = gulp.src('public/checkout/static/assets/styles/app.styl')
    .pipe(changed('public/checkout/static/assets/styles/**/*.*'))
    .pipe(stylus(config))
    .pipe(gulp.dest('public/checkout/static/assets/styles/'))
    .pipe(livereload());

  let authentication = gulp.src('public/authentication/static/assets/styles/app.styl')
    .pipe(changed('public/authentication/static/assets/styles/**/*.*'))
    .pipe(stylus(config))
    .pipe(gulp.dest('public/authentication/static/assets/styles/'))
    .pipe(livereload());

  let manager = gulp.src('public/manager/static/assets/styles/app.styl')
    .pipe(changed('public/manager/static/assets/styles/**/*.*'))
    .pipe(stylus(config))
    .pipe(gulp.dest('public/manager/static/assets/styles/'))
    .pipe(livereload());

  return mergeStream(retailer, client, checkout, authentication, manager);
});

gulp.task('bower', function () {

  var retailer = gulp.src('public/retailer/static/index.html')
    .pipe(changed('public/retailer/static'))
    .pipe(wiredep.stream(WIREDEP_CONFIG_RETAILER))
    .pipe(gulp.dest('public/retailer/static'));

  var client = gulp.src('public/client/static/index.html')
    .pipe(changed('public/client/static'))
    .pipe(wiredep.stream(WIREDEP_CONFIG_CLIENT))
    .pipe(gulp.dest('public/client/static'));

  var checkout = gulp.src('public/checkout/static/index.html')
    .pipe(changed('public/checkout/static'))
    .pipe(wiredep.stream(WIREDEP_CONFIG_CHECKOUT))
    .pipe(gulp.dest('public/checkout/static'));

  var authentication = gulp.src('public/authentication/static/index.html')
    .pipe(changed('public/authentication/static'))
    .pipe(wiredep.stream(WIREDEP_CONFIG_AUTHENTICATION))
    .pipe(gulp.dest('public/authentication/static'));

  var manager = gulp.src('public/manager/static/index.html')
    .pipe(changed('public/manager/static'))
    .pipe(wiredep.stream(WIREDEP_CONFIG_MANAGER))
    .pipe(gulp.dest('public/manager/static'));

  return mergeStream(retailer, client, checkout, authentication, manager);
});

gulp.task('stylus:bootstrap', () => {
  let config = {
    use: [bootstrap(), autoprefixer()]
  };

  let retailer = gulp.src('public/retailer/static/assets/styles/bootstrap.styl')
    .pipe(stylus(config))
    .pipe(gulp.dest('public/retailer/static/assets/styles/'))
    .pipe(livereload());

  let client = gulp.src('public/client/static/assets/styles/bootstrap.styl')
    .pipe(stylus(config))
    .pipe(gulp.dest('public/client/static/assets/styles/'))
    .pipe(livereload());

  let checkout = gulp.src('public/checkout/static/assets/styles/bootstrap.styl')
    .pipe(stylus(config))
    .pipe(gulp.dest('public/checkout/static/assets/styles/'))
    .pipe(livereload());

  let authentication = gulp.src('public/authentication/static/assets/styles/bootstrap.styl')
    .pipe(stylus(config))
    .pipe(gulp.dest('public/authentication/static/assets/styles/'))
    .pipe(livereload());

  let manager = gulp.src('public/manager/static/assets/styles/bootstrap.styl')
    .pipe(stylus(config))
    .pipe(gulp.dest('public/manager/static/assets/styles/'))
    .pipe(livereload());

  return mergeStream(retailer, client, checkout, authentication, manager);

});

gulp.task('watch', ['stylus:custom', 'stylus:bootstrap'], () => {


  livereload.listen();
  gulp.watch('public/retailer/static/assets/styles/**/*.styl', ['stylus:custom']);
  gulp.watch('public/client/static/assets/styles/**/*.styl', ['stylus:custom']);
  gulp.watch('public/checkout/static/assets/styles/**/*.styl', ['stylus:custom']);
  gulp.watch('public/authentication/static/assets/styles/**/*.styl', ['stylus:custom']);
  gulp.watch('public/manager/static/assets/styles/**/*.styl', ['stylus:custom']);

  gulp.watch([
    'public/retailer/static/assets/styles/bootstrap.styl',
    'public/retailer/static/assets/styles/variables.styl'
  ], ['stylus:bootstrap']);
  gulp.watch([
    'public/client/static/assets/styles/bootstrap.styl',
    'public/client/static/assets/styles/variables.styl'
  ], ['stylus:bootstrap']);
  gulp.watch([
    'public/checkout/static/assets/styles/bootstrap.styl',
    'public/checkout/static/assets/styles/variables.styl'
  ], ['stylus:bootstrap']);
  gulp.watch([
    'public/authentication/static/assets/styles/bootstrap.styl',
    'public/authentication/static/assets/styles/variables.styl'
  ], ['stylus:bootstrap']);
  gulp.watch([
    'public/manager/static/assets/styles/bootstrap.styl',
    'public/manager/static/assets/styles/variables.styl'
  ], ['stylus:bootstrap']);

});

gulp.task('htmlreplace', ['bower'], () => {

  let retailer = gulp.src('public/retailer/static/index.html')
    .pipe(changed('dist/public/retailer/static'))
    .pipe(htmlreplace({
      js: `app/vendors.min.js?${REV}`,
      angular: `app/app.min.js?${REV}`,
      css: `assets/styles/app.min.css?${REV}`,
      cssVendors: `assets/styles/vendors.min.css?${REV}`,
      livereload: ``
    }))
    .pipe(gulp.dest('dist/public/retailer/static'));

  let client = gulp.src('public/client/static/index.html')
    .pipe(changed('dist/public/client/static'))
    .pipe(htmlreplace({
      js: `app/vendors.min.js?${REV}`,
      angular: `app/app.min.js?${REV}`,
      css: `assets/styles/app.min.css?${REV}`,
      cssVendors: `assets/styles/vendors.min.css?${REV}`,
      livereload: ``
    }))
    .pipe(gulp.dest('dist/public/client/static'));

  let checkout = gulp.src('public/checkout/static/index.html')
    .pipe(changed('dist/public/checkout/static'))
    .pipe(htmlreplace({
      js: `app/vendors.min.js?${REV}`,
      angular: `app/app.min.js?${REV}`,
      css: `assets/styles/app.min.css?${REV}`,
      cssVendors: `assets/styles/vendors.min.css?${REV}`,
      livereload: ``
    }))
    .pipe(gulp.dest('dist/public/checkout/static'));

  let authentication = gulp.src('public/authentication/static/index.html')
    .pipe(changed('dist/public/authentication/static'))
    .pipe(htmlreplace({
      js: `app/vendors.min.js?${REV}`,
      angular: `app/app.min.js?${REV}`,
      css: `assets/styles/app.min.css?${REV}`,
      cssVendors: `assets/styles/vendors.min.css?${REV}`,
      livereload: ``
    }))
    .pipe(gulp.dest('dist/public/authentication/static'));

  let manager = gulp.src('public/manager/static/index.html')
    .pipe(changed('dist/public/manager/static'))
    .pipe(htmlreplace({
      js: `app/vendors.min.js?${REV}`,
      angular: `app/app.min.js?${REV}`,
      css: `assets/styles/app.min.css?${REV}`,
      cssVendors: `assets/styles/vendors.min.css?${REV}`,
      livereload: ``
    }))
    .pipe(gulp.dest('dist/public/manager/static'));

  return mergeStream(retailer, client, checkout, authentication, manager);

});

gulp.task('css', ['stylus:custom', 'stylus:bootstrap'], () => {

  let retailer = gulp.src([
    'public/retailer/static/assets/styles/bootstrap.css',
    'public/retailer/static/assets/styles/app.css'
  ])
    .pipe(changed('dist/public/retailer/static/assets/styles/'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('dist/public/retailer/static/assets/styles/'));

  let client = gulp.src([
    'public/client/static/assets/styles/bootstrap.css',
    'public/client/static/assets/styles/app.css'
  ])
    .pipe(changed('dist/public/client/static/assets/styles/'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('dist/public/client/static/assets/styles/'));

  let checkout = gulp.src([
    'public/checkout/static/assets/styles/bootstrap.css',
    'public/checkout/static/assets/styles/app.css'
  ])
    .pipe(changed('dist/public/checkout/static/assets/styles/'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('dist/public/checkout/static/assets/styles/'));

  let authentication = gulp.src([
    'public/authentication/static/assets/styles/bootstrap.css',
    'public/authentication/static/assets/styles/app.css'
  ])
    .pipe(changed('dist/public/authentication/static/assets/styles/'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('dist/public/authentication/static/assets/styles/'));

  let manager = gulp.src([
    'public/manager/static/assets/styles/bootstrap.css',
    'public/manager/static/assets/styles/app.css'
  ])
    .pipe(changed('dist/public/manager/static/assets/styles/'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('dist/public/manager/static/assets/styles/'));

  return mergeStream(retailer, client, checkout, authentication, manager);

});

gulp.task('css:vendors', function () {

  var streams = [];

  let retailer = gulp.src(DEPENDENCIES_RETAILER.css)
    .pipe(changed('dist/public/retailer/static/assets/styles/'))
    .pipe(concat('vendors.min.css'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/retailer/static/assets/styles/'));
  streams.push(retailer);

  let client = gulp.src(DEPENDENCIES_CLIENT.css)
    .pipe(changed('dist/public/client/static/assets/styles/'))
    .pipe(concat('vendors.min.css'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/client/static/assets/styles/'));
  streams.push(client);

  let manager = gulp.src(DEPENDENCIES_MANAGER.css)
    .pipe(changed('dist/public/manager/static/assets/styles/'))
    .pipe(concat('vendors.min.css'))
    .pipe(cssnano({zindex: false}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/manager/static/assets/styles/'));
  streams.push(manager);

  if (DEPENDENCIES_CHECKOUT.css) {
    var checkout = gulp.src(DEPENDENCIES_CHECKOUT.css)
      .pipe(changed('dist/public/checkout/static/assets/styles/'))
      .pipe(concat('vendors.min.css'))
      .pipe(cssnano({zindex: false}).on('error', gutil.log))
      .pipe(gulp.dest('dist/public/checkout/static/assets/styles/'));
    streams.push(checkout);
  }

  if (DEPENDENCIES_AUTHENTICATION.css) {
    var authentication = gulp.src(DEPENDENCIES_AUTHENTICATION.css)
      .pipe(changed('dist/public/authentication/static/assets/styles/'))
      .pipe(concat('vendors.min.css'))
      .pipe(cssnano({zindex: false}).on('error', gutil.log))
      .pipe(gulp.dest('dist/public/authentication/static/assets/styles/'));
    streams.push(authentication);
  }

  return mergeStream.apply(null, streams);
});

gulp.task('js:app', function () {

  var retailer = gulp.src([
    'public/retailer/static/app/**/*.lib.vendor.js',
    'public/retailer/static/app/**/*.vendor.js',
    'public/retailer/static/app/**/*.mdl.js',
    'public/retailer/static/app/*.mdl.js',
    'public/retailer/static/app/*!(mdl).js',
    'public/retailer/static/app/**/*!(mdl).js'
  ])
    .pipe(changed('dist/public/retailer/static/app/'))
    .pipe(concat('app.min.js', {newLine: ';'}).on('error', gutil.log))
    .pipe(ngAnnotate())
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/retailer/static/app/'))
    .pipe(livereload());

  var client = gulp.src([
    'public/client/static/app/**/*.lib.vendor.js',
    'public/client/static/app/**/*.vendor.js',
    'public/client/static/app/**/*.mdl.js',
    'public/client/static/app/*.js',
    'public/client/static/app/**/*!(mdl).js'
  ])
    .pipe(changed('dist/public/client/static/app/'))
    .pipe(concat('app.min.js', {newLine: ';'}).on('error', gutil.log))
    .pipe(ngAnnotate())
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/client/static/app/'))
    .pipe(livereload());

  var checkout = gulp.src([
    'public/checkout/static/app/*.js',
    'public/checkout/static/app/**/*.js'
  ])
    .pipe(changed('dist/public/checkout/static/app/'))
    .pipe(concat('app.min.js', {newLine: ';'}).on('error', gutil.log))
    .pipe(ngAnnotate())
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/checkout/static/app/'))
    .pipe(livereload());

  var authentication = gulp.src([
    'public/authentication/static/app/**/*.lib.vendor.js',
    'public/authentication/static/app/**/*.vendor.js',
    'public/authentication/static/app/**/*.mdl.js',
    'public/authentication/static/app/*.js',
    'public/authentication/static/app/**/*!(mdl).js'
  ])
    .pipe(changed('dist/public/authentication/static/app/'))
    .pipe(concat('app.min.js', {newLine: ';'}).on('error', gutil.log))
    .pipe(ngAnnotate())
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/authentication/static/app/'))
    .pipe(livereload());

  var manager = gulp.src([
    'public/manager/static/app/**/*.lib.vendor.js',
    'public/manager/static/app/**/*.vendor.js',
    'public/manager/static/app/**/*.mdl.js',
    'public/manager/static/app/*.js',
    'public/manager/static/app/**/*!(mdl).js'
  ])
    .pipe(changed('dist/public/manager/static/app/'))
    .pipe(concat('app.min.js', {newLine: ';'}).on('error', gutil.log))
    .pipe(ngAnnotate())
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/manager/static/app/'))
    .pipe(livereload());

  return mergeStream(retailer, client, checkout, authentication, manager);

});

gulp.task('js:angular', ['htmlreplace'], function () {
  var streams = [];

  var retailer = gulp.src(DEPENDENCIES_RETAILER.js)
    .pipe(changed('dist/public/retailer/static/app/'))
    .pipe(concat('vendors.min.js'))
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/retailer/static/app/'))
    .pipe(livereload());
  streams.push(retailer);

  var client = gulp.src(DEPENDENCIES_CLIENT.js)
    .pipe(changed('dist/public/client/static/app/'))
    .pipe(concat('vendors.min.js'))
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/client/static/app/'))
    .pipe(livereload());
  streams.push(client);

  var manager = gulp.src(DEPENDENCIES_MANAGER.js)
    .pipe(changed('dist/public/manager/static/app/'))
    .pipe(concat('vendors.min.js'))
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(gulp.dest('dist/public/manager/static/app/'))
    .pipe(livereload());
  streams.push(manager);

  if (DEPENDENCIES_CHECKOUT.js) {
    var checkout = gulp.src(DEPENDENCIES_CHECKOUT.js)
      .pipe(changed('dist/public/checkout/static/app/'))
      .pipe(concat('vendors.min.js'))
      .pipe(uglify({mangle: true}).on('error', gutil.log))
      .pipe(gulp.dest('dist/public/checkout/static/app/'))
      .pipe(livereload());
    streams.push(checkout);
  }

  if (DEPENDENCIES_AUTHENTICATION.js) {
    var authentication = gulp.src(DEPENDENCIES_AUTHENTICATION.js)
      .pipe(changed('dist/public/authentication/static/app/'))
      .pipe(concat('vendors.min.js'))
      .pipe(uglify({mangle: true}).on('error', gutil.log))
      .pipe(gulp.dest('dist/public/authentication/static/app/'))
      .pipe(livereload());
    streams.push(authentication);
  }

  return mergeStream.apply(null, streams);
});

gulp.task('copy:build', function () {
  gulp.src('package.json').pipe(gulp.dest('dist'));
  gulp.src('node_modules/**').pipe(gulp.dest('dist/node_modules/'));
  gulp.src(['server/**', '!server/init_test_db', '!server/init_test_db/**']).pipe(gulp.dest('dist/server/'));

  gulp.src('public/retailer/static/assets/fonts/**').pipe(gulp.dest('dist/public/retailer/static/assets/fonts'));
  gulp.src('public/client/static/assets/fonts/**').pipe(gulp.dest('dist/public/client/static/assets/fonts'));
  gulp.src('public/checkout/static/assets/fonts/**').pipe(gulp.dest('dist/public/checkout/static/assets/fonts'));
  gulp.src('public/authentication/static/assets/fonts/**').pipe(gulp.dest('dist/public/authentication/static/assets/fonts'));
  gulp.src('public/manager/static/assets/fonts/**').pipe(gulp.dest('dist/public/manager/static/assets/fonts'));

  gulp.src('public/retailer/static/app/**/*.html').pipe(gulp.dest('dist/public/retailer/static/app'));
  gulp.src('public/client/static/app/**/*.html').pipe(gulp.dest('dist/public/client/static/app'));
  gulp.src('public/checkout/static/app/**/*.html').pipe(gulp.dest('dist/public/checkout/static/app'));
  gulp.src('public/authentication/static/app/**/*.html').pipe(gulp.dest('dist/public/authentication/static/app'));
  gulp.src('public/manager/static/app/**/*.html').pipe(gulp.dest('dist/public/manager/static/app'));

  gulp.src('public/retailer/static/app/**/*.json').pipe(gulp.dest('dist/public/retailer/static/app'));
  gulp.src('public/client/static/app/**/*.json').pipe(gulp.dest('dist/public/client/static/app'));
  gulp.src('public/checkout/static/app/**/*.json').pipe(gulp.dest('dist/public/checkout/static/app'));
  gulp.src('public/authentication/static/app/**/*.json').pipe(gulp.dest('dist/public/authentication/static/app'));
  gulp.src('public/manager/static/app/**/*.json').pipe(gulp.dest('dist/public/manager/static/app'));

  gulp.src('public/retailer/static/assets/images/**').pipe(gulp.dest('dist/public/retailer/static/assets/images/'));
  gulp.src('public/client/static/assets/images/**').pipe(gulp.dest('dist/public/client/static/assets/images/'));
  gulp.src('public/checkout/static/assets/images/**').pipe(gulp.dest('dist/public/checkout/static/assets/images/'));
  gulp.src('public/authentication/static/assets/images/**').pipe(gulp.dest('dist/public/authentication/static/assets/images/'));
  gulp.src('public/manager/static/assets/images/**').pipe(gulp.dest('dist/public/manager/static/assets/images/'));

  gulp.src('public/retailer/static/favicon.ico').pipe(gulp.dest('dist/public/retailer/static/'));
  gulp.src('public/client/static/favicon.ico').pipe(gulp.dest('dist/public/client/static/'));
  gulp.src('public/checkout/static/favicon.ico').pipe(gulp.dest('dist/public/checkout/static/'));
  gulp.src('public/authentication/static/favicon.ico').pipe(gulp.dest('dist/public/authentication/static/'));
  gulp.src('public/manager/static/favicon.ico').pipe(gulp.dest('dist/public/manager/static/'));

});

gulp.task('build:dist', ['htmlreplace', 'css', 'css:vendors', 'js:app', 'js:angular', 'copy:build']);
