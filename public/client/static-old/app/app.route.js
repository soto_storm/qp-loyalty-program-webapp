(function () {
  'use strict';

  angular.module('app').config(
    ['$stateProvider', '$urlRouterProvider', config]);

  function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/whereToExplore');

    $stateProvider
      .state('whereToExplore', waitForServices({
        url: '/whereToExplore',
        templateUrl: 'app/core/where_to_explore/where_to_explore.tpl.html',
        controller: 'app.core.whereToExplore.whereToExploreCtrl'
      }))
      .state('vouchers', waitForServices({
        url: '/vouchers',
        templateUrl: 'app/core/vouchers/vouchers.tpl.html',
        controller: 'app.core.vouchers.vouchersCtrl'
      }))
      .state('profile', waitForServices({
        url: '/profile',
        templateUrl: 'app/core/profile/profile.tpl.html',
        controller: 'app.core.profile.profileCtrl'
      }))
      .state('needConfirmEmail', waitForServices({
        url: '/needConfirmEmail',
        templateUrl: 'app/core/confirm_email/confirm_email.tpl.html',
        controller: 'app.core.confirmEmail.confirmEmailCtrl'
      }));

    function waitForServices(conf) {
      if (!conf.resolve) {
        conf.resolve = {};
      }

      conf.resolve.waitForServicesInitialization = ['$q', 'ClientsSrv', function ($q, ClientsSrv) {

        var def = $q.defer();
        ClientsSrv._addInitListener(function () {
          def.resolve();
        });

        return def.promise;
      }];

      return conf;
    }

  }

})();
