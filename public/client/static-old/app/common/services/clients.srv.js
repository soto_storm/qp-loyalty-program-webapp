(function () {
  'use strict';

  angular.module('app.common.services').factory('ClientsSrv',
    ['$http', 'LoggerSrv', '$translate', ClientsSrv]);

  function ClientsSrv($http, LoggerSrv, $translate) {

    var initialized = false,
      initListeners;

    function getCurrentClient(callback) {
      $http({
        url: '/auth/current',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function saveClient(client, callback) {
      if (client._id) {

        $http({
          url: '/api/client/' + client._id,
          method: 'PUT',
          data: client
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function () {

          LoggerSrv.error($translate.instant('SERVER_ERROR'));

        });

      } else {

        $http({
          url: '/api/client',
          method: 'POST',
          data: client
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function () {

          LoggerSrv.error($translate.instant('SERVER_ERROR'));

        });

      }
    }

    function changePassword(clientId, clientPasswords, callback) {
      if (!clientId) {
        throw 'clientId is required (ClientsSrv.changePassword)'
      }
      if (!clientPasswords.password) {
        throw 'password is required (ClientsSrv.changePassword)'
      }
      if (!clientPasswords.newPassword) {
        throw 'newPassword is required (ClientsSrv.changePassword)'
      }

      clientPasswords.password = md5(clientPasswords.password);
      clientPasswords.newPassword = md5(clientPasswords.newPassword);

      $http({
        url: '/api/client/changePassword/' + clientId,
        method: 'POST',
        data: clientPasswords
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (resp) {

        if (resp.data && resp.data.error && callback) {
          callback(resp.data);
        } else {
          LoggerSrv.error($translate.instant('SERVER_ERROR'));
        }

      });
    }

    function logoutClient(callback) {
      $http({
        url: '/auth/logout',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function sendConfirmEmail(client, callback) {
      if(!client){
        throw 'clientId is required'
      }

      $http({
        url: '/api/mail/sendConfirmEmail?clientId=' + client._id,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function addInitListener(callback) {
      if (!callback) {
        throw "callback is required!"
      }

      if (initialized) {
        callback();
      } else {
        initListeners = callback;
      }

    }

    return {
      _addInitListener: addInitListener,
      getCurrentClient: getCurrentClient,
      saveClient: saveClient,
      changePassword: changePassword,
      logoutClient: logoutClient,
      sendConfirmEmail: sendConfirmEmail
    }

  }

}());
