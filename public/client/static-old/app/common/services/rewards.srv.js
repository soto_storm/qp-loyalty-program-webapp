(function () {
  'use strict';

  angular.module('app.common.services').factory('RewardsSrv',
    ['$http', 'LoggerSrv', '$translate', RewardsSrv]);

  function RewardsSrv($http, LoggerSrv, $translate) {

    function getRewardByRetailerId(retailerId, callback) {
      if (!retailerId) {
        throw 'retailerId is required (RewardsSrv.getRewardByRetailerId)'
      }

      $http({
        url: '/api/reward?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    return {
      getRewardByRetailerId: getRewardByRetailerId
    }
  }

}());
