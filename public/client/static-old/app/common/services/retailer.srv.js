(function () {
	'use strict';

	angular.module('app.common.services').factory('RetailerSrv',
		['$http', 'LoggerSrv', '$translate', RetailerSrv]);

	function RetailerSrv($http, LoggerSrv, $translate) {

		function getAll(callback){

			$http({
				url: '/api/retailer',
				method: 'GET'
			}).then(function (resp) {

				if (callback) {
					callback(resp.data);
				}
			}, function () {

				LoggerSrv.error($translate.instant('SERVER_ERROR'));

			});
		}

		return {
			getAll: getAll
		}

	}

}());
