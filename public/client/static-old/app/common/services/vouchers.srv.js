(function () {
  'use strict';

  angular.module('app.common.services').factory('VoucherSrv',
    ['$http', 'LoggerSrv', '$translate', VoucherSrv]);

  function VoucherSrv($http, LoggerSrv, $translate) {

    function getVouchersByClient(clientId, callback){
      if(!clientId){
        throw 'ClientId is required (VoucherSrv.getVouchersByClient)'
      }

      $http({
        url: '/api/voucher/client/' + clientId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function generateVoucher(voucher, callback){
      if(!voucher || !voucher.client || !voucher.reward){
        throw 'client and reward is required for generate voucher(VoucherSrv.generateVoucher)'
      }

      $http({
        url: '/api/voucher',
        method: 'POST',
        data: voucher
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    return {
      getVouchersByClient: getVouchersByClient,
      generateVoucher: generateVoucher
    }

  }

}());
