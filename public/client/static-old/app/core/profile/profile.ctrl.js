(function () {
  'use strict';

  angular.module('app.core.profile').controller('app.core.profile.profileCtrl',
    ['$scope', 'ClientsSrv', '$timeout', '$state', 'LoggerSrv', '$translate', profileCtrl]);

  function profileCtrl($scope, ClientsSrv, $timeout, $state, LoggerSrv, $translate) {
    $scope.isCollapsed = {
      value: true
    };

    var isWrongPass = false;
    $scope.editClient = angular.copy($scope.currentClient);
    $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    $scope.passwords = {};
    $scope.inputPassType = {
      main: 'password',
      new: 'password',
      confirmNew: 'password'
    };


    $scope.editClient.birthday = new Date($scope.editClient.birthday);

    $scope.popup = {
      opened: false
    };

    $scope.dateOptions = {
      maxDate: new Date(),
      startingDay: 1
    };

    $scope.gender = {
      M: 'Male',
      F: 'Female'
    };


    $scope.changeCollapse = function (form) {

      if ($scope.isCollapsed.value) {
        $scope.passwords = {};
        form.$setUntouched();
        $scope.tryToSavePass = false;
      }

      $scope.isCollapsed.value = !$scope.isCollapsed.value;
    };

    $scope.chooseGender = function (key) {
      $scope.editClient.gender = key;
    };

    $scope.hideShowPassword = function (valueType) {
      $scope.inputPassType[valueType] = $scope.inputPassType[valueType] === 'text' ? 'password' : 'text';
    };

    $scope.isShowErrorsClient = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSaveClient);
    };

    $scope.isShowErrorsPass = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSavePass);
    };

    $scope.setNewPassword = function (form) {
      $scope.$watch('passwords.password', function () {
        if (isWrongPass) {
          form.password.$setValidity("wrongPassword", true);
          isWrongPass = false;
        }
      });

      if (form.$valid) {
        $scope.disabledSave = true;

        var clientPasswords = {
          password: $scope.passwords.password,
          newPassword: $scope.passwords.newPassword
        };
        ClientsSrv.changePassword($scope.editClient._id, clientPasswords, function (resp) {

          if (resp.error) {
            isWrongPass = true;
            form.password.$setValidity("wrongPassword", false);
          } else {
            $scope.passwords = {};
            form.$setUntouched();
            $scope.tryToSavePass = false;
            $scope.changeCollapse(form);
            LoggerSrv.success($translate.instant('PASSWORD_HAS_BEEN_CHANGED'));
          }
        });
      } else {
        $scope.tryToSavePass = true;
      }
    };

    $scope.saveClient = function (form) {
      if (form.$valid) {
        var oldClient = $scope.currentClient;

        var isNameChange = $scope.editClient.name !== oldClient.name;
        var isEmailChange = $scope.editClient.email !== oldClient.email;
        var isGenderChange = $scope.editClient.gender !== oldClient.gender;
        var isBirthdayChange = +new Date($scope.editClient.birthday) !== +new Date(oldClient.birthday);

        if (isNameChange || isEmailChange || isGenderChange || isBirthdayChange) {

          $scope.disabledSaveClient = true;

          var client = {
            _id: $scope.editClient._id,
            name: $scope.editClient.name,
            email: $scope.editClient.email,
            gender: $scope.editClient.gender,
            birthday: $scope.editClient.birthday
          };

          if (!$scope.editClient.hasPassword) {
            client.password = $scope.editClient.password;
          }

          ClientsSrv.saveClient(client, function () {

            $scope.disabledSaveClient = false;
            $scope._loadCurrentClient(function (newClient) {
              if (!oldClient.hasPassword && newClient.hasPassword) {
                $timeout(function () {
                  $state.go('whereToExplore');
                }, 0)
              }
            });
            $scope.tryToSaveClient = false;
            LoggerSrv.success($translate.instant('USER_HAS_BEEN_CHANGED'));
          });
        }
      } else {
        $scope.tryToSaveClient = true;
      }
    };
  }

})();
