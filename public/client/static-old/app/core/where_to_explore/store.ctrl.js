(function () {
  'use strict';

  angular.module('app.core.whereToExplore').controller('app.core.whereToExplore.storeCtrl',
    ['$scope', 'RewardsSrv', '$uibModal', 'LoggerSrv', '$translate', storeCtrl]);

  function storeCtrl($scope, RewardsSrv, $uibModal, LoggerSrv, $translate) {

    $scope.openRewards = function () {
      RewardsSrv.getRewardByRetailerId($scope.store.retailer, function (data) {
        $scope.rewards = data;
        $scope.showReward = !$scope.showReward
      });
    };

    $scope.isVisited = $scope.store.clients && $scope.store.clients.indexOf($scope.currentClient._id) !== -1;

    $scope.openReward = function (reward) {

      if (!$scope.isVisited) {
        return
      }

      if (reward.points > $scope.currentClient.points) {
        LoggerSrv.warning($translate.instant('NOT_ENOUGH_DOTS'));
        return
      }

      $uibModal.open({
        templateUrl: 'app/core/where_to_explore/reward_modal.tpl.html',
        controller: 'app.core.whereToExplore.rewardModalCtrl',
        windowClass: 'reward-modal',
        animation: true,
        resolve: {
          reward: reward
        }
      });
    }
  }

})();
