"use strict";
(function () {
  angular.module('app.core.header').directive("qpHeader", ['$timeout', qpHeader]);

  function qpHeader($timeout) {
    return {
      link: function (scope, element) {

        var body = $('body');
        var touchSideBar = element.find('#touch-side-bar');
        var touchSideBar2 = element.find('#touch-side-bar2');
        var qpHeaderNav = element.find('[qp-header-nav]');

        body.on('click', '#touch-side-bar', function (event) {
          touchSideBar.toggleClass("show-left-bar");
          qpHeaderNav.toggleClass("show-left-bar");
          event.stopPropagation();
        });

        qpHeaderNav.click(function (event) {
          if (!scope.currentClient.hasPassword && scope.currentClient.emailConfirm) {
            event.stopPropagation();
          }
        });

        $timeout(function (val) {
          $('.right-panel').click(function (event) {
            event.stopPropagation();
          });
        }, 0);

        $(window).on('click', function () {
          qpHeaderNav.removeClass("show-left-bar");
          touchSideBar.removeClass("show-left-bar");
        });

        body.on('click', '#touch-side-bar2', function () {
          touchSideBar2.toggleClass("show-right-bar");
          $('.right-panel').toggleClass("show-right-bar");
          event.stopPropagation();
        });

        $(window).on('click', function () {
          $('.right-panel').removeClass("show-right-bar");
          touchSideBar2.removeClass("show-right-bar");
        });

        var tooltip = element.find('[qp-header-tooltip]');

        scope.$watch('currentClient.hasPassword', function () {
          if (!scope.currentClient.hasPassword && scope.currentClient.emailConfirm) {
            element.attr('style', 'cursor: not-allowed !important');
            qpHeaderNav.attr('style', 'pointer-events: none');
            tooltip.text('Select password first')

          } else {
            tooltip.hide();
            element.attr('style', '');
            qpHeaderNav.attr('style', '');
          }
        });

        element[0].addEventListener('mouseover', function () {
          if (!scope.currentClient.hasPassword) {
            element[0].addEventListener('mousemove', mousemove, false);
          }
        }, false);

        element[0].addEventListener('mouseout', function () {
          element[0].addEventListener('mousemove', mousemove, false);
        }, false);

        function mousemove(e) {
          tooltip[0].style.left = e.pageX + 'px';
          tooltip[0].style.top = e.pageY + 'px';
        }

      }
    };
  }

}());
