'use strict';
(function () {

  angular.module('app.common.directives').directive('voucherPrint',
    ['$http', printDirective]);


  function printDirective($http) {

    return {
      link: link,
      restrict: 'A'
    };

    function link(scope, element) {

      var voucherDoc;

      $http.get('app/core/vouchers/voucher_print/voucher_print.tpl.html')
        .then(function (response) {
          voucherDoc = response.data;

          voucherDoc = voucherDoc.replace('%voucher-title%', scope.voucher.title);
          voucherDoc = voucherDoc.replace('%voucher-code%', scope.voucher.code);
          voucherDoc = voucherDoc.replace('%voucher-status%', scope.statusMap[scope.voucher.status]);

        });

      element.on('click', function () {

        var myWindow = window.open();
        myWindow.document.write(voucherDoc);

        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        myWindow.close();

      });
    }
  }

})();

