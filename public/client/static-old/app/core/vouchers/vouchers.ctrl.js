(function () {
  'use strict';

  angular.module('app.core.vouchers').controller('app.core.vouchers.vouchersCtrl',
    ['$scope', 'VoucherSrv', 'RetailerSrv', rewardCtrl]);

  function rewardCtrl($scope, VoucherSrv, RetailerSrv) {

    $scope.statusMap = {
      '1': 'Issued',
      '2': 'Exchanged',
      '3': 'Expired'
    };

    RetailerSrv.getAll(function (data) {
      $scope.retailerNames = {};

      data.forEach(function (retailer) {
        $scope.retailerNames[retailer._id] = retailer.name;
      })
    });

    VoucherSrv.getVouchersByClient($scope.currentClient._id, function (data) {
      $scope.vouchers = data;
    });

  }

})();
