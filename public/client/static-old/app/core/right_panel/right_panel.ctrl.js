(function () {
  'use strict';

  angular.module('app.core.rightPanel').controller('app.core.rightPanel.rightPanelCtrl',
    ['$scope', 'StoresSrv', rightPanelCtrl]);

  function rightPanelCtrl($scope, StoresSrv) {

    StoresSrv.getStoreByClient($scope.currentClient._id, function (data) {
      $scope.storesNames = {};

      data.forEach(function (store) {
        $scope.storesNames[store._id] = store.storeName;
      })
    })

  }
})();
