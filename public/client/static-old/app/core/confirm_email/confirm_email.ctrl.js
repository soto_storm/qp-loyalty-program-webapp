(function () {
  'use strict';

  angular.module('app.core.confirmEmail').controller('app.core.confirmEmail.confirmEmailCtrl',
    ['$scope', 'ClientsSrv', confirmEmailCtrl]);

  function confirmEmailCtrl($scope, ClientsSrv) {

    $scope.sendAgain = function(){
      $scope.disabledSend = true;
      ClientsSrv.sendConfirmEmail($scope.currentClient, function(){
        $scope.disabledSend = false;
      })
    }

  }

})();
