"use strict";
 angular.module('clientApp').directive("isolate", [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            element.on('click', function() {
                element.find('span').toggleClass("glyphicon-chevron-up");
                element.find('span').toggleClass("glyphicon-chevron-down");
                if (attr.moreClasses) {
                    // Optionally toggle more classes if passed
                    element.toggleClass(attr.moreClasses);
                }
            });
        }
    };
}]);