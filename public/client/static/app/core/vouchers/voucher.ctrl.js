(function () {
  'use strict';

  angular.module('app.core.vouchers').controller('app.core.vouchers.voucherCtrl',
    ['$scope', '$uibModal', rewardCtrl]);

  function rewardCtrl($scope, $uibModal) {

    $scope.openVoucher = function () {

      if ($scope.voucher.status == 1) {
        $uibModal.open({
          templateUrl: 'app/core/vouchers/voucher_modal.tpl.html',
          controller: 'app.core.vouchers.voucherModalCtrl',
          windowClass: 'voucher-modal',
          animation: true,
          resolve: {
            voucherInfo: $scope.voucher
          }
        });
      }
    }
  }

})();
