(function () {
  'use strict';

  angular.module('app.core.vouchers').controller('app.core.vouchers.voucherModalCtrl',
    ['$scope', 'voucherInfo', '$uibModalInstance', voucherModalCtrl]);

  function voucherModalCtrl($scope, voucherInfo, $uibModalInstance) {

    $scope.statusMap = {
      '1': 'Issued',
      '2': 'Concluded',
      '3': 'Expired'
    };

    $scope.close = function () {
      $uibModalInstance.dismiss();
    };

    $scope.voucher = voucherInfo;
  }

})();
