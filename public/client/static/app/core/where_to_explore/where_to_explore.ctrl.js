(function () {
  'use strict';

  angular.module('app.core.whereToExplore').controller('app.core.whereToExplore.whereToExploreCtrl',
    ['$scope', 'StoresSrv', 'RewardsSrv', '$uibModal', 'LoggerSrv', '$translate', whereToExploreCtrl]);

  function whereToExploreCtrl($scope, StoresSrv, RewardsSrv, $uibModal, LoggerSrv, $translate) {
    $scope.onlyPlacesVisited = {
      value: false
    };
    $scope.isPlacesVisited = {
      value: false
    };

    StoresSrv.getAllStore(function (data) {
      $scope.stores = data;
    });

    $scope.showMapRetailer = function (place) {
      $scope.store = place;
      $scope.isMapStoreVisited = place.clients && place.clients.indexOf($scope.currentClient._id) !== -1;

      RewardsSrv.getRewardByRetailerId($scope.store.retailer, function (data) {
        $scope.storeRewards = data;
      });
    };

    $scope.storesFilter = function (item) {
      if ($scope.onlyPlacesVisited.value) {
        return item.clients && item.clients.indexOf($scope.currentClient._id) != -1;
      } else {
        return item
      }
    };

    $scope.openRewardForMapTab = function (reward) {

      if (!$scope.isMapStoreVisited) {
        return
      }

      if (reward.points > $scope.currentClient.points) {
        LoggerSrv.warning($translate.instant('NOT_ENOUGH_DOTS'));
        return
      }

      $uibModal.open({
        templateUrl: 'app/core/where_to_explore/reward_modal.tpl.html',
        controller: 'app.core.whereToExplore.rewardModalCtrl',
        windowClass: 'reward-modal',
        animation: true,
        resolve: {
          reward: reward
        }
      });
    }

  }

})();
