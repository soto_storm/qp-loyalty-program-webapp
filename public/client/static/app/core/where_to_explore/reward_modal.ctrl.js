(function () {
  'use strict';

  angular.module('app.core.whereToExplore').controller('app.core.whereToExplore.rewardModalCtrl',
    ['$scope', 'VoucherSrv', 'ConfirmationSrv', 'reward', '$uibModalInstance', 'LoggerSrv', '$translate', rewardModalCtrl]);

  function rewardModalCtrl($scope, VoucherSrv, ConfirmationSrv, reward, $uibModalInstance, LoggerSrv, $translate) {

    $scope.reward = reward;

    $scope.close = function () {
      $uibModalInstance.dismiss();
    };

    $scope.exchangeReward = function () {
      var message = 'Exchange reward (' + $scope.reward.points + ' dots' + ')?';
      var buttonText = 'Yes';

      ConfirmationSrv.show(message, buttonText).then(function () {
        var newVoucher = {
          client: $scope.currentClient._id,
          reward: $scope.reward._id
        };

        VoucherSrv.generateVoucher(newVoucher, function (data) {
          $scope._loadCurrentClient();
          LoggerSrv.success($translate.instant('VOUCHER_HAS_BEEN_CREATED', {code: data.code}));
        });

        $scope.close();
      })
    }
  }

})();
