'use strict';

(function () {

  angular.module("app.core.whereToExplore").directive("placesNearby",
    ['$rootScope', 'StoresSrv', '$timeout', '$http', placesNearby]);

  function placesNearby($rootScope, StoresSrv, $timeout, $http) {
    return {
      restrict: 'A',
      scope: {
        showRetailer: '=showRetailer',
        isVisited: "="
      },
      templateUrl: 'app/core/where_to_explore/places_nearby.tpl.html',
      link: function (scope, element) {

        var map;
        var params = {
          isVisited: false
        };
        var placesData = {
          placeMarkers: [],
          places: [],
          infoWindows: []
        };

        var defaultPlace = {
          lat: -13.950346,
          lng: -60.261953,
          zoom: 3
        };

        scope.$watch('isVisited', function (value) {
          if (value !== undefined && map) {
            params.isVisited = value;
            for (var i = 0; i < placesData.placeMarkers.length; i++) {
              placesData.placeMarkers[i].setMap(null);
            }
            placesData = {
              placeMarkers: [],
              places: [],
              infoWindows: []
            };
            google.maps.event.trigger(map, 'idle');
          }
        });

        $timeout(function () {
          initMap(defaultPlace.lat, defaultPlace.lng, defaultPlace.zoom);

          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
              function success(location) {
                setLocation(location.coords.latitude, location.coords.longitude);
              },
              function error() {
                noGeolocation()
              }
            );
          } else {
            noGeolocation();
          }
        }, 0);

        function noGeolocation() {
          $http.post('https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDxEu6bKLri9y1E6JpBKBCNM9IBJXAHiRU').then(
            function success(resp) {
              if (resp.data.location) {
                setLocation(resp.data.location.lat, resp.data.location.lng);
              }
            },
            function error() {
              console.error('Geolocation error')
            }
          )
        }

        function setLocation(lat, lng) {

          var centerPoint = new google.maps.LatLng(lat, lng);
          var marker = new google.maps.Marker({
            position: centerPoint,
            map: map,
            label: 'I'
          });

          map.setZoom(12);
          map.setCenter(marker.getPosition());
        }

        function initMap(lat, lng, zoom) {

          var centerPoint = new google.maps.LatLng(lat, lng);
          var mapOpt = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: centerPoint,
            zoom: zoom || 12
          };

          map = new google.maps.Map(element.find('[map]')[0], mapOpt);

          google.maps.event.addListener(map, 'idle', function () {

            var bounds = map.getBounds();

            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();

            var address = {
              nelat: ne.lat(),
              nelng: ne.lng(),
              swlat: sw.lat(),
              swlng: sw.lng()
            };

            StoresSrv.getNearby(address, function (data) {
              var places = data;
              for (var i = 0, len = places.length; i < len; i++) {

                if (params.isVisited && (!places[i].clients || places[i].clients.indexOf($rootScope.currentClient._id) === -1)) {
                  continue
                }
                var place = places[i];

                if (ifPlaceMarkExist(place._id)) {
                  continue
                }

                var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(place.address.locationLat, place.address.locationLng),
                  map: map,
                  title: place.storeName,
                  draggable: false,
                  flat: true,
                  id: place._id
                });

                var contentInfo = "<div>" +
                  "<h1>" + place.storeName + "</h1>" +
                  "<p>" + (place.description || '') + "</p>" +
                  ((place.clients && (place.clients.indexOf($rootScope.currentClient._id) != -1))
                    ? "<span style='color:#337ab7'>You visited this place</span>" : "")
                  + "</div>";

                var infoWindow = new google.maps.InfoWindow({
                  content: contentInfo,
                  maxWidth: 200
                });

                placesData.placeMarkers.push(marker);
                placesData.infoWindows.push(infoWindow);
                placesData.places.push(place);

                google.maps.event.addListener(placesData.placeMarkers[placesData.placeMarkers.length - 1], 'mouseover', function () {
                  var infoWindow = placesData.infoWindows[placesData.places.length - 1];
                  var placeMarker = placesData.placeMarkers[placesData.places.length - 1];
                  return function () {
                    infoWindow.open(map, placeMarker);
                  }
                }(placesData.placeMarkers.length - 1));

                google.maps.event.addListener(placesData.placeMarkers[placesData.placeMarkers.length - 1], 'mouseout', function () {
                  var infoWindow = placesData.infoWindows[placesData.places.length - 1];
                  return function () {
                    infoWindow.close();
                  }
                }(placesData.placeMarkers.length - 1));

                google.maps.event.addListener(placesData.placeMarkers[placesData.placeMarkers.length - 1], 'click', function () {
                  var placeData = placesData.places[placesData.places.length - 1];
                  return function () {
                    scope.showRetailer(placeData);
                  }
                }(placesData.placeMarkers.length - 1));

              }

            })

          });

          function ifPlaceMarkExist(markId) {
            for (var i = 0, len = placesData.placeMarkers.length; i < len; i++) {
              if (markId === placesData.placeMarkers[i].id) {
                return true
              }
            }
          }
        }

      }
    }
  }

})();
