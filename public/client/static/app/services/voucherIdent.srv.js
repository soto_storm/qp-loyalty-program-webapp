/**
 * Created by Jesus Soto on 10/25/2016.
 */
(function(){
  angular.module('clientApp').service('voucherIdentService',
    function(){
      var voucher = '';
      return{
        getVoucher: function(){
          return voucher;
        },
        setVoucher: function(value){
          voucher = value;
        }
      };
    });
}());
