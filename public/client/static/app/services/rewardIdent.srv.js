/**
 * Created by Jesus Soto on 10/25/2016.
 */
(function(){
  angular.module('clientApp').service('rewardIdentService',
    function(){
      var reward = '';
      return{
        getReward: function(){
          return reward;
        },
        setReward: function(value){
          reward = value;
        }
      };
    });
}());
