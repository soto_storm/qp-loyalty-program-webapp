(function () {
	'use strict';

	angular.module('clientApp').factory('RetailerSrv',
		['$http', RetailerSrv]);

	function RetailerSrv($http) {

		function getAll(callback){

			$http({
				url: '/api/retailer',
				method: 'GET'
			}).then(function (resp) {

				if (callback) {
					callback(resp.data);
				}
			}, function (err) {

				callback(err);

			});
		}

		return {
			getAll: getAll
		}

	}

}());
