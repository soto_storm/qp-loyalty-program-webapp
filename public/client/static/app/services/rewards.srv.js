(function () {
  'use strict';

  angular.module('clientApp').factory('RewardsSrv',
    ['$http',RewardsSrv]);

  function RewardsSrv($http) {

    function getRewardByRetailerId(retailerId, callback) {
      if (!retailerId) {
        throw 'retailerId is required (RewardsSrv.getRewardByRetailerId)'
      }

      $http({
        url: '/api/reward?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getRewardByRetailerId: getRewardByRetailerId
    }
  }

}());
