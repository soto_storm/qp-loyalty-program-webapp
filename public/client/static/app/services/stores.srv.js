(function () {
  'use strict';

  angular.module('clientApp').factory('StoresSrv',
    ['$http', StoresSrv]);

  function StoresSrv($http) {

    function getAllStore(callback) {
        $http({
        url: '/api/store',
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
        }, function (err) {

        callback(err);

      });
    }

    function getNearby(address, callback) {
      if(!address.nelat || !address.nelng || !address.swlat || !address.swlng){
        throw 'wrong address'
      }
      $http({
        url: '/api/store/placesNearby?nelat=' + address.nelat + '&nelng=' + address.nelng + '&swlat=' + address.swlat + '&swlng=' + address.swlng,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    function getStoreByClient(clientId, callback) {
      if (!clientId) {
        throw 'clientId is required (StoresSrv.getStoreByClient)'
      }

      $http({
        url: '/api/store?clientId=' + clientId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getAllStore: getAllStore,
      getNearby: getNearby,
      getStoreByClient: getStoreByClient
    }

  }

}());
