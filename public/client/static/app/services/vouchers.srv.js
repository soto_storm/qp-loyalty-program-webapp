(function () {
  'use strict';

  angular.module('clientApp').factory('VoucherSrv',
    ['$http', 'voucherIdentService', VoucherSrv]);

  function VoucherSrv($http) {

    function getVouchersByClient(clientId, callback){
      if(!clientId){
        throw 'ClientId is required (VoucherSrv.getVouchersByClient)'
      }

      $http({
        url: '/api/voucher/client/' + clientId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    function generateVoucher(voucher, callback){
      if(!voucher || !voucher.client || !voucher.reward){
        throw 'client and reward is required for generate voucher(VoucherSrv.generateVoucher)'
      }

      $http({
        url: '/api/voucher',
        method: 'POST',
        data: voucher
      }).then(function (resp) {

        if (callback) {
          //voucherIdentService.setVoucher(resp.data);
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getVouchersByClient: getVouchersByClient,
      generateVoucher: generateVoucher
    }

  }

}());
