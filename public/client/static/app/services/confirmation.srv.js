(function () {
  'use strict';

  angular.module('clientApp').service('ConfirmationSrv',
    [ConfirmationSrv]);

  function ConfirmationSrv($uibModal) {

    var modalParams = {
      animation: true,
      backdrop: 'static',
      keyboard: false,
      windowClass: 'modal-confirmation',
      templateUrl: 'app/common/templates/confirmation.tpl.html'
    };

    this.show = function (message, button, hideCancel) {

      modalParams.controller = (['$scope', '$uibModalInstance',
        function ($scope, $uibModalInstance) {

          $scope.messageConfirm = message;
          $scope.buttonConfirm = button;
          $scope.hideCancel = hideCancel;

          $scope.approveConfirm = function (result) {
            $uibModalInstance.close(result);
          };

          $scope.cancelConfirm = function () {
            $uibModalInstance.dismiss();
          };

        }]);

      return $uibModal.open(modalParams).result;
    };

  }

}());

