(function () {
  'use strict';

  angular.module('clientApp').factory('PlacesSrv',
    ['$http', PlacesSrv]);

  function PlacesSrv($http) {
    function getPlacesByClient(clientId, callback){
      if(!clientId){
        throw 'ClientId is required (PlacesSrv.getPlacesByClient)'
      }

      $http({
        url: '/api/place?clientId=' + clientId,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getPlacesByClient: getPlacesByClient
    }

  }

}());
