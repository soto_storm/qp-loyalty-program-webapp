(function () {
  'use strict';

  angular.module('clientApp').controller('rewardModalCtrl',
    ['$scope', '$rootScope', '$mdDialog', 'VoucherSrv', 'rewardIdentService', 'voucherIdentService', rewardModalCtrl]);

  function rewardModalCtrl($scope, $rootScope, $mdDialog, VoucherSrv, rewardIdentService, voucherIdentService) {
    $rootScope.generatedVoucher = {};
    $scope.reward = rewardIdentService.getReward();    
    $scope.showAlert = function(message) {
      // Appending dialog to document.body to cover sidenav in docs app
      // Modal dialogs should fully cover application
      // to prevent interaction outside of dialog      
      var template = 'app/views/where_to_explore/exchanged_success.html';
      $scope.generatedVoucher=voucherIdentService.getVoucher();      
      if($scope.generatedVoucher.data){
        if($scope.generatedVoucher.data.message==='Lack of point to buy this voucher'){
          var template='app/views/where_to_explore/not_enought_points.html';
        }
      }
      $mdDialog.show({
          templateUrl: template,
          clickOutsideToClose:true
        }
      );
    };

    $scope.addSeparator = function(number) {
          return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  };

    $scope.printCoupon=function(divName){
    //window.print();
    //console.log(voucherIdentService.getVoucher());
    $mdDialog.hide();
    $scope.generatedVoucher = voucherIdentService.getVoucher(); 
    var couponTitle = $scope.generatedVoucher.title;
    var couponCode = $scope.generatedVoucher.code;   
    document.getElementById(divName).innerHTML = "Prêmio: " + couponTitle + "<br/>Código do Voucher: " + couponCode; 
    var printContents = document.getElementById(divName).innerHTML; 
    var popupWin = window.open('', '_blank', 'width='+screen.availWidth*0.85+',height='+screen.availHeight*0.85);
    angular.element(popupWin.document.querySelector(divName)).show();
    popupWin.document.open();
    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
    popupWin.document.close();
  };


    $scope.exchangeReward = function () {

      var newVoucher = {
        client: $rootScope.currentClient._id,
        reward: $scope.reward._id
      };
      VoucherSrv.generateVoucher(newVoucher, function (data) {        
        if(data.reward){
          voucherIdentService.setVoucher(data);
          $scope.showAlert('Congratulations!, new voucher created!');
          $scope.beforePoints = $rootScope.currentClient.points.replace(".","");
          $scope.currentPoints = parseInt($scope.beforePoints) - data.points;
          $rootScope.currentClient.points = $scope.addSeparator($scope.currentPoints);
        }else{
          if(data.status===-1){
            voucherIdentService.setVoucher(data);
            $scope.showAlert('No internet');
          }else{
            if(data.data.message){
              voucherIdentService.setVoucher(data);
              $scope.showAlert('No points');
            }
          }
        }
      });
    };
  }
})();
