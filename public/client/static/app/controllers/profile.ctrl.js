/**
 * Created by Jesus Soto on 10/19/2016.
 */
(function(){
  angular.module('clientApp').controller('profileCtrl',
    function($rootScope, $scope, $state, $mdDialog, $location, ClientsSrv){
        var cpfArray=$rootScope.currentClient.cpf.split('');        
        $rootScope.explorerClass='';
        $rootScope.voucherClass='';
        $scope.cpfEx= cpfFormatter(cpfArray);
        $scope.InvalidDate=false;
        $scope.dateMessage='';
        var mongoDate=new Date($rootScope.currentClient.birthday);
        $scope.pass = false; // if true makes visible the resetPassword Form
        $scope.invalidDate = false;
        var formatted = $.datepicker.formatDate("dd/mm/yy", new Date(mongoDate));
        $scope.aniversario=formatted;
        $scope.errorMessage='';
        $scope.wrongPassword=false;
        $scope.client={
          _id: $rootScope.currentClient._id,
          name: $rootScope.currentClient.name,
          birthday: '',
          gender: $rootScope.currentClient.gender,
          email: $rootScope.currentClient.email
        };
        
      if($scope.client.gender==='M'){
        $scope.sex=false;
      }else{        
        $scope.sex=true;
      }

      $scope.clearMessage=function(){
        $scope.invalidDate=false;
        $scope.wrongPassword=false;
      };

      $scope.showPasswordForm=function(){
        if($scope.pass) {
          $scope.pass = false;
        }else{
          $scope.pass = true;
        }
      };

      function cpfFormatter(number){
        var cpf='';
        for(var i=0; i<number.length; i++){
          if(i>-1&&i<2){
            cpf+=cpfArray[i];
          }else if(i===2){
            cpf+=cpfArray[i]+'.';
          }else if(i>2 && i<=4){
            cpf+=cpfArray[i];
          }else if(i===5){
            cpf+=cpfArray[i]+'.';
          }else if(i>4 && i<=7){
            cpf+=cpfArray[i];
          }else if(i===8){
            cpf+=cpfArray[i]+'-';
          }else if(i>8){
            cpf+=cpfArray[i];
          }
        }
        return cpf;
      }

     $scope.showAlert = function(message) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog
        $mdDialog.show({
            templateUrl: 'app/views/profile/profile_updated.html',
            clickOutsideToClose:true
          });
      };

      $scope.getSexChoice = function(){
        if($scope.sex){          
          $rootScope.userImage = 'assets/images/profile2.png';
        }else{
          $rootScope.userImage = 'assets/images/profile.png';
        }
      };

      $scope.setNewPassword=function(form){
        if(form.$valid){
          if($scope.new===$scope.confirm){
            var clientPasswords = {
              password: $scope.old,
              newPassword: $scope.new
            };
            ClientsSrv.changePassword($rootScope.currentClient._id, clientPasswords, function (resp) {
              if(!resp.status) {
                if (resp.error) {
                  $scope.wrongPassword = true;
                } else {
                  $scope.showAlert('Your password has changed.');
                }
              }else{
                $scope.showAlert('Connection failed. Check your internet :(');
              }
            });
          }
        }
      };

      $scope.save=function(form){
        if(form.$valid) {          
          if ($scope.sex) {
            $scope.client.gender = 'F';
          } else {
            $scope.client.gender = 'M';
          }
          var dateArr=$scope.aniversario.split('/');
          var date=dateArr[2]+"/"+dateArr[1]+"/"+dateArr[0];
          if(parseInt(dateArr[2])<2000){
            if(new Date(date).toString()==='Invalid Date'){
              $scope.invalidDate=true;
              $scope.message='Invalid Date';
            }
            $scope.client.birthday=new Date(date);
          }else{
            $scope.invalidDate=true;
            $scope.message='You must be at least 16 years old.';
          }

          if(!$scope.invalidDate){
            ClientsSrv.saveClient($scope.client, function (data) {
              if(!data.status) {
                $rootScope._loadCurrentClient();
                $scope.showAlert('Your info has been updated. :D');
              }else{
                $scope.showAlert('Connection failed. Check your internet :(');
              }
            });
          }
        }
      }
    });
}());

