/**
 * Created by Jesus Soto on 10/18/2016.
 */
(function(){
  angular.module('clientApp').controller('mapCtrl',
    function($rootScope, $scope, $mdDialog, StoresSrv, RewardsSrv, rewardIdentService, NgMap){
      $rootScope.explorerClass='active';
      $rootScope.voucherClass='';
      $scope.stores = [];
      StoresSrv.getStoreByClient($rootScope.currentClient, function (data) {
          for(var info in data){
            $scope.stores.push({
              position: '['+data[info].address.locationLat+','
                           +data[info].address.locationLng+']',
              specificPosition: {
                lat: data[info].address.locationLat,
                lng: data[info].address.locationLng
              },
              storeName: data[info].storeName,
              description: data[info].description,
              showDetails: false,
              retailer: data[info].retailer,
              clients: data[info].clients,
              emptyRewards: false
            });
          }
        NgMap.getMap().then(function(map){
          $scope.map=map;
        });
      });

      $scope.showStore = function(event, store){
        $scope.selectedStore = store;
          marker = new google.maps.Marker({
            map: $scope.map,
            position: {
              lat:parseFloat(store.specificPosition.lat),
              lng:parseFloat(store.specificPosition.lng)
            }
          });
        $scope.map.showInfoWindow('storeInfo', marker);
        marker.setVisible(true);
      };

      $scope.showAdvanced = function() {
        $mdDialog.show({
          templateUrl: 'app/views/where_to_explore/confirmExchange.html',
          clickOutsideToClose:true
        });
      };

      $scope.getReward = function(reward, store){
        if(store.isVisited) {
          rewardIdentService.setReward(reward);
          $scope.showAdvanced();
        }
      };

      $scope.click=function(store){
        if(store.rewards){}else {
          store.exchangeClass = 'btn btn-greenalt btn-troque';
          store.isVisited=true;
          store.rewards = [];
          RewardsSrv.getRewardByRetailerId(store.retailer, function (data) {
            store.rewards = data;
            if(store.rewards.length===0){
              store.emptyRewards=true;
            }
          });
        }

        if(store.showDetails){
          store.showDetails=false;
        }else{
          store.showDetails=true;
        }
      };
    });
}());
