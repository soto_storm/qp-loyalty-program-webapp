/**
 * Created by Jesus Soto on 10/25/2016.
 */
(function(){
  angular.module('clientApp').controller('singleVoucherCtrl', function($rootScope, $scope, voucherIdentService){
      $scope.voucher=voucherIdentService.getVoucher();
      $scope.useVoucher=function(){
        window.print();
      };
  });
}());
