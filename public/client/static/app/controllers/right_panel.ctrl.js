(function(){
  angular.module('clientApp').controller('rightPanelCtrl',
    function($rootScope, $scope, $timeout, StoresSrv){
      var waitForRootscope=null;
      var wait=0;

      console.log($scope.activities);

      $scope.on_rootScope=function(){
        waitForRootscope=$timeout($scope.on_rootScope,1000);
        wait++;
        if(wait===1){
          StoresSrv.getStoreByClient($rootScope.currentClient, function (data) {
            $scope.storesNames = {};
            data.forEach(function (store) {
              $scope.storesNames[store._id] = store.storeName;
            });
          });
        }else if(wait>2){
          $timeout.cancel(waitForRootscope);
        }
      };

      $scope.loadStores=function(){
        waitForRootscope=$timeout($scope.on_rootScope,1000);
      };

      $scope.loadStores();
    });
}());
