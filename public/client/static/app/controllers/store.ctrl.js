(function(){
  angular.module('clientApp').controller('storeCtrl',
    function($rootScope, $scope, $timeout, $mdDialog, $filter, StoresSrv, RewardsSrv
      ,rewardIdentService){
      $scope.stores=[];
      $scope.rewards=[];
      $rootScope.explorerClass='active';
      $rootScope.voucherClass='';
      $scope.storesVisible=[];
      $scope.verMais=true;
      $scope.visitedStores = [];
      $scope.getDistance=function(){
        try {
          for (var store in $scope.stores) {
            var latLngA = new google.maps.LatLng($rootScope.coords.lat, $rootScope.coords.long);
            var latLngB = new google.maps.LatLng($scope.stores[store].address.locationLat,
              $scope.stores[store].address.locationLng);
            var distance = google.maps.geometry.spherical.computeDistanceBetween(latLngA, latLngB) / 1000;
            $scope.stores[store].distance = parseFloat(Math.round(distance * 100) / 100).toFixed(2);
          }
        }catch(err){}
      };

      StoresSrv.getStoreByClient($rootScope.currentClient._id, function (data){
        $scope.stores=data;
        for(var store in $scope.stores){
          $scope.stores[store].showDetails=true;
          $scope.stores[store].emptyRewards=false;
          $scope.stores[store].isVisited=false;
          $scope.stores[store].exchangeClass = 'btn btn-greenalt btn-troque';
          for(var client in $scope.stores[store].clients){
            if($scope.stores[store].clients[client]===$rootScope.currentClient._id){
               $scope.stores[store].isVisited=true;
               $scope.visitedStores.push($scope.stores[store]);
            }
          }
        }
        $scope.getDistance();
        $scope.loadStores(0);
      });

      $scope.loadStores=function(init){
        for(var i=init; i<7+init; i++) {
          if($scope.stores.length>i) {
            $scope.storesVisible.push($scope.stores[i]);
          }else{
            $scope.verMais=false;
          }
        }

        for(var i = 0; i<$scope.storesVisible.length; i++){
  		    if($scope.storesVisible[i].isVisited) {
  		       $scope.storesVisible.splice(i, 1);
  		    }
		    }        
      };

      $scope.showAdvanced = function() {
        $mdDialog.show({
          templateUrl: 'app/views/where_to_explore/confirmExchange.html',
          clickOutsideToClose:true
        });
      };

      $scope.close = function () {
        $mdDialog.hide();
      };

      $scope.getReward = function(reward, store){
        //if(store.isVisited) {
          reward.store=store.storeName;
          rewardIdentService.setReward(reward);
          $scope.showAdvanced();
        //}
      };

      $scope.printCoupon=function(divName){
        //window.print();
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', '_blank', 'width=300,height=300');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
      };

      $scope.click=function(store){
        if(store.rewards){}else {
          store.rewards = [];
          RewardsSrv.getRewardByRetailerId(store.retailer, function (data) {
            store.rewards = data;
            if(store.rewards.length===0){
              store.emptyRewards=true;
            }
          });
        }

        if(store.showDetails){
          store.showDetails=false;
        }else{
          store.showDetails=true;
        }
      };
    });
}());
