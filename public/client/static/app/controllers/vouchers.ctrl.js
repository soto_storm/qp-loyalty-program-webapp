/**
 * Created by Jesus Soto on 10/25/2016.
 */
(function(){
  angular.module('clientApp').controller('vouchersCtrl', function($rootScope, $scope, $mdDialog, VoucherSrv,
                                                                  RetailerSrv, voucherIdentService){

      $rootScope.explorerClass='';
      $rootScope.voucherClass='active';
      $scope.retailerNames = [];
      $scope.vouchersVisible=[];
      $scope.verMais=true;
      RetailerSrv.getAll(function (data) {
        for(var info in data){
          $scope.retailerNames.push({name:data[info].name,
                                     id:data[info]._id});
        }

        VoucherSrv.getVouchersByClient($rootScope.currentClient._id, function (data) {
          $scope.vouchers = data;
          for(var i=0; i<data.length; i++){
            for(var j=0; j<$scope.retailerNames.length; j++){
              if(data[i].retailer===$scope.retailerNames[j].id){
                $scope.vouchers[i].retailerName=$scope.retailerNames[j].name;
              }
            }

            $scope.vouchers[i].statusClass='btn btn-greenalt btn-imprimir';
            $scope.vouchers[i].statusIcon='glyphicon glyphicon-print';
            $scope.vouchers[i].showDate=true;
            switch($scope.vouchers[i].status){
              case 1:
                $scope.vouchers[i].status='IMPRIMIR CUPOM';
                break;
              case 2:
                $scope.vouchers[i].statusClass='btn btn-whiteb btn-trocados';
                $scope.vouchers[i].statusIcon='glyphicon glyphicon-retweet';
                $scope.vouchers[i].status='TROCADOS';
                $scope.vouchers[i].showDate=false;
                break;
              case 3:
                $scope.vouchers[i].statusClass='btn btn-yellowalt btn-expirado';
                $scope.vouchers[i].statusIcon='glyphicon glyphicon-calendar';
                $scope.vouchers[i].status='EXPIRADO';
                $scope.vouchers[i].showDate=false;
                break;
            }
          }          
          $scope.loadVouchers(data.length-1);                  
        });      
      });

      $scope.loadVouchers=function(init){
        for(var i=init; i>init-7; i--) {                    
          if($scope.vouchersVisible.length>=$scope.vouchers.length){
            $scope.verMais=false;
          }else{
            $scope.vouchersVisible.push($scope.vouchers[i]);
          }  
        }
      };

      $scope.showAdvanced = function() {
        $mdDialog.show({
          templateUrl: 'app/views/vouchers/useVoucher.html',
          clickOutsideToClose:true
        });
      };

      $scope.openVoucher=function(voucher){
        if(voucher.status==='IMPRIMIR CUPOM'){
          voucherIdentService.setVoucher(voucher);
          console.log(voucherIdentService.getVoucher(voucher));
          $scope.showAdvanced();
        }
      };

    });
}());
