(function () {
  'use strict';

  angular.module('app.common.services').factory('PlacesSrv',
    ['$http', 'LoggerSrv', '$translate', PlacesSrv]);

  function PlacesSrv($http, LoggerSrv, $translate) {

    function getPlacesByClient(clientId, callback){
      if(!clientId){
        throw 'ClientId is required (PlacesSrv.getPlacesByClient)'
      }

      $http({
        url: '/api/place?clientId=' + clientId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    return {
      getPlacesByClient: getPlacesByClient
    }

  }

}());
