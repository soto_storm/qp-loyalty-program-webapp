(function () {
  'use strict';

  var app = angular.module('app',
    [
      'ngResource',
      'ngSanitize',
      'ngStorage',
      'ui.router',
      'ngplus',
      'angular-loading-bar',
      'ui.bootstrap',
      'ui.utils.masks',
      'idf.br-filters',
      'pascalprecht.translate',
      'oitozero.ngSweetAlert',
      'ngMessages',
      'ngclipboard',
      'chart.js',

      'app.common.services',
      'app.common.filters',
      'app.common.directives',
      'app.core.header',
      'app.core.whereToExplore',
      'app.core.profile',
      'app.core.rightPanel',
      'app.core.vouchers',
      'app.core.confirmEmail'
    ]);

  app.run(['$rootScope', 'ClientsSrv', '$state', '$location', '$translate', '$localStorage', run]);
  app.config(['toastr', toastrConfig]);
  app.config(['$translateProvider', translate]);
  app.config(['$httpProvider', interceptor]);
  app.constant('toastr', toastr);

  function run($rootScope, ClientsSrv, $state, $location, $translate, $localStorage) {

    $rootScope.changeLanguage = function (langKey) {
      $rootScope.currentLang = langKey;
      $translate.use(langKey);
      $localStorage.currentLang = langKey;
    };

    if ($localStorage.currentLang) {
      $rootScope.changeLanguage($localStorage.currentLang);
    } else {
      $rootScope.currentLang = 'en';
    }

    $rootScope._loadCurrentClient = function (callback) {
      ClientsSrv.getCurrentClient(function (data) {
        $rootScope.currentClient = angular.copy(data);

        if (!$rootScope.currentClient.emailConfirm) {
          $state.go('needConfirmEmail');
          return
        }

        if (!$rootScope.currentClient.hasPassword) {
          $state.go('profile');
          return
        }

        if ($location.path() === '/needConfirmEmail' && $rootScope.currentClient.emailConfirm) {
          $state.go('whereToExplore');
          return
        }

        if (callback) {
          callback($rootScope.currentClient)
        }
      });
    };
    $rootScope._loadCurrentClient();

    $rootScope.$on('$stateChangeStart', function (event, toState) {
      var client = $rootScope.currentClient;

      if (client && toState.name !== 'needConfirmEmail' && !client.emailConfirm) {
        if (event) {
          event.preventDefault();
        }
        $state.go('needConfirmEmail');
        return
      }

      if (client && toState.name !== 'profile' && toState.name !== 'needConfirmEmail' && !client.hasPassword) {
        if (event) {
          event.preventDefault();
        }
        $state.go('profile');
        return
      }

      if (client && toState.name === 'needConfirmEmail' && client.emailConfirm) {
        if (event) {
          event.preventDefault();
        }
        $state.go('whereToExplore');
        return
      }
    });

    $rootScope.logout = function () {
      ClientsSrv.logoutClient(function (data) {
        window.location.href = data.redirectTo ? data.redirectTo : "/authentication";
      })
    }

  }

  function toastrConfig(toastr) {
    toastr.options = {
      progressBar: true,
      timeOut: 10000,
      positionClass: 'toast-top-right'
    };
  }

  function translate($translateProvider) {

    $translateProvider.useSanitizeValueStrategy('escape');

    $translateProvider.useStaticFilesLoader({
      prefix: 'app/i18n/locale-',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');
  }

  function interceptor($httpProvider) {

    $httpProvider.interceptors.push(function () {
      return {
        request: function (config) {

          if (config.url) {

            if (!config.params) {
              config.params = {};
            }

            if (!(config.url.indexOf('.html') >= 0)) {
              config.params.t = new Date().getTime();
            }
          }

          return config;
        }
      }
    });
  }

})();
