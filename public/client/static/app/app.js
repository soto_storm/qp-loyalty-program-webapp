var app=angular.module('clientApp',['ui.router',
                                    'ngMap',
                                    'ngMaterial',
                                    'geolocation',
                                    'ngAnimate',
                                    'angular.filter',
                                    'ui.mask'])
  .run(function($rootScope, ClientsSrv, $location, $state, geolocation){

  $rootScope.explorerClass='active';
  $rootScope.voucherClass='';

  $rootScope.addSeparator = function(number) {
          return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        };

  $rootScope._loadCurrentClient = function (callback) {
    ClientsSrv.getCurrentClient(function (data) {  

    $rootScope.currentClient = angular.copy(data);    

      if($rootScope.currentClient.gender==='M'){
        $rootScope.userImage="assets/images/profile.png";
      }else{
        $rootScope.userImage="assets/images/profile2.png";
      }

    if($rootScope.currentClient.points) { // add a comma to the retailer points so it looks cleaner.
      $rootScope.currentClient.points = $rootScope.addSeparator($rootScope.currentClient.points);
    }

      
      if (!$rootScope.currentClient.emailConfirm) {
        //$state.go('needConfirmEmail');
        $state.go('profile');
        return;
      }

      if (!$rootScope.currentClient.hasPassword) {
        $state.go('whereToExplore');
        return;
      }

      if ($location.path() === '/needConfirmEmail' && $rootScope.currentClient.emailConfirm) {
        $state.go('profile');
        return;
      }

      if (callback) {
        callback($rootScope.currentClient);
      }
    });
  };

  $rootScope.getDistance=function() {
    geolocation.getLocation().then(function (data) {
      $rootScope.coords = {lat: data.coords.latitude, long: data.coords.longitude};
      $rootScope.currentPosition ='['+$rootScope.coords.lat+','+$rootScope.coords.long+']';
    }, function(err){
      jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAKzIKyXjQFrpTG2B4InBjmzydcjQKLWdM", function(data) {
          $rootScope.coords = {lat: data.location.lat, long: data.location.lng};
          $rootScope.currentPosition ='['+$rootScope.coords.lat+','+$rootScope.coords.long+']';
        })
        .fail(function(err) {
          alert("API Geolocation error! \n\n");
          console.log(err);
        });
    });
  };

  $rootScope._loadCurrentClient();
  $rootScope.getDistance();

  $rootScope.$on('$stateChangeStart', function (event, toState) {
    var client = $rootScope.currentClient;

  /*  if (client && toState.name !== 'needConfirmEmail' && !client.emailConfirm) {
      if (event) {
        event.preventDefault();
      }
      $state.go('needConfirmEmail');
      return
    }*/

    if (client && toState.name !== 'profile' && toState.name !== 'needConfirmEmail' && !client.hasPassword) {
      if (event) {
        event.preventDefault();
      }
      $state.go('profile');
      return;
    }

    if (client && toState.name === 'needConfirmEmail' && client.emailConfirm) {
      if (event) {
       event.preventDefault();
      }
      $state.go('whereToExplore');
      return;
    }
  });

  $rootScope.logout = function () {
    ClientsSrv.logoutClient(function (data) {
      window.location.href = data.redirectTo ? data.redirectTo : "/authentication";
    })
  };

});

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/whereToExplore');

    $stateProvider
      .state('whereToExplore', waitForServices({
        url: '/whereToExplore',
        templateUrl: 'app/views/where_to_explore/map.html',
        controller: 'mapCtrl'
      }))
      .state('list', waitForServices({
        url: '/list',
        templateUrl: 'app/views/where_to_explore/list.html',
        controller: 'storeCtrl'
      }))
      .state('profile', waitForServices({
        url: '/profile',
        templateUrl: 'app/views/profile/profile.html',
        controller: 'profileCtrl'
      }))
      .state('vouchers', waitForServices({
        url: '/vouchers',
        templateUrl: 'app/views/vouchers/vouchers.html',
        controller: 'vouchersCtrl'
      }));

    function waitForServices(conf) {
      if (!conf.resolve) {
        conf.resolve = {};
      }

      conf.resolve.waitForServicesInitialization = ['$q', 'ClientsSrv', function ($q, ClientsSrv) {

        var def = $q.defer();
        ClientsSrv._addInitListener(function () {
          def.resolve();
        });

        return def.promise;
      }];

      return conf;
    }

});
