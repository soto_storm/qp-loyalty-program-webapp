'use strict';

(function () {

  angular.module("app.common.services").service("SignUpClientSrv",
    ['$http', 'LoggerSrv', '$translate', SignUpClientSrv]);

  function SignUpClientSrv($http, LoggerSrv, $translate) {

    function clientSignUp(client, callback) {
      var newClient = {
        name: client.name,
        cpf: client.cpf,
        email: client.email,
        birthday: client.birthdate,
        gender: client.gender
      };

      if (client.password) {
        newClient.password = md5(client.password)
      }

      return $http({
        method: "POST",
        url: '/api/client?signup=true',
        data: JSON.stringify(newClient)
      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      })
    }

    return {
      clientSignUp: clientSignUp
    };
  }

})();
