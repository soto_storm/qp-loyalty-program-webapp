/* global moment */

angular.module('app.common.directives').directive('dateSelect', [function () {

  return {
    restrict: 'A',
    replace: true,
    templateUrl: 'app/common/templates/date_select.tpl.html',
    require: 'ngModel',
    scope: {
      signUp: '=dateSelectForm',
      isShowErrors: '=isShowErrors'
    },

    link: function (scope, elem, attrs, model) {
      scope.val = {};

      var min = scope.min = moment(attrs.min || '1900-01-01');
      var max = scope.max = moment(attrs.max); // Defaults to now

      scope.years = [];

      for (var i = max.year(); i >= min.year(); i--) {
        scope.years.push(i);
      }

      scope.clear = function () {
        scope.val = {};
        model.$setViewValue();
        scope.signUp.birthdate.$setPristine();
        scope.signUp.birthdate.$setUntouched();

        for (var error in scope.signUp.birthdate.$error) {
          model.$setValidity(error, true);
        }
      };

      scope.$watch('val.year', function () {
        updateMonthOptions();
      });

      scope.$watchCollection('[val.month, val.year]', function () {
        updateDateOptions();
      });

      scope.$watchCollection('[val.date, val.month, val.year]', function () {
        if (scope.val.year && scope.val.month && scope.val.date) {
          var m = moment([scope.val.year, scope.val.month - 1, scope.val.date]);
          model.$setViewValue(new Date(m.format()));
          scope.signUp.birthdate.$setPristine();
          scope.signUp.birthdate.$setUntouched();

          for (var error in scope.signUp.birthdate.$error) {
            model.$setValidity(error, true);
          }

        } else if (!scope.val.year && !scope.val.month && !scope.val.date) {
          scope.signUp.birthdate.$setViewValue();
        } else if (!scope.val.year && scope.val.month && scope.val.date) {
          scope.signUp.birthdate.$setValidity("yearRequired", false);
        } else if (!scope.val.month && scope.val.year && scope.val.date) {
          scope.signUp.birthdate.$setValidity("monthRequired", false);
        } else if (!scope.val.date && scope.val.year && scope.val.month) {
          scope.signUp.birthdate.$setValidity("dateRequired", false);
        } else if (!scope.val.date && !scope.val.month && scope.val.year) {
          scope.signUp.birthdate.$setValidity("dateMonthRequired", false);
        } else if (!scope.val.date && !scope.val.year && scope.val.month) {
          scope.signUp.birthdate.$setValidity("dateYearRequired", false);
        } else if (!scope.val.month && !scope.val.year && scope.val.date) {
          scope.signUp.birthdate.$setValidity("monthYearRequired", false);
        }

      });

      function updateMonthOptions() {
        scope.months = [];

        var minMonth = scope.val.year && min.isSame([scope.val.year], 'year') ? min.month() : 0;
        var maxMonth = scope.val.year && max.isSame([scope.val.year], 'year') ? max.month() : 11;

        var monthNames = moment.months();

        for (var j = minMonth; j <= maxMonth; j++) {
          scope.months.push({
            name: monthNames[j],
            value: j + 1
          });
        }

        if (scope.val.month - 1 > maxMonth || scope.val.month - 1 < minMonth) delete scope.val.month;
      }

      function updateDateOptions(year, month) {
        var minDate, maxDate;

        if (scope.val.year && scope.val.month && min.isSame([scope.val.year, scope.val.month - 1], 'month')) {
          minDate = min.date();
        } else {
          minDate = 1;
        }

        if (scope.val.year && scope.val.month && max.isSame([scope.val.year, scope.val.month - 1], 'month')) {
          maxDate = max.date();
        } else if (scope.val.year && scope.val.month) {
          maxDate = moment([scope.val.year, scope.val.month - 1]).daysInMonth();
        } else {
          maxDate = 31;
        }

        scope.dates = [];

        for (var i = minDate; i <= maxDate; i++) {
          scope.dates.push(i);
        }
        if (scope.val.date > scope.dates.length) delete scope.val.date;
      }

      model.$render = function () {
        if (!model.$viewValue) {
          return;
        }

        var m = moment(model.$viewValue);
        scope.val = {
          year: m.year(),
          month: m.month() + 1,
          date: m.date()
        };
      };
    }
  };
}]);
