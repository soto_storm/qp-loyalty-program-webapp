'use strict';

(function () {

  angular.module("app.core.signInClient").directive("forgotPasswordModal",
    ['$state', '$uibModal',
      function ($state, $uibModal) {
        return {
          restrict: 'A',
          link: function ($scope, element) {

            element.on('click', function () {

              var modalInstance = $uibModal.open({
                templateUrl: 'app/core/sign_in_client/forgot_password/forgot_password.tpl.html',
                controller: 'app.core.signInClient.forgotPasswordCtrl',
                windowClass: 'forgot-password-modal',
                size: 'sm',
                //backdrop: 'static',
                animation: true
              });

            });
          }
        }
      }]);
})();
