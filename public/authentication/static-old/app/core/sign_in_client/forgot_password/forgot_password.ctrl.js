(function () {
  'use strict';

  angular.module('app.core.signInClient').controller('app.core.signInClient.forgotPasswordCtrl',
    ['$scope', '$uibModalInstance', 'SignInClientSrv', 'LoggerSrv', '$translate', forgotPasswordCtrl]);

  function forgotPasswordCtrl($scope, $uibModalInstance, SignInClientSrv, LoggerSrv, $translate) {

    var isWrongCPF = false;

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.sendCpf = function (form) {
      $scope.tryToSave = true;

      $scope.$watch('cpf', function () {
        if (isWrongCPF) {
          form.cpf.$setValidity("wrongCpf", true);
          isWrongCPF = false;
        }
      });

      if (form.$valid) {
        $scope.disabledSave = true;

        SignInClientSrv.forgotPassword($scope.cpf, function (data) {
          $scope.disabledSave = false;

          if (data.error) {
            isWrongCPF = true;
            form.cpf.$setValidity("wrongCpf", false);
          } else {
            LoggerSrv.success($translate.instant('CHECK_EMAIL'));
            $uibModalInstance.dismiss('cancel');
          }
        })
      }
    };

    $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

  }

})();
