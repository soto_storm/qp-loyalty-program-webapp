(function () {
  'use strict';

  angular.module('app.core.signInManager').controller('app.core.signInManager.signInManagerCtrl',
    ['$scope', 'ManagersSrv', LogoutCtrl]);

  function LogoutCtrl($scope, ManagersSrv) {

    $scope.manager = {};
    $scope.inputType = 'password';
    var isWrongPass = false;
    var isWrongEmail= false;

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSignIn);
    };

    $scope.hideShowPassword = function () {
      $scope.inputType = $scope.inputType === 'password' ? 'text' : 'password';
    };

    $scope.managerLogin = function (form) {

      $scope.$watchCollection('[manager.password, manager.email]', function () {
        if (isWrongPass) {
          form.password.$setValidity("wrongPassword", true);
          isWrongPass = false;
        } else if (isWrongEmail) {
          form.email.$setValidity("wrongEmail", true);
          isWrongEmail = false;
        }

        $scope.disabledSave = false;
      });

      if (form.$valid) {
        $scope.disabledSave = true;

        ManagersSrv.managerLogin($scope.manager, function (data) {

          if (data.message && data.message[0] === 'WRONG_PASSWORD') {
            isWrongPass = true;
            form.password.$setValidity("wrongPassword", false);
            $scope.disabledSave = true;
          } else if (data.message && data.message[0] === 'WRONG_EMAIL') {
            isWrongEmail = true;
            form.email.$setValidity("wrongEmail", false);
            $scope.disabledSave = true;
          } else {
            window.location.href = data.redirectTo ? data.redirectTo : "/manager";
            $scope.disabledSave = false;
          }

        })
      } else {
        $scope.tryToSignIn = true;
      }
    };

  }

})();
