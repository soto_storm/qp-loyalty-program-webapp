(function () {
  'use strict';

  angular.module('app.core.signUpRetailer').controller('app.core.signUpRetailer.signUpRetailerCtrl',
    ['$scope', 'SignUpRetailerSrv', 'ManagersSrv', LogoutCtrl]);

  function LogoutCtrl($scope, SignUpRetailerSrv, ManagersSrv) {

    $scope.newRetailer = {};
    $scope.managerName = '';
    $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    $scope.inputPassType = {
      main: 'password',
      confirm: 'password'
    };

    ManagersSrv.getAllManagers(function (resp) {
      $scope.managers = resp;
    });

    $scope.$watch('managerName', function (val) {

      if (!val || val.length === 0) {
        $scope.signUpRetailerForm.manager.$setValidity("incorrect", true);
        $scope.newRetailer.manager = '';
        $scope.disabledSaveRetailer = false;
      } else {
        if (!$scope.managers) {
          $scope.signUpRetailerForm.manager.$setValidity("incorrect", false);
          return
        }

        for (var i = 0, len = $scope.managers.length; i < len; i++) {
          if (val.toLowerCase() === $scope.managers[i].name.toLowerCase()) {
            $scope.managerName = $scope.managers[i].name;
            $scope.newRetailer.manager = $scope.managers[i]._id;
            $scope.signUpRetailerForm.manager.$setValidity("incorrect", true);
            $scope.disabledSave = false;
            return;
          }
        }

        $scope.signUpRetailerForm.manager.$setValidity("incorrect", false);
        $scope.disabledSave = true
      }
    });

    $scope.clearManager = function () {
      $scope.managerName = '';
    };

    $scope.hideShowPassword = function (valueType) {
      $scope.inputPassType[valueType] = $scope.inputPassType[valueType] === 'text' ? 'password' : 'text';
    };

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.retailerSignUp = function (form) {
      $scope.tryToSave = true;

      if (form.$valid) {
        $scope.disabledSave = true;

        SignUpRetailerSrv.retailerSignUp($scope.newRetailer, function (data) {
          window.location.href = data.redirectTo ? data.redirectTo : "/retailer";
          $scope.disabledSave = false;
        })
      }
    };
  }

})();
