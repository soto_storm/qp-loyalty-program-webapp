'use strict';

(function () {

  angular.module("app.core.signUpClient").directive("signUpClientModal",
    ['$state', '$uibModal',
      function ($state, $uibModal) {
        return {
          restrict: 'A',
          link: function ($scope, element) {

            element.on('click', function () {
              var modalInstance = $uibModal.open({
                templateUrl: 'app/core/sign_up_client/sign_up_client_modal.tpl.html',
                controller: 'app.core.signUpClient.signUpClientCtrl',
                windowClass: 'sign-up-client-modal',
                size: 'sm',
                animation: true
              });

            });
          }
        }
      }]);
})();
