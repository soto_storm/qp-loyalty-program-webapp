(function () {
  'use strict';

  angular.module('app.core.resetPassword').controller('app.core.resetPassword.resetPasswordModalCtrl',
    ['$scope', '$uibModalInstance', 'LoggerSrv', '$translate', 'SignInClientSrv', resetPasswordModalCtrl]);

  function resetPasswordModalCtrl($scope, $uibModalInstance, LoggerSrv, $translate, SignInClientSrv) {

    $scope.inputPassType = {
      main: 'password',
      confirm: 'password'
    };
    $scope.resetPasswords = {};

    $scope.hideShowPassword = function (valueType) {
      $scope.inputPassType[valueType] = $scope.inputPassType[valueType] === 'text' ? 'password' : 'text';
    };

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.resetPassword = function (form) {
      if (form.$valid) {
        $scope.disabledSave = true;

        SignInClientSrv.resetPassword($scope.resetPasswords, function () {
          $scope.disabledSave = false;
          $uibModalInstance.dismiss('cancel');
          LoggerSrv.success($translate.instant('PASSWORD_SUCCESSFULLY_CHANGED'));
        })
      } else {
        $scope.tryToSave = true;
      }
    };

    $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }

})();
