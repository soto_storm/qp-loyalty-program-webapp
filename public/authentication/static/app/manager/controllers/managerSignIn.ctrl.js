/**
 * Created by Jesus Soto on 11/2/2016.
 */
(function(){
  angular.module('home').controller('managerSignInCtrl',
    function($rootScope, $scope, ManagersSrv){
      $scope.manager = {};
      $scope.error=false;
      $scope.message='';
      $scope.login = function (form) {
        if (form.$valid) {
           ManagersSrv.managerLogin($scope.manager, function (data) {
             console.log(data);
             if(data) {
               if (data.message && data.message[0] === 'WRONG_PASSWORD') {
                 $scope.error = true;
                 $scope.message = 'Wrong password';
               } else if (data.message && data.message[0] === 'WRONG_EMAIL') {
                 $scope.error = true;
                 $scope.message = 'Wrong email';
               } else {
                 window.location.href = data.redirectTo ? data.redirectTo : "/manager";
               }
             }else{
               $scope.error = true;
               $scope.message = 'You do not have internet connection';
             }
           });
         }
      };
    });
}());
