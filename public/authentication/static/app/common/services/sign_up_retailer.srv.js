'use strict';

(function () {

  angular.module("app.common.services").service("SignUpRetailerSrv",
    ['$http', 'LoggerSrv', '$translate', SignUpRetailerSrv]);

  function SignUpRetailerSrv($http, LoggerSrv, $translate) {

    function retailerSignUp(retailer, callback) {

      var data = {
        name: retailer.name,
        password: md5(retailer.password),
        email: retailer.email
      };

      if (retailer.manager) {
        data.manager = retailer.manager;
      }

      return $http({
        method: "POST",
        url: '/api/retailer?signup=true',
        data: data
      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      })
    }

    return {
      retailerSignUp: retailerSignUp
    };
  }

})();
