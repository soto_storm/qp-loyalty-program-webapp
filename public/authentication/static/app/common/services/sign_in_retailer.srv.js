'use strict';

(function () {

  angular.module("app.common.services").service("SignInRetailerSrv",
    ['$http', 'LoggerSrv', '$translate', SignInRetailerSrv]);

  function SignInRetailerSrv($http, LoggerSrv, $translate) {

    function retailerLogin(retailer, callback) {
      return $http({
        method: "POST",
        url: '/auth/retailer',
        data: {
          email: retailer.email,
          password: md5(retailer.password)
        }

      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      })
    }

    return {
      retailerLogin: retailerLogin
    };
  }

})();
