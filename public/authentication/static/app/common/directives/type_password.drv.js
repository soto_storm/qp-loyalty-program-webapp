"use strict";
(function () {
  angular.module('app.common.directives').directive("passwordVerify",
    [passwordVerify]);

  function passwordVerify() {
    return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function (scope, element, attrs, ctrl) {

        scope.$watch(function () {
          var combined;

          if (scope.passwordVerify || ctrl.$viewValue) {
            combined = scope.passwordVerify + '_' + ctrl.$viewValue;
          }
          return combined;
        }, function (value) {

          if (value) {

            if (scope.passwordVerify !== ctrl.$viewValue) {
              ctrl.$setValidity("passwordVerify", false);
            } else {
              ctrl.$setValidity("passwordVerify", true);
              return ctrl.$viewValue;
            }
          }
        });
      }
    }
  }

}());
