/**
 * Created by Jesus Soto on 10/16/2016.
 */
(function(){
  angular.module('home').controller('sing_up_retailer.ctrl',
    function($rootScope, $scope, ManagersSrv, SignUpRetailerSrv){
      $scope.retailer={
        name:'',
        manager:'',
        email:'',
        password:''
      };

      $scope.managerName='';

      ManagersSrv.getAllManagers(function (resp) {
        $scope.managers = resp;
      });

      $scope.$watch('managerName', function (val) {

        if (!val || val.length === 0) {
          $scope.retailerRegs.manager.$setValidity("incorrect", true);
          $scope.retailer.manager = '';
        } else {
          if (!$scope.managers) {
            $scope.retailerRegs.manager.$setValidity("incorrect", false);
            return
          }

          for (var i = 0, len = $scope.managers.length; i < len; i++) {
            if (val.toLowerCase() === $scope.managers[i].name.toLowerCase()) {
              $scope.managerName = $scope.managers[i].name;
              $scope.retailer.manager = $scope.managers[i]._id;
              $scope.retailerRegs.manager.$setValidity("incorrect", true);
              return
            }
          }
        }
        $scope.retailerRegs.manager.$setValidity("incorrect", false);
      });

      $scope.signUp=function(form){
        if(form.$valid){
          SignUpRetailerSrv.retailerSignUp($scope.retailer, function (data) {
            if(data!=null){
              window.location.href="#modal-cadastro-realizado";
            }else{
              $scope.error=true;
              $scope.message='Connection failure. Check your internet';
            }
          });
        }
      };
    });
}());
