/**
 * Created by Jesus Soto on 10/16/2016.
 */
(function(){
  angular.module('home').controller('retailer_login.ctrl.js',
    function($rootScope, $scope, SignInRetailerSrv){
      $scope.error=false;
      $scope.message='';
      $scope.retailer={
        email: '',
        password: ''
      };

      $scope.login=function(form){
        if(form.$valid){
          SignInRetailerSrv.retailerLogin($scope.retailer, function (data) {
            if(data==null){
              $scope.error=true;
              $scope.message='Connection failure. Check your internet';
            }
            if(data) {
              if (data.message && data.message[0] === 'WRONG_PASSWORD') {
                $scope.error = true;
                $scope.message = 'Wrong password';
              } else if (data.message && data.message[0] === 'WRONG_EMAIL') {
                $scope.error = true;
                $scope.message = 'Retailer does not exist';
              } else {
                $scope.error = false;
                window.location.href = data.redirectTo ? data.redirectTo : "/retailer";
                $scope.disabledSave = false;
              }
            }
          });
        }
      };
    });
}());
