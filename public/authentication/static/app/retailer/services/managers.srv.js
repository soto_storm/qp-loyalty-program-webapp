(function () {
  'use strict';

  angular.module('home').factory('ManagersSrv',
    ['$http', ManagersSrv]);

  function ManagersSrv($http, LoggerSrv, $translate) {

    var initialized = false,
      initListeners;

    function getAllManagers(callback) {
      $http({
        url: '/api/manager',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function (err) {

        callback(err);

      });
    }

    function managerLogin(manager, callback) {
      if(!manager) throw 'Manager is required';

      return $http({
        method: "POST",
        url: '/auth/manager',
        data: {
          email: manager.email,
          password: md5(manager.password)
        }

      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (err) {

        callback(err);

      })
    }

    return {
      getAllManagers: getAllManagers,
      managerLogin: managerLogin
    }

  }

}());
