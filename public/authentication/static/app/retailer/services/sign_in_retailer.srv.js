'use strict';

(function () {

  angular.module("home").service("SignInRetailerSrv",
    ['$http', SignInRetailerSrv]);

  function SignInRetailerSrv($http) {

    function retailerLogin(retailer, callback) {
      return $http({
        method: "POST",
        url: '/auth/retailer',
        data: {
          email: retailer.email,
          password: md5(retailer.password)
        }

      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (err) {

        callback(err);

      })
    }

    return {
      retailerLogin: retailerLogin
    };
  }

})();
