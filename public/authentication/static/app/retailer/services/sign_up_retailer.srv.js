'use strict';

(function () {

  angular.module("home").service("SignUpRetailerSrv",
    ['$http', SignUpRetailerSrv]);

  function SignUpRetailerSrv($http) {

    function retailerSignUp(retailer, callback) {

      var data = {
        name: retailer.name,
        password: md5(retailer.password),
        email: retailer.email
      };

      if (retailer.manager) {
        data.manager = retailer.manager;
      }

      return $http({
        method: "POST",
        url: '/api/retailer?signup=true',
        data: data
      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (err) {

        callback(err);

      })
    }

    return {
      retailerSignUp: retailerSignUp
    };
  }

})();
