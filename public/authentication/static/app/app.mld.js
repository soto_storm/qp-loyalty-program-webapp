(function () {
  'use strict';

  var app = angular.module('app',
    [
      'ui.router',
      'ui.bootstrap',
      'ngMessages',
      'ui.utils.masks',
      'pascalprecht.translate',

      'app.common.services',
      'app.common.directives',
      'app.core.main',
      'app.core.header',
      'app.core.resetPassword',
      'app.core.signInClient',
      'app.core.signInRetailer',
      'app.core.signUpClient',
      'app.core.signUpRetailer',
      'app.core.signInManager'
    ]);

  app.config(['$translateProvider', translate]);
  app.run(['$rootScope', '$translate', rootScope]);
  app.config(['toastr', toastrConfig]);
  app.config(['$httpProvider', interceptor]);
  app.constant('toastr', toastr);

  function toastrConfig(toastr) {
    toastr.options = {
      progressBar: true,
      timeOut: 10000,
      positionClass: 'toast-top-right'
    };
  }

  function translate($translateProvider) {

    $translateProvider.useSanitizeValueStrategy('escape');

    $translateProvider.useStaticFilesLoader({
      prefix: 'app/i18n/locale_',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');
  }

  function rootScope($rootScope, $translate) {
    $rootScope.currentLang = 'en';
    $rootScope.emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    $rootScope.changeLanguage = function (langKey) {
      $rootScope.currentLang = langKey;
      $translate.use(langKey);
    };
  }


  function interceptor($httpProvider) {

    $httpProvider.interceptors.push(function () {
      return {
        request: function (config) {

          if (config.url) {

            if (!config.params) {
              config.params = {};
            }

            if (!(config.url.indexOf('.html') >= 0)) {
              config.params.t = new Date().getTime();
            }
          }

          return config;
        }
      }
    });
  }

})();
