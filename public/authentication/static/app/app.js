/**
 * Created by Jesus Soto on 10/14/2016.
 */
var app = angular.module('home',
                        ['ngMessages',
                         'ngCpfCnpj',
                         'ui.mask',
                         'ngRoute',
                         'bootstrapLightbox',
                         'ui.utils.masks']
                        ).run(function($rootScope,SignUpClientSrv){});
