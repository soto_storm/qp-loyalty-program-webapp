/**
 * Created by Jesus Soto on 10/14/2016.
 */
(function(){
  angular.module('home').controller('clientRegsCtrl',
    function($rootScope, $scope, SignUpClientSrv){
        $scope.client={};
        $scope.confirmation=false;
        $scope.message='';
        $scope.client.conf='';
        $scope.client.birth='';
        $scope.InvalidDate=false;
        $scope.finalError = false;
        $scope.dateMessage='';
        $scope.gender = "M";
        $scope.passConf={
          color:'green'
        };
        $scope.passwordConfirmation=function(){
            $scope.confirmation=true;
            if($scope.client.conf.length>1) {
              if (($scope.client.nova === $scope.client.conf)) {
                $scope.message = 'Passwords match!';
                $scope.passConf.color='green';
              } else {
                $scope.message = 'Passwords does not match.!';
                $scope.passConf.color='red';
              }
            }else{
              $scope.message='You must confirm your password';
              $scope.passConf.color='red';
            }
        };

        $scope.dismatchPassword=function(){
          $scope.message='You must confirm your password';
          $scope.passConf.color='red';
        };

      function converToDate(date){
          var v = date.toString().split('');
          var day='';
          var month='';
          var year='';
          for(var i=0; i<2; i++){
            day=day+date[i];
          }
          for(var j=2; j<4; j++){
            month=month+date[j];
          }
          for(var k=4; k<8; k++){
            year=year+date[k];
          }
        if(parseInt(year)>2000){
          $scope.InvalidDate=true;
          $scope.dateMessage='You must be at least 16 years old.';
        }else{
          try{
            date=new Date(year+'-'+month+'-'+day);
            $scope.InvalidDate=false;
            $scope.client.birth=new Date(year+'-'+month+'-'+day);
            if(date==='Invalid Date'){
              $scope.InvalidDate=true;
              $scope.dateMessage='Invalid Date';
              return false;
            }
          }catch(error){
            console.error(error);
          }
        }
        var result={
          valid:true,
          date:year+'-'+month+'-'+day
        };
        return result;
      }

      $scope.clientSignUp = function (form) {
        converToDate($scope.client.birth);
        if($scope.client.gender){
          $scope.client.gender="F";
        }else{
          $scope.client.gender="M";
        }
          if(form.$valid){
            if(!$scope.dateMessage){
              if($scope.passConf.color==='green'){
                if($scope.client.birth==='Invalid Date'){
                  $scope.InvalidDate=true;
                  $scope.dateMessage='Invalid Date';
                }else{
                  SignUpClientSrv.clientSignUp($scope.client, function (data) {                    
                    if(data!=null) {
                      if (data.message === 'Client already exist') {
                        $scope.message = data.message;
                        $scope.passConf.color = 'red';
                      }else{
                        window.location.href = "#modal-cadastro-realizado";
                      }
                    }else{
                        $scope.message='No internet connection';
                        $scope.passConf.color='red';
                    }
                  });
                }
              }
            }
          }
      };
    });
}());
