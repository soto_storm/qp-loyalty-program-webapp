/**
 * Created by Jesus Soto on 10/16/2016.
 */
(function(){
  angular.module('home').controller('clientLoginCtrl',
    function($rootScope, $scope, SignInClientSrv){
      $scope.client={
        cpf:'',
        password:''
      };
      $scope.error=false;
      $scope.success=false;
      $scope.message='';

      $scope.singIn=function(form) {
        if(($scope.client.cpf==='')||($scope.client.password==='')){}else{
          if(form.$valid) {
            SignInClientSrv.clientLogin($scope.client, function (data) {
              if(data!=null) {
                if (form.$valid) {
                  if (data.message && data.message[0] === 'WRONG_PASSWORD') {
                    $scope.error = true;
                    $scope.message = 'Wrong Password';
                  } else if (data.message && data.message[0] === 'WRONG_CPF') {
                    $scope.error = true;
                    $scope.message = 'User does not exists';
                  } else {
                    window.location.href = data.redirectTo ? data.redirectTo : "/client";
                  }
                }
              }else {
                $scope.error=true;
                $scope.message='Internet connection problem. Try again later.';
              }
            });
          }
        }
      };

      $scope.sendCpf = function (form) {
       if($scope.client.cpf===''){}else {
           if(form.$valid) {
             SignInClientSrv.forgotPassword($scope.client.cpf, function (data) {
               console.log(data);
               if(data===null){
                 $scope.error = true;
                 $scope.success = false;
                 $scope.message = 'Connection failed :( . Please check your internet.';
               }else {
                 if (data.error) {
                   $scope.error = true;
                   $scope.success = false;
                   $scope.message = 'This customer does not exists';
                 } else {
                   $scope.success = true;
                   $scope.error = false;
                 }
               }
             });
           }
       }
      };

      $scope.cleanMessages=function(){
        $scope.success=false;
        $scope.error=false;
      };
    });
}());
