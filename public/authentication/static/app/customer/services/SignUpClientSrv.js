/**
 * Created by Jesus Soto on 10/14/2016.
 */
'use strict';

(function () {

  angular.module('home').service("SignUpClientSrv",
    ['$http', SignUpClientSrv]);

  function SignUpClientSrv($http) {

    function clientSignUp(client, callback) {
      var newClient = {
        name: client.name,
        cpf: client.cpf,
        email: client.email,
        birthday: client.birth,
        gender: client.gender
      };

      if (client.conf) {
        newClient.password = md5(client.conf);
      }

      return $http({
        method: "POST",
        url: '/api/client?signup=true',
        data: JSON.stringify(newClient)
      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (error) {
          callback(error);
      })
    }

    return {
      clientSignUp: clientSignUp
    };
  }

})();
