'use strict';

(function () {

  angular.module("home").service("SignInClientSrv",
    ['$http', SignInClientSrv]);

  function SignInClientSrv($http) {

    function clientLogin(client, callback) {
      return $http({
        method: "POST",
        url: '/auth/client',
        data: {
          cpf: client.cpf,
          password: md5(client.password)
        }

      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (err) {

        callback(err);

      })
    }

    function forgotPassword(cpf, callback) {
      return $http({
        method: "GET",
        url: '/api/mail/forgotPassword?cpf=' + cpf
      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (err) {

        callback(err);

      })
    }

    function resetPassword(passwords, callback) {
      return $http({
        method: "POST",
        url: '/api/client/resetPassword?token=' + urlParam('token'),
        data: {
          password: md5(passwords.password)
        }
      }).success(function (response) {
        if (callback) {
          callback(response)
        }
      }).error(function (err) {

        callback(err);

      })
    }

    function urlParam(name) {
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      if (!results) {
        return ''
      }
      return results[1] || 0;
    }

    return {
      clientLogin: clientLogin,
      forgotPassword: forgotPassword,
      resetPassword: resetPassword
    };
  }

})();
