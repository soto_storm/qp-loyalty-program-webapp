(function () {
  'use strict';

  angular.module('app').config(['$stateProvider', '$httpProvider', '$urlRouterProvider', config]);

  /*@ngInject*/
  function config($stateProvider, $httpProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('main');

    $stateProvider
      .state('main', {
        url: '/main',
        templateUrl: 'app/core/main/main.tpl.html'
      })
      .state('resetPassword', {
        url: '/resetPassword',
        templateUrl: 'app/core/main/main.tpl.html',
        controller: 'app.core.resetPassword.resetPasswordCtrl',
      })
      .state('client', {
        url: '/client',
        templateUrl: 'app/core/sign_in_client/sign_in_client_modal.tpl.html',
        controller: 'app.core.signInClient.signInClientCtrl',
      })
      .state('retailer', {
        url: '/retailer',
        templateUrl: 'app/core/sign_in_retailer/sign_in_retailer_modal.tpl.html',
        controller: 'app.core.signInRetailer.signInRetailerCtrl',
      })
      .state('manager', {
        url: '/manager',
        templateUrl: 'app/core/sign_in_manager/sign_in_manager.tpl.html',
        controller: 'app.core.signInManager.signInManagerCtrl',
      });
  }

})();
