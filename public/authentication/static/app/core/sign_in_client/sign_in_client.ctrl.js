(function () {
  'use strict';

  angular.module('app.core.signInClient').controller('app.core.signInClient.signInClientCtrl',
    ['$scope', 'SignInClientSrv', 'LoggerSrv', '$translate', LogoutCtrl]);

  function LogoutCtrl($scope, SignInClientSrv, LoggerSrv, $translate) {

    $scope.client = {};
    $scope.clientCpf = '';
    $scope.inputType = 'password';
    var isWrongPass = false;
    var isWrongCPF = false;

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSaveSignIn);
    };

    $scope.hideShowPassword = function () {
      $scope.inputType = $scope.inputType === 'password' ? 'text' : 'password';
    };

    $scope.clientLogin = function (form) {

      $scope.$watchCollection('[client.password, client.cpf]', function () {
        if (isWrongPass) {
          form.password.$setValidity("wrongPassword", true);
          isWrongPass = false;
        } else if (isWrongCPF) {
          form.cpf.$setValidity("wrongCpf", true);
          isWrongCPF = false;
        }

        $scope.disabledSaveSignIn = false;
      });

      if (form.$valid) {
        $scope.disabledSaveSignIn = true;

        SignInClientSrv.clientLogin($scope.client, function (data) {

          if (data.message && data.message[0] === 'WRONG_PASSWORD') {
            isWrongPass = true;
            form.password.$setValidity("wrongPassword", false);
          } else if (data.message && data.message[0] === 'WRONG_CPF') {
            isWrongCPF = true;
            form.cpf.$setValidity("wrongCpf", false);
          } else {
            window.location.href = data.redirectTo ? data.redirectTo : "/client";
            $scope.disabledSaveSignIn = false;
          }
        })
      } else {
        $scope.tryToSaveSignIn = true;
      }
    };

    $scope.getFacebookClient = function (form) {
      if (form.$valid) {

        FB.login(function (response) {

          FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
              FB.api('/me', {locale: 'en_US', fields: 'name, email, birthday, gender'},
                function (response) {
                  if (response && !response.error) {

                    if (!response.name || !response.email || !response.gender) {
                      LoggerSrv.warning($translate.instant('CANT_GET_REQUIRED_INFO'));
                      return
                    }

                    var client = {
                      cpf: $scope.client.cpf,
                      name: response.name,
                      email: response.email,
                      gender: response.gender == 'male' ? 'M' : 'F'
                    };

                    SignUpClientSrv.clientSignUp(client, function (data) {
                      window.location.href = data.redirectTo ? data.redirectTo : "/client";
                      $scope.disabledSaveFacebook = false;
                    })
                  } else {
                    LoggerSrv.error($translate.instant('SERVER_ERROR'));
                  }
                }
              );
            }
            else {
              var win = window.open('http://www.facebook.com', '_blank');
              if (win) {
                win.focus();
              } else {
                window.open("http://www.facebook.com", "_self")
              }
            }
          });

        }, {scope: 'email'});

      }
    }    

  }

})();
