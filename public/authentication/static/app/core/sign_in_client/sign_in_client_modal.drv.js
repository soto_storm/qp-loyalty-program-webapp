'use strict';

(function () {

  angular.module("app.core.signInClient").directive("signInClientModal",
    ['$state', '$uibModal',
      function ($state, $uibModal) {
        return {
          restrict: 'A',
          link: function ($scope, element) {

            element.on('click', function () {

              var modalInstance = $uibModal.open({
                templateUrl: 'app/core/sign_in_client/sign_in_client_modal.tpl.html',
                controller: 'app.core.signInClient.signInClientCtrl',
                windowClass: 'sign-in-client-modal',
                size: 'sm',
                //backdrop: 'static',
                animation: true
              });

            });
          }
        }
      }]);
})();
