(function () {
  'use strict';

  angular.module('app.core.signInClient').controller('app.core.signInClient.signInClientFacebookCtrl',
    ['$scope', 'SignUpClientSrv', 'LoggerSrv', '$translate', signInClientFacebookCtrl]);

  function signInClientFacebookCtrl($scope, SignUpClientSrv, LoggerSrv, $translate) {

    $scope.clientCpf = '';

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSaveFacebook);
    };

    $scope.getFacebookClient = function (form) {
      if (form.$valid) {
        $scope.disabledSaveFacebook = true;

        FB.login(function (response) {

          FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
              FB.api('/me', {locale: 'en_US', fields: 'name, email, birthday, gender'},
                function (response) {
                  if (response && !response.error) {

                    if (!response.name || !response.email || !response.gender) {
                      LoggerSrv.warning($translate.instant('CANT_GET_REQUIRED_INFO'));
                      return
                    }

                    var client = {
                      cpf: $scope.clientCpf,
                      name: response.name,
                      email: response.email,
                      gender: response.gender == 'male' ? 'M' : 'F'
                    };

                    SignUpClientSrv.clientSignUp(client, function (data) {
                      window.location.href = data.redirectTo ? data.redirectTo : "/client";
                      $scope.disabledSaveFacebook = false;
                    })
                  } else {
                    LoggerSrv.error($translate.instant('SERVER_ERROR'));
                  }
                }
              );
            }
            else {
              var win = window.open('http://www.facebook.com', '_blank');
              if (win) {
                win.focus();
              } else {
                window.open("http://www.facebook.com", "_self")
              }
            }
          });

        }, {scope: 'email'});

      } else {
        $scope.tryToSaveFacebook = true;
      }
    }

  }

})();
