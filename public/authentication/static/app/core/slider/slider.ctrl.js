(function(){
  angular.module('home').controller('SliderCtrl', function($rootScope, $scope, Lightbox){
      $scope.images = [
      {
        'url': 'assets/images/slider/ferramenta_01.png',
        'thumbUrl': 'assets/images/slider/ferramenta_01.png' // used only for this example
      },
      {
        'url': 'assets/images/slider/ferramenta_02.png',
        'thumbUrl': 'assets/images/slider/ferramenta_02.png' // used only for this example
      },
      {
        'url': 'assets/images/slider/ferramenta_03.png',
        'thumbUrl': 'assets/images/slider/ferramenta_03.png' // used only for this example
      }
    ];

    $scope.openLightboxModal = function (index) {
      Lightbox.openModal($scope.images, index);
    };
});
}());