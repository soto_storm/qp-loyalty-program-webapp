'use strict';

(function () {

  angular.module("app.core.signUpRetailer").directive("signUpRetailerModal",
    ['$state', '$uibModal',
      function ($state, $uibModal) {
        return {
          restrict: 'A',
          link: function ($scope, element) {

            element.on('click', function () {
              $uibModal.open({
                templateUrl: 'app/core/sign_up_retailer/sign_up_retailer_modal.tpl.html',
                controller: 'app.core.signUpRetailer.signUpRetailerCtrl',
                windowClass: 'sign-up-retailer-modal',
                size: 'sm',
                animation: true
              });

            });
          }
        }
      }]);
})();
