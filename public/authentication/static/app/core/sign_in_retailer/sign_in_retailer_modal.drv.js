'use strict';

(function () {

  angular.module("app.core.signInRetailer").directive("signInRetailerModal",
    ['$state', '$uibModal',
      function ($state, $uibModal) {
        return {
          restrict: 'A',
          link: function ($scope, element) {

            element.on('click', function () {

              var modalInstance = $uibModal.open({
                templateUrl: 'app/core/sign_in_retailer/sign_in_retailer_modal.tpl.html',
                controller: 'app.core.signInRetailer.signInRetailerCtrl',
                windowClass: 'sign-in-retailer-modal',
                size: 'sm',
                //backdrop: 'static',
                animation: true
              });

            });
          }
        }
      }]);
})();
