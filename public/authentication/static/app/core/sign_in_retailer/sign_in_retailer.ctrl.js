(function () {
  'use strict';

  angular.module('app.core.signInRetailer').controller('app.core.signInRetailer.signInRetailerCtrl',
    ['$scope', 'SignInRetailerSrv', signInRetailerCtrl]);

  function signInRetailerCtrl($scope, SignInRetailerSrv) {

    $scope.retailer = {};
    $scope.inputType = 'password';
    var isWrongPass = false;
    var isWrongEmail = false;


    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$dirty || $scope.tryToSave);
    };

    $scope.hideShowPassword = function () {
      $scope.inputType = $scope.inputType === 'password' ? 'text' : 'password';
    };

    $scope.signInRetailer = function (form) {

      $scope.$watchCollection('[retailer.password, retailer.email]', function () {
        if (isWrongPass) {
          form.password.$setValidity("wrongPassword", true);
          isWrongPass = false;
        } else if (isWrongEmail) {
          form.email.$setValidity("wrongEmail", true);
          isWrongEmail = false;
        }

        $scope.disabledSave = false;
      });

      if (form.$valid) {
        $scope.disabledSave = true;

        SignInRetailerSrv.retailerLogin($scope.retailer, function (data) {

          if (data.message && data.message[0] === 'WRONG_PASSWORD') {
            isWrongPass = true;
            form.password.$setValidity("wrongPassword", false);
            $scope.disabledSave = true;
          } else if (data.message && data.message[0] === 'WRONG_EMAIL') {
            isWrongEmail = true;
            form.email.$setValidity("wrongEmail", false);
            $scope.disabledSave = true;
          } else {
            window.location.href = data.redirectTo ? data.redirectTo : "/retailer";
            $scope.disabledSave = false;
          }
        })
      } else {
        $scope.tryToSave = true;
      }
    };

  }

})();
