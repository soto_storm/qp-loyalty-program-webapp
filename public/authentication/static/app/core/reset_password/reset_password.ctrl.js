(function () {
  'use strict';

  angular.module('app.core.resetPassword').controller('app.core.resetPassword.resetPasswordCtrl',
    ['$scope', '$uibModal', '$state', resetPasswordCtrl]);

  function resetPasswordCtrl($scope, $uibModal, $state) {

    if (!urlParam('token')) {
      $state.go('main')
    }

    $uibModal.open({
      templateUrl: 'app/core/reset_password/reset_password_modal.tpl.html',
      controller: 'app.core.resetPassword.resetPasswordModalCtrl',
      windowClass: 'reset_password-modal',
      size: 'sm',
      animation: true
    });

    function urlParam(name) {
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      if (!results) {
        return ''
      }
      return results[1] || 0;
    }
  }

})();
