(function () {
  'use strict';

  angular.module('app.core.signUpClient').controller('app.core.signUpClient.signUpClientCtrl',
    ['$scope', 'SignUpClientSrv', LogoutCtrl]);

  function LogoutCtrl($scope, SignUpClientSrv) {

    $scope.newClient = {};
    $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    $scope.newClient.gender = 'M';
    $scope.popup = {
      opened: false
    };
    $scope.inputPassType = {
      main: 'password',
      confirm: 'password'
    };

    $scope.dateOptions = {
      maxDate: new Date(),
      startingDay: 1
    };

    $scope.gender = {
      M: 'male',
      F: 'female'
    };

    $scope.chooseGender = function (key) {
      $scope.newClient.gender = key;
    };

    $scope.openCalendar = function () {
      $scope.popup.opened = true;
    };

    $scope.hideShowPassword = function (valueType) {
      $scope.inputPassType[valueType] = $scope.inputPassType[valueType] === 'text' ? 'password' : 'text';
    };

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.clientSignUp = function (form) {
      $scope.tryToSave = true;

      if (form.$valid) {
        $scope.disabledSave = true;

        SignUpClientSrv.clientSignUp($scope.newClient, function (data) {
          window.location.href = data.redirectTo ? data.redirectTo : "/client";
          $scope.disabledSave = false;
        })
      }
    };
  }

})();
