(function () {
  'use strict';

  var app = angular.module('app', [
    'ngResource',
    'ngSanitize',
    'ngStorage',
    'ui.router',
    'ngplus',
    'angular-loading-bar',
    'ui.bootstrap',
    'ui.utils.masks',
    'idf.br-filters',
    'pascalprecht.translate',
    'oitozero.ngSweetAlert',
    'ngMessages',
    'ngclipboard',
    'chart.js',

    'app.common.services',
    'app.common.filters',
    'app.common.directives',
    'app.core.clientsGroup',
    'app.core.rechargeDots',
    'app.core.profile',
    'app.core.rewards',
    'app.core.support',
    'app.core.reports',
    'app.core.stores',
    'app.core.rightPanel',
    'app.core.header'
  ]);

  app.config(['$translateProvider', translate]);
  app.run(['$rootScope', '$translate', 'RetailersSrv', '$locale', rootScope]);
  app.config(['toastr', toastrConfig]);
  app.config(['$httpProvider', interceptor]);
  app.constant('toastr', toastr);

  function toastrConfig(toastr) {
    toastr.options = {
      progressBar: true,
      timeOut: 10000,
      positionClass: 'toast-top-right'
    };
  }

  function translate($translateProvider) {

    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('escape');

    $translateProvider.useStaticFilesLoader({
      prefix: 'app/i18n/locale-',
      suffix: '.json'
    });
  }

  function rootScope($rootScope, $translate, RetailersSrv, $locale) {
    $rootScope.currentLang = 'en';
    $locale.NUMBER_FORMATS.DECIMAL_SEP = ',';
    $locale.NUMBER_FORMATS.GROUP_SEP = '.';

    $rootScope._loadCurrentRetailer = function (callback) {
      RetailersSrv.getCurrentRetailer(function (data) {
        $rootScope.currentRetailer = angular.copy(data);

        if (callback) {
          callback($rootScope.currentRetailer)
        }
      });
    };
    $rootScope._loadCurrentRetailer();

    $rootScope.logout = function () {
      RetailersSrv.logoutRetailer(function (data) {
        window.location.href = data.redirectTo ? data.redirectTo : "/authentication";
      })
    };

    $rootScope.changeLanguage = function (langKey) {
      $rootScope.currentLang = langKey;
      $translate.use(langKey);
    };
  }

  function interceptor($httpProvider) {

    $httpProvider.interceptors.push(function () {
      return {
        request: function (config) {

          if (config.url) {

            if (!config.params) {
              config.params = {};
            }

            if (!(config.url.indexOf('.html') >= 0)) {
              config.params.t = new Date().getTime();
            }
          }

          return config;
        }
      }
    });
  }

})();
