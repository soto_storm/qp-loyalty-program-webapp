(function () {
  'use strict';

  angular.module('app.core.rewards').controller('app.core.rewards.rewardsCtrl',
    ['$scope', 'RewardsSrv', rewardCtrl]);

  function rewardCtrl($scope, RewardsSrv) {

    RewardsSrv.getRewardsByRetailerId($scope.currentRetailer._id, function (data) {
      $scope.rewards = data;
    });

    $scope.editAddReward = function (rewardId) {
      $scope.editRewardId = rewardId;
    };
  }

})();
