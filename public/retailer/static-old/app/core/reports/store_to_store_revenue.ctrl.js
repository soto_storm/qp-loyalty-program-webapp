(function () {
  'use strict';

  angular.module('app.core.reports').controller('app.core.reports.soreToStoreRevenueCtrl',
    ['$scope', '$filter', storeRevenueBarCtrl]);

  function storeRevenueBarCtrl($scope, $filter) {

    var maxBarOnPage;
    var months = $scope.revenue.storesRevenue.months;
    var points = $scope.revenue.storesRevenue.points;
    var stores = $scope.revenue.storesRevenue.stores;

    switch (true) {
      case stores.length <= 6 :
        maxBarOnPage = 6;
        break;
      case stores.length <= 12 :
        maxBarOnPage = 3;
        break;
      default :
        maxBarOnPage = 1;
        break;
    }

    var pageStatus = {
      start: 1,
      end: Math.ceil(months.length / maxBarOnPage),
      current: 1,
      maxBarOnPage: maxBarOnPage
    };

    $scope.options = {
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true
          }
        }]
      },
      legend: {display: true}
    };

    goToPage(1);
    updateDisabled();

    $scope.next = function () {
      if (!$scope.disableNext) {
        goToPage(--pageStatus.current);
        updateDisabled();
      }
    };

    $scope.previous = function () {
      if (!$scope.disablePrevious) {
        goToPage(++pageStatus.current);
        updateDisabled();
      }
    };

    function goToPage(page) {
      var fromSlice = -page * pageStatus.maxBarOnPage;
      var toSlice = page === 1 ? undefined : -(page - 1) * pageStatus.maxBarOnPage;

      var monthDates = months.slice(fromSlice, toSlice);

      $scope.months = monthDates.map(function (date) {
        return $filter('date')(date, 'MMM, y')
      });

      $scope.points = points.map(function (storePoints) {
        return storePoints.slice(fromSlice, toSlice);
      });

      $scope.stores = stores.slice(fromSlice, toSlice);
    }

    function updateDisabled() {
      $scope.disableNext = pageStatus.current === pageStatus.start;
      $scope.disablePrevious = pageStatus.current === pageStatus.end;
    }

  }
  
})();
