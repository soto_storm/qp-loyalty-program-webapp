(function () {
  'use strict';

  angular.module('app.core.reports').controller('app.core.reports.totalRevenueCtrl',
    ['$scope', '$filter', storeRevenueBarCtrl]);

  function storeRevenueBarCtrl($scope, $filter) {

    var maxBarOnPage = 12;

    var months = $scope.revenue.totalRevenue.months;
    var points = $scope.revenue.totalRevenue.points;

    var pageStatus = {
      start: 1,
      end: Math.ceil(months.length / maxBarOnPage),
      current: 1,
      maxBarOnPage: maxBarOnPage
    };

    $scope.options = {
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true
          }
        }]
      }
    };

    goToPage(1);
    updateDisabled();

    $scope.next = function () {
      if (!$scope.disableNext) {
        goToPage(--pageStatus.current);
        updateDisabled();
      }
    };

    $scope.previous = function () {
      if (!$scope.disablePrevious) {
        goToPage(++pageStatus.current);
        updateDisabled();
      }
    };

    function goToPage(page) {
      var fromSlice = -page * pageStatus.maxBarOnPage;
      var toSlice = page === 1 ? undefined : -(page - 1) * pageStatus.maxBarOnPage;

      var monthDates = months.slice(fromSlice, toSlice);

      $scope.months = monthDates.map(function (date) {
        return $filter('date')(date, 'MMM, y')
      });
      $scope.points = points.slice(fromSlice, toSlice);
    }

    function updateDisabled() {
      $scope.disableNext = pageStatus.current === pageStatus.start;
      $scope.disablePrevious = pageStatus.current === pageStatus.end;
    }

  }

})();
