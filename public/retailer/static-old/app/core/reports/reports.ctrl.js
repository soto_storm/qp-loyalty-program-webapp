(function () {
  'use strict';

  angular.module('app.core.reports').controller('app.core.reports.reportsCtrl',
    ['$scope', 'ClientsSrv', 'RetailersSrv', 'StoresSrv', reportsCtrl]);

  function reportsCtrl($scope, ClientsSrv, RetailersSrv, StoresSrv) {

    $scope.statusMap = {
      '1': 'Issued',
      '2': 'Concluded',
      '3': 'Expired'
    };

    $scope.genderLabels = ["Male", "Female"];
    $scope.ageGroupLabels = [
      "Age Group 17 or less",
      "Age Group from 18 to 25",
      "Age Group from 26 to 30",
      "Age Group from 30 to 40",
      "Age Group from 41 or more"
    ];

    getFilterClients({'onlyLength': true});
    getFilterClients({'profitable': 10});
    getFilterClients({'birthday': true});
    getFilterClients({'genderGraphData': true});
    getFilterClients({'ageGroupGraphData': true});

    RetailersSrv.exchangedRewardsToVouchers(function (resp) {
        $scope.rewardsInVouchers = resp;
      }
    );

    StoresSrv.getRetailerRevenue(function (revenue) {
      $scope.revenue = revenue
    });

    RetailersSrv.retailerLastActivity(function (resp) {
      $scope.retailerLastActivity = resp;
    });

    function getFilterClients(option) {

      ClientsSrv.getFilterClients(option, function (resp) {

        if (option.onlyLength) {
          $scope.numberOfClients = resp.value;
        } else if (option.profitable) {
          $scope.mostProfitableClients = resp;
        } else if (option.birthday) {
          $scope.clientsBirthday = resp || [];

          $scope.clientsBirthday.sort(function (a, b) {
            if (new Date(a.birthday).getMonth() > new Date(b.birthday).getMonth()) {
              return 1;
            }
            if (new Date(a.birthday).getMonth() === new Date(b.birthday).getMonth()) {
              if (new Date(a.birthday).getDate() > new Date(b.birthday).getDate()) {
                return 1;
              } else {
                return -1;
              }
            }
            if (new Date(a.birthday).getMonth() < new Date(b.birthday).getMonth()) {
              return -1;
            }
            return 0;
          });
        } else if (option.genderGraphData) {
          if ((+resp.male + +resp.female) === 0) {
            $scope.gender = [];
          } else {
            $scope.gender = [resp.male, resp.female];
          }
        } else if (option.ageGroupGraphData) {
          if (!+resp.TO_17 && !+resp.FROM_18_TO_25 && !+resp.FROM_26_TO_30 && !+resp.FROM_31_TO_40 && !+resp.FROM_41) {
            $scope.ageGroup = [];
          } else {
            $scope.ageGroup = [resp.TO_17, resp.FROM_18_TO_25, resp.FROM_26_TO_30, resp.FROM_31_TO_40, resp.FROM_41];
          }
        }

      })
    }

  }

})();
