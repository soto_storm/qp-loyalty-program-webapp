(function () {
  'use strict';

  angular.module('app.core.reports').directive('scrollToClient',
    ['$state', function ($state) {
      return {
        scope: {
          client: '=scrollToClient'
        },
        link: function (scope, element) {

          $(element).click(function () {
            $state.go('clientsGroup', {emit: scope.client._id});
          })
        }
      }
    }]);

})();
