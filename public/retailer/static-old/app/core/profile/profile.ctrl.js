(function () {
  'use strict';

  angular.module('app.core.profile').controller('app.core.profile.profileCtrl',
    ['$scope', 'RetailersSrv', 'ManagersSrv', 'LoggerSrv', '$translate', profileCtrl]);

  function profileCtrl($scope, RetailersSrv, ManagersSrv, LoggerSrv, $translate) {

    var isWrongPass = false;

    $scope.isCollapsed = {
      value: true
    };
    $scope.managerName = '';
    $scope.editRetailer = angular.copy($scope.currentRetailer);
    $scope.emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    $scope.passwords = {};
    $scope.inputPassType = {
      main: 'password',
      new: 'password',
      confirmNew: 'password'
    };

    ManagersSrv.getAllManagers(function (resp) {
      $scope.managers = resp;
      if ($scope.editRetailer.manager) {
        for (var i = 0, len = $scope.managers.length; i < len; i++) {
          if ($scope.managers[i]._id === $scope.editRetailer.manager) {
            $scope.managerName = $scope.managers[i].name;
            return;
          }
        }
      }
    });

    $scope.selectManager = function (manager) {
      $scope.editRetailer.manager = manager._id;
    };

    $scope.clearManager = function () {
      $scope.editRetailer.manager = null;
    };

    $scope.changeCollapse = function (form) {

      if ($scope.isCollapsed.value) {
        $scope.passwords = {};
        form.$setUntouched();
        $scope.tryToSavePass = false;
      }

      $scope.isCollapsed.value = !$scope.isCollapsed.value;
    };

    $scope.hideShowPassword = function (valueType) {
      $scope.inputPassType[valueType] = $scope.inputPassType[valueType] === 'text' ? 'password' : 'text';
    };

    $scope.isShowErrorsRetailer = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSaveRetailer);
    };

    $scope.isShowErrorsPass = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSavePass);
    };

    $scope.setNewPassword = function (form) {

      $scope.$watch('passwords.password', function () {
        if (isWrongPass) {
          form.password.$setValidity("wrongPassword", true);
          isWrongPass = false;
        }
      });

      if (form.$valid) {
        $scope.disabledSave = true;

        var retailerPasswords = {
          password: $scope.passwords.password,
          newPassword: $scope.passwords.newPassword
        };

        RetailersSrv.changePassword($scope.editRetailer._id, retailerPasswords, function (resp) {

          if (resp.error) {
            isWrongPass = true;
            form.password.$setValidity("wrongPassword", false);
          } else {
            $scope.passwords = {};
            form.$setUntouched();
            $scope.tryToSavePass = false;
            $scope.changeCollapse(form);
            LoggerSrv.success($translate.instant('PASSWORD_HAS_BEEN_CHANGED'));
          }
        });
      } else {
        $scope.tryToSavePass = true;
      }
    };

    $scope.saveRetailer = function (form) {

      if (form.$valid) {
        var oldRetailer = $scope.currentRetailer;

        var isNameChange = $scope.editRetailer.name !== oldRetailer.name;
        var isEmailChange = $scope.editRetailer.email !== oldRetailer.email;
        var isManagerChange = $scope.editRetailer.manager !== oldRetailer.manager;

        if (isNameChange || isEmailChange || isManagerChange) {

          $scope.disabledSaveRetailer = true;

          var retailer = {
            _id: $scope.editRetailer._id,
            name: $scope.editRetailer.name,
            email: $scope.editRetailer.email,
            manager: $scope.editRetailer.manager
          };

          RetailersSrv.saveRetailer(retailer, function (resp) {
            $scope._loadCurrentRetailer();
            $scope.disabledSaveRetailer = false;
            $scope.tryToSaveRetailer = false;
            LoggerSrv.success($translate.instant('USER_HAS_BEEN_CHANGED'));
          });
        }
      } else {
        $scope.tryToSaveRetailer = true;
      }
    };
  }
})();
