(function () {
  'use strict';

  angular.module('app.core.support').controller('app.core.support.supportCtrl',
    ['$scope', 'ManagersSrv', 'LoggerSrv', '$translate', 'RetailersSrv', supportCtrl]);

  function supportCtrl($scope, ManagersSrv, LoggerSrv, $translate, RetailersSrv) {

    var topicMap = {
      'SERVER_ERROR': 'Server Error',
      'POINTS_NOT_RECHARGE': 'Points not recharge',
      'EMAIL_VALIDATION': 'Email validation'
    };

    ManagersSrv.getManagerByRetailerId($scope.currentRetailer.manager, function (resp) {
      $scope.manager = resp;
    });

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.setTopic = function (option) {
      $scope.topic = topicMap[option];
    };

    $scope.send = function (form) {
      if (form.$valid) {

        $scope.disabledSave = true;

        var message = {
          message: $scope.message,
          topic: $scope.topic,
          subject: $scope.subject,
          type: 'SUPPORT_MESSAGE'
        };

        RetailersSrv.supportEmail(message, function (resp) {
         $scope.disabledSave = false;
        });

        $scope.message = '';
        $scope.topic = '';
        $scope.subject = '';

        form.message.$setUntouched();
        form.topic.$setUntouched();
        form.subject.$setUntouched();
        $scope.tryToSave = false;
        $scope.disabledSave = false;
        LoggerSrv.success($translate.instant('MESSAGE_SUCCESSFULLY_SEND'));

      } else {
        $scope.tryToSave = true;
      }
    }
  }

})();
