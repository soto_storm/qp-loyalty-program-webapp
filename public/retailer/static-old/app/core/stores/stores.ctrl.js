(function () {
  'use strict';

  angular.module('app.core.stores').controller('app.core.stores.storesCtrl',
    ['$scope', 'StoresSrv', storesCtrl]);

  function storesCtrl($scope, StoresSrv) {

    $scope.editStoreId = '';

    StoresSrv.getStoreByRetailer($scope.currentRetailer._id, function (data) {
      $scope.stores = data;
    });

    $scope.editAddStore = function (store) {
      $scope.editStoreId = store;
    };
  }
})();
