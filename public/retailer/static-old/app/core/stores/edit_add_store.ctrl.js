(function () {
  'use strict';

  angular.module('app.core.stores').controller('app.core.stores.editAddStoreCtrl',
    ['$scope', 'StoresSrv', 'ManagersSrv', 'LoggerSrv', '$translate', editAddStoreCtrl]);

  function editAddStoreCtrl($scope, StoresSrv, ManagersSrv, LoggerSrv, $translate) {

    $scope.editStore = $scope.store ? angular.copy($scope.store) : {};

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    ManagersSrv.getAllManagers(function (resp) {

      $scope.managers = resp;
      if ($scope.editStore.manager) {
        for (var i = 0, len = $scope.managers.length; i < len; i++) {
          if ($scope.managers[i]._id === $scope.editStore.manager) {
            $scope.managerName = $scope.managers[i].name;
            return;
          }
        }
      }
    });

    $scope.selectManager = function (manager) {
      $scope.editStore.manager = manager._id;
    };

    $scope.clearManager = function () {
      $scope.editStore.manager = null;
    };

    $scope.saveStore = function (form) {
      if (form.$valid) {

        var oldStore = angular.copy($scope.store);

        if (!oldStore) {
          var isNew = true;
        } else {

          var isNameChange = $scope.editStore.storeName !== oldStore.storeName;
          var isPhoneChange = $scope.editStore.phone !== oldStore.phone;
          var isManagerChange = $scope.editStore.manager !== oldStore.manager;
          var isAddressChange = !angular.equals($scope.editStore.address, oldStore.address);
          var isCnpjChange = $scope.editStore.cnpj !== oldStore.cnpj;
          var isDescriptionChange = $scope.editStore.description !== oldStore.description;
        }


        if (isNew || isNameChange || isPhoneChange || isManagerChange || isAddressChange || isCnpjChange || isDescriptionChange) {
          $scope.disabledSave = true;

          var store = {
            storeName: $scope.editStore.storeName,
            description: $scope.editStore.description,
            phone: $scope.editStore.phone,
            cnpj: $scope.editStore.cnpj,
            address: $scope.editStore.address,
            manager: $scope.editStore.manager
          };

          if ($scope.editStore._id) {
            store._id = $scope.editStore._id
          }

          StoresSrv.updateStore(store, function (resp) {
            $scope.disabledSave = false;
            if ($scope.editStore._id) {
              LoggerSrv.success($translate.instant('STORE_HAS_BEEN_CHANGED'));
            } else {
              LoggerSrv.success($translate.instant('ADD_NEW_STORE'));
            }

            if (!store._id) {
              $scope.stores.push(resp);
            } else {
              for (var i = 0; i < $scope.stores.length; i++) {
                if (store._id === $scope.stores[i]._id) {
                  $scope.stores[i] = resp;
                  break;
                }
              }
            }

            $scope.editAddStore();
          });
        }
      } else {
        $scope.tryToSave = true;
      }
    };
  }
})();
