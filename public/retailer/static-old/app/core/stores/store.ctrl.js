(function () {
  'use strict';

  angular.module('app.core.stores').controller('app.core.stores.storeCtrl',
    ['$scope', 'StoresSrv', storeCtrl]);

  function storeCtrl($scope, StoresSrv) {

    StoresSrv.getUniqueToken($scope.store._id, function (data) {
      if(!data.token){
        $scope.uniqueLink = false;
      } else {
        $scope.uniqueLink = '/goToCheckout?token=' + data.token
      }
    })

  }
})();
