(function () {
  'use strict';

  angular.module('app.core.clientsGroup').controller('app.core.clientsGroup.rewardsModalCtrl',
    ['$scope', 'RewardsSrv', 'clientsData', '$uibModalInstance', '$state', rewardsModalCtrl]);

  function rewardsModalCtrl($scope, RewardsSrv, clientsData, $uibModalInstance, $state) {

    $scope.clientsData = clientsData;

    RewardsSrv.getRewardsByRetailerId($scope.currentRetailer._id, function (resp) {
      $scope.rewards = resp
    });

    $scope.chooseReward = function (reward) {
      $scope.rewardId = reward._id;
      $scope.disabledSave = false;
    };

    $scope.closeModal = function () {
      $uibModalInstance.dismiss();
    };

    $scope.send = function () {

      if ($scope.rewardId) {
        $state.go('emailTheme', {
          reward: $scope.rewardId,
          clients: $scope.clientsData.clients
        });
        $uibModalInstance.dismiss();
      } else {
        $scope.disabledSave = true;
      }
    }
  }

})();
