(function () {
  'use strict';

  angular.module('app.core.clientsGroup').controller('app.core.clientsGroup.clientsGroupCtrl',
    ['$scope', 'ClientsSrv', '$uibModal', 'LoggerSrv', '$translate', '$stateParams', clientsGroupCtrl]);

  function clientsGroupCtrl($scope, ClientsSrv, $uibModal, LoggerSrv, $translate, $stateParams) {

    $scope.filter = {
      ageGroup: {}
    };
    $scope.selectedClients = {};

    var sendEmailMap = {
      'ALL': {all: true},
      'PROFITABLE': {profitable: true},
      'BIRTHDAY': {birthday: true},
      'INACTIVE': {inactive: true},
      'PRESELECTION': {preselection: []},
      'GENDER_M': {gender: 'M'},
      'GENDER_F': {gender: 'F'},
      'AGE_GROUP_17': {ageGroup: {ageFrom: '', ageTo: 17}},
      'AGE_GROUP_18_25': {ageGroup: {ageFrom: 18, ageTo: 25}},
      'AGE_GROUP_26_30': {ageGroup: {ageFrom: 26, ageTo: 30}},
      'AGE_GROUP_30_40': {ageGroup: {ageFrom: 30, ageTo: 40}},
      'AGE_GROUP_41': {ageGroup: {ageFrom: 41, ageTo: ''}}
    };

    getFilterClients($scope.filter);

    $scope.disableSendEmail = (function(){
      return $scope.currentRetailer.sendEmailStatistic && $scope.currentRetailer.sendEmailStatistic.quantity > 1
    }());

    $scope.selectAll = function () {

      $scope.selectedAll = $scope.selectedAll ? true : false;

      angular.forEach($scope.selectedClients, function (item, i, arr) {
        arr[i] = $scope.selectedAll;
      });
    };

    $scope.orderClient = {
      predicate: 'name',
      reverse: false
    };

    $scope.sortClients = function (predicate) {
      $scope.orderClient.reverse = ($scope.orderClient.predicate === predicate) ? !$scope.orderClient.reverse : false;
      $scope.orderClient.predicate = predicate;
    };

    $scope.ifAllSelected = function () {

      for (var key in $scope.selectedClients) {
        if (!$scope.selectedClients[key]) {
          $scope.selectedAll = false;
          return
        }
      }
      $scope.selectedAll = true;
    };

    $scope.chooseGender = function (val) {

      if ($scope.filter.gender === sendEmailMap[val].gender) {
        $scope.filter.gender = '';
        getFilterClients($scope.filter);
      } else {
        $scope.filter.gender = sendEmailMap[val].gender;
        getFilterClients($scope.filter);
      }
    };

    $scope.chooseAge = function (val) {

      if ($scope.filter.ageGroup.ageFrom === sendEmailMap[val].ageGroup.ageFrom
        && $scope.filter.ageGroup.ageTo === sendEmailMap[val].ageGroup.ageTo) {

        $scope.filter.ageGroup = {};
        getFilterClients($scope.filter);
      } else {
        $scope.filter.ageGroup = angular.copy(sendEmailMap[val].ageGroup);
        getFilterClients($scope.filter);
      }
    };

    $scope.chooseBirthday = function () {
      $scope.filter.birthday = !$scope.filter.birthday;
      getFilterClients($scope.filter);
    };

    $scope.chooseInactive = function () {
      $scope.filter.inactive = !$scope.filter.inactive;
      getFilterClients($scope.filter);
    };

    $scope.chooseProfitable = function () {
      $scope.filter.profitable = !$scope.filter.profitable;
      getFilterClients($scope.filter);
    };

    $scope.sendingEmail = function () {


        var selectedClientsToMessage = [];

        for (var key in $scope.selectedClients) {
          loop:
            if ($scope.selectedClients[key]) {
              for (var k = 0; k < $scope.clients.length; k++) {
                if ($scope.clients[k]._id === key) {
                  selectedClientsToMessage.push($scope.clients[k]);
                  break loop;
                }
              }
            }
        }

        if (selectedClientsToMessage.length === 0) {
          LoggerSrv.warning($translate.instant('YOU_MUST_CHOOSE_CLIENT_FIRST'));
          return;
        }

          $uibModal.open({
            templateUrl: 'app/core/clients_group/rewards_modal.tpl.html',
            controller: 'app.core.clientsGroup.rewardsModalCtrl',
            windowClass: 'reward-modal',
            animation: true,
            resolve: {
              clientsData: {
                clients: selectedClientsToMessage
              }
            }
          });

    };

    function getFilterClients(filter) {
      ClientsSrv.getFilterClients(filter, function (resp) {

        $scope.clients = resp;
        if ($scope.clients) {
          $scope.selectedAll = false;
          $scope.selectedClients = {};

          for (var k = 0; k < $scope.clients.length; k++) {
            $scope.selectedClients[$scope.clients[k]._id] = false
          }
        }

        if ($stateParams.emit) {
          $scope.$emit('scroll-to-element', $stateParams.emit);
        }
      });
    }
  }

})();
