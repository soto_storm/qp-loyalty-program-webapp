(function () {
  'use strict';

  angular.module('app.core.clientsGroup').controller('app.core.clientsGroup.clientModalCtrl',
    ['$scope', '$uibModalInstance', 'client', 'disableSendEmail', 'RewardsSrv', '$state', clientModalCtrl]);

  function clientModalCtrl($scope, $uibModalInstance, client, disableSendEmail, RewardsSrv, $state) {

    $scope.client = client;
    $scope.disableSendEmail = disableSendEmail;

    $scope.closeModal = function () {
      $uibModalInstance.dismiss();
    };

    RewardsSrv.getRewardsByRetailerId($scope.currentRetailer._id, function (resp) {
      $scope.rewards = resp
    });

    $scope.chooseReward = function (reward) {
      $scope.rewardId = reward._id;
      $scope.disabledSave = false;
    };

    $scope.send = function () {
      if ($scope.rewardId) {
        client = [$scope.client];
        $state.go('emailTheme', {clients: client, reward: $scope.rewardId});
        $uibModalInstance.dismiss();

      } else {
        $scope.disabledSave = true;
      }
    }
  }

})();
