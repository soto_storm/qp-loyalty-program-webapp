(function () {
  'use strict';

  angular.module('app.core.rechargeDots').controller('app.core.rechargeDots.rechargeDotsCtrl',
    ['$scope', 'RetailersSrv', 'LoggerSrv', '$translate', rechargeDotsCtrl]);

  function rechargeDotsCtrl($scope, RetailersSrv, LoggerSrv, $translate) {

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.send = function (form) {

      if (form.$valid) {
        $scope.disableToSave = true;

        var message = {
          message: $scope.message,
          rechargePoints: $scope.points,
          type: 'RECHARGE_DOTS'
        };

        RetailersSrv.supportEmail(message, function () {

          $scope.message = '';
          $scope.points = '';
          form.message.$setUntouched();
          form.points.$setUntouched();

          $scope.disableToSave = false;
          $scope.tryToSave = false;
          LoggerSrv.success($translate.instant('DOTS_SUCCESSFULLY_RECHARGED'));
        });
      } else {
        $scope.tryToSave = true;
      }
    };

  }

})();
