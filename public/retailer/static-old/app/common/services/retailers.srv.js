(function () {
  'use strict';

  angular.module('app.common.services').factory('RetailersSrv',
    ['$http', 'LoggerSrv', '$translate', '$rootScope', RetailersSrv]);

  function RetailersSrv($http, LoggerSrv, $translate, $rootScope) {

    var initialized = false,
      initListeners;

    function getCurrentRetailer(callback) {
      $http({
        url: '/auth/current',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function logoutRetailer(callback) {
      $http({
        url: '/auth/logout',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function addInitListener(callback) {
      if (!callback) {
        throw "callback is required!"
      }

      if (initialized) {
        callback();
      } else {
        initListeners = callback;
      }

    }

    function saveRetailer(retailer, callback) {
      if (retailer._id) {

        $http({
          url: '/api/retailer/' + retailer._id,
          method: 'PUT',
          data: retailer
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function () {

          LoggerSrv.error($translate.instant('SERVER_ERROR'));

        });

      } else {

        $http({
          url: '/api/retailer',
          method: 'POST',
          data: retailer
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function () {

          LoggerSrv.error($translate.instant('SERVER_ERROR'));

        });

      }
    }

    function changePassword(retailerId, retailerPasswords, callback) {
      if (!retailerId) {
        throw 'retailerId is required (RetailersSrv.changePassword)'
      }
      if (!retailerPasswords.password) {
        throw 'password is required (RetailersSrv.changePassword)'
      }
      if (!retailerPasswords.newPassword) {
        throw 'newPassword is required (RetailersSrv.changePassword)'
      }

      retailerPasswords.password = md5(retailerPasswords.password);
      retailerPasswords.newPassword = md5(retailerPasswords.newPassword);

      $http({
        url: '/api/retailer/changePassword/' + retailerId,
        method: 'POST',
        data: retailerPasswords
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (resp) {

        if (resp.data && resp.data.error && callback) {
          callback(resp.data);
        } else {
          LoggerSrv.error($translate.instant('SERVER_ERROR'));
        }

      });
    }

    function retailerLastActivity(callback) {
      $http({
        url: '/api/retailer/lastActivity/' + $rootScope.currentRetailer._id,
        method: 'get'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function exchangedRewardsToVouchers(callback) {
      $http({
        url: '/api/retailer/exchangedRewardsToVouchers?retailerId=' + $rootScope.currentRetailer._id,
        method: 'get'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function supportEmail(resp, callback) {

      $http({
        url: '/api/retailer/support/' + $rootScope.currentRetailer._id,
        method: 'POST',
        data: JSON.stringify(resp)
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function (resp) {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

        if (callback) {
          callback(resp.data);
        }
      });
    }

    return {
      _addInitListener: addInitListener,
      getCurrentRetailer: getCurrentRetailer,
      logoutRetailer: logoutRetailer,
      saveRetailer: saveRetailer,
      changePassword: changePassword,
      retailerLastActivity: retailerLastActivity,
      exchangedRewardsToVouchers: exchangedRewardsToVouchers,
      supportEmail: supportEmail
    }

  }

}());
