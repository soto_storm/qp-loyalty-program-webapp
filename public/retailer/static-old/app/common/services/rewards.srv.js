(function () {
  'use strict';

  angular.module('app.common.services').factory('RewardsSrv',
    ['$http', 'LoggerSrv', '$translate', '$rootScope', RewardsSrv]);

  function RewardsSrv($http, LoggerSrv, $translate, $rootScope) {

    function getRewardsByRetailerId(retailerId, callback) {
      if (!retailerId) {
        throw 'retailerId is required (RewardsSrv.getRewardsByRetailerId)'
      }

      $http({
        url: '/api/reward?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function updateReward(reward, callback) {
      if (!reward) {
        throw 'reward is required (StoresSrv.updateReward)'
      }

      if (reward._id) {

        $http({
          url: '/api/reward/' + reward._id,
          method: 'PUT',
          data: reward
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function () {

          LoggerSrv.error($translate.instant('SERVER_ERROR'));

        });

      } else {

        reward.retailer = $rootScope.currentRetailer._id;

        $http({
          url: '/api/reward',
          method: 'POST',
          data: reward
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function () {

          LoggerSrv.error($translate.instant('SERVER_ERROR'));

        });
      }
    }

    return {
      updateReward: updateReward,
      getRewardsByRetailerId: getRewardsByRetailerId
    }
  }

}());
