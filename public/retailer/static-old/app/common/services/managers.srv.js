(function () {
  'use strict';

  angular.module('app.common.services').factory('ManagersSrv',
    ['$http', 'LoggerSrv', '$translate', ManagersSrv]);

  function ManagersSrv($http, LoggerSrv, $translate) {

    var initialized = false,
      initListeners;

    function getAllManagers(callback) {
      $http({
        url: '/api/manager',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function getManagerByRetailerId(managerId, callback) {

      $http({
        url: '/api/manager?managerId=' + managerId,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    return {
      getAllManagers: getAllManagers,
      getManagerByRetailerId: getManagerByRetailerId
    }

  }

}());
