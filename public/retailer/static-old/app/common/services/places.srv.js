(function () {
  'use strict';

  angular.module('app.common.services').factory('PlacesSrv',
    ['$http', 'LoggerSrv', '$translate', PlacesSrv]);

  function PlacesSrv($http, LoggerSrv, $translate) {

    function getPlacesByRetailer(retailerId, callback) {
      if (!retailerId) {
        throw 'RetailerId is required (PlacesSrv.getPlacesByRetailer)'
      }

      $http({
        url: '/api/place?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    return {
      getPlacesByRetailer: getPlacesByRetailer
    }

  }
}());
