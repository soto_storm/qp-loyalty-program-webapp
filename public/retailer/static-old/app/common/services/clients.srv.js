(function () {
  'use strict';

  angular.module('app.common.services').factory('ClientsSrv',
    ['$rootScope', '$http', 'LoggerSrv', '$translate', ClientsSrv]);

  function ClientsSrv($rootScope, $http, LoggerSrv, $translate) {

    function getFilterClients(options, callback) {
      var urlRequest = '/api/client?isFilter=true&retailer=' + $rootScope.currentRetailer._id;

      if (options.onlyLength) {
        urlRequest = '/api/client?retailer=' + $rootScope.currentRetailer._id + '&onlyLength=true';
      }

      if (options.profitable && typeof options.profitable === 'number') {
        urlRequest += '&profitable=' + options.profitable;
      } else if (options.profitable && !(typeof options.profitable === 'number')) {
        urlRequest += '&profitable=true';
      }

      if (options.birthday && typeof options.birthday === 'number') {
        urlRequest += '&birthday=' + options.birthday;
      } else if (options.birthday && !(typeof options.birthday === 'number')) {
        urlRequest += '&birthday=true';
      }

      if (options.ageGroup) {
        if (Number.isInteger(+options.ageGroup.ageFrom) && +options.ageGroup.ageFrom > 0) {
          urlRequest += '&ageFrom=' + +options.ageGroup.ageFrom
        }
        if (Number.isInteger(+options.ageGroup.ageTo) && +options.ageGroup.ageTo > 0) {
          urlRequest += '&ageTo=' + +options.ageGroup.ageTo
        }
      }

      if (options.inactive) {
        urlRequest += '&inactive=true'
      }

      if (options.gender) {
        urlRequest += '&gender=' + options.gender
      }

      if (options.genderGraphData) {
        urlRequest = '/api/client?retailer=' + $rootScope.currentRetailer._id + '&genderGraphData=true'
      }

      if (options.ageGroupGraphData) {
        urlRequest = '/api/client?retailer=' + $rootScope.currentRetailer._id + '&ageGroupGraphData=true'
      }

      $http({
        url: urlRequest,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }

      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    function sendEmailToClient(emailOption, callback) {
      if (!emailOption.reward) {
        throw 'ClientsSrv.sendEmailToClient reward is required'
      }

      if (!emailOption.template) {
        throw 'ClientsSrv.sendEmailToClient template is required'
      }

      if (!emailOption.clients) {
        throw 'ClientsSrv.sendEmailToClient clients is required'
      }
      
      emailOption.retailer = $rootScope.currentRetailer._id;

      $http({
        url: '/api/mail/sendToClientsGroup',
        method: 'POST',
        data: JSON.stringify(emailOption)
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function () {

        LoggerSrv.error($translate.instant('SERVER_ERROR'));

      });
    }

    return {
      getFilterClients: getFilterClients,
      sendEmailToClient: sendEmailToClient
    }

  }

}());
