(function () {
  'use strict';


  angular.module('app.common.filters').filter('cpf', cpf);

  function cpf() {
    return function (input) {
      return ('' + input || '').replace(/\D/g, '')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d{1,2})$/, '$1-$2');
    }
  }

})();
