(function () {
  'use strict';

  angular.module('app.common.filters').filter('points', points);

  function points() {
    return function(input) {
      input = '' + input;
      return input.indexOf('.') > -1 ? parseFloat(input).toFixed(2) : input;
    };
  }

})();
