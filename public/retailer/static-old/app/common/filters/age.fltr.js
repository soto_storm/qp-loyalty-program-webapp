(function () {
  'use strict';

  angular.module('app.common.filters').filter('age', age);

  function age() {
    function calculateAge(birthdate) { // birthdate is a date
      var date = new Date(birthdate);
      var ageDifMs = Date.now() - date.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    return function (birthdate) {
      return calculateAge(birthdate);
    };

  }

})();
