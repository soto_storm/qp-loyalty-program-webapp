"use strict";
(function () {
  angular.module('app.common.directives').directive("clearTypeahead",
    ['$timeout', clearTypeahead]);

  function clearTypeahead($timeout) {
    return {
      restrict: 'A',
      link: function (scope, element) {

        var clearTypeaheadIcon = element.find('[clear-typeahead-icon]');

        $(clearTypeaheadIcon).click(function () {
          var input = element.find('[uib-typeahead]');

          input.val('');

          $timeout(function () {
            input.focus();
          }, 0)

        });
      }
    };
  }

}());
