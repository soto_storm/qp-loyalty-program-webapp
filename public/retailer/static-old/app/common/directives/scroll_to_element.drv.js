"use strict";
(function () {
  angular.module('app.common.directives').directive('scrollTo',
    ['$timeout', function ($timeout) {
      return {

        link: function (scope, element) {

          scope.$on('scroll-to-element', function (event, data) {

              $timeout(function () {
                var newElement = element.find('[st-element=\'' + data + '\']');
                var scrollTo = newElement.offset();
                window.scrollBy(scrollTo.left, +scrollTo.top - 100);

                newElement.addClass('new-element');
                $timeout(function () {
                  newElement.removeClass('new-element')
                }, 3000);

              }, 0);
            }
          );
        }
      }
    }]);

}());
