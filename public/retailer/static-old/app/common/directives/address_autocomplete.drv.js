'use strict';

(function () {

  angular.module("app.common.directives").directive("addressAutocomplete",
    ['$timeout', addressAutocomplete]);

  function addressAutocomplete($timeout) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        model: '=ngModel',
        isTryToSave: '=isTryToSave'
      },
      templateUrl: 'app/common/templates/address_autocomplete.tpl.html',
      link: function ($scope, element, attrs, ngModelCtrl) {

        var defaultZoom = 15,
          init = false,
          defaultCenter = new google.maps.LatLng(41.9955835, -93.63813119999998),
          mapOptions = {
            center: defaultCenter,
            zoom: defaultZoom,
            zoomControl: true,
            scaleControl: true,
            scrollwheel: true,
            zoomControlOptions: {
              position: google.maps.ControlPosition.LEFT_TOP
            }
          },
          mapDiv = $(element).find('[map]')[0],
          input = $(element).find('[map-input]'),
          inputCtrl = input.controller('ngModel'),
          autocomplete = new google.maps.places.Autocomplete(input[0]),
          map,
          marker;

        map = new google.maps.Map(mapDiv, mapOptions);

        var hasLocation = ngModelCtrl.$viewValue.locationLat && ngModelCtrl.$viewValue.locationLng;
        if (hasLocation) {
          mapOptions.center = new google.maps.LatLng(ngModelCtrl.$viewValue.locationLat, ngModelCtrl.$viewValue.locationLng);
        }

        $scope.isShowingErrors = function (form, fieldName) {
          return form[fieldName] && ($scope.isTryToSave || form[fieldName].$dirty) && !form[fieldName].$valid;
        };

        map = new google.maps.Map(mapDiv, mapOptions);
        marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
        autocomplete.setTypes(['address']);

        $scope.$watch(function () {
          return ngModelCtrl.$viewValue && ngModelCtrl.$viewValue.address;
        }, function () {
          resetMap(map, marker, inputCtrl);
          if (!init && ngModelCtrl.$viewValue && ngModelCtrl.$viewValue.locationLat && ngModelCtrl.$viewValue.locationLng) {
            init = true;
            var position = new google.maps.LatLng(ngModelCtrl.$viewValue.locationLat, ngModelCtrl.$viewValue.locationLng);

            marker.setPosition(position);
            marker.setVisible(true);

            map.setCenter(position);
          }
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
          inputCtrl.$setValidity('correct', !!(place && place.geometry));

          if (!place || !place.geometry) {
            $scope.$apply();
            return;
          }

          ngModelCtrl.$viewValue.address = input.val();
          ngModelCtrl.$viewValue.locationLat = place.geometry.location.lat();
          ngModelCtrl.$viewValue.locationLng = place.geometry.location.lng();

          for (var i = 0, len = place.address_components.length; i < len; i++) {
            if (place.address_components[i].types.indexOf('country') !== -1) {
              ngModelCtrl.$viewValue.country = place.address_components[i].long_name;
              ngModelCtrl.$viewValue.countryCode = place.address_components[i].short_name;
            }
            if (place.address_components[i].types.indexOf('locality') !== -1) {
              ngModelCtrl.$viewValue.longCity = place.address_components[i].long_name;
              ngModelCtrl.$viewValue.shortCity = place.address_components[i].short_name;
            }
          }

          $scope.$apply();
        });

        function resetMap(map, marker, inputCtrl) {

          var hasMark = ngModelCtrl.$viewValue && ngModelCtrl.$viewValue.locationLat && ngModelCtrl.$viewValue.locationLng;

          inputCtrl.$setValidity('correct', !!hasMark);

          if (hasMark) {
            var position = new google.maps.LatLng(ngModelCtrl.$viewValue.locationLat, ngModelCtrl.$viewValue.locationLng);

            marker.setPosition(position);
            marker.setVisible(true);

            map.setCenter(position);

          } else {
            marker.setVisible(false);
          }

        }
      }
    }
  }

})();
