(function () {
  'use strict';

  angular.module('app.core.rewards').controller('app.core.rewards.editAddRewardsCtrl',
    ['$scope', 'RewardsSrv', 'LoggerSrv', '$translate', editAddRewardsCtrl]);

  function editAddRewardsCtrl($scope, RewardsSrv, LoggerSrv, $translate) {

    $scope.editReward = $scope.reward ? angular.copy($scope.reward) : {};

    $scope.isShowErrors = function (form, formField) {
      return form && form[formField] && form[formField].$invalid && (form[formField].$touched || $scope.tryToSave);
    };

    $scope.saveReward = function (form) {
      if (form.$valid) {

        var oldReward = angular.copy($scope.reward);

        if (!oldReward) {
          var isNew = true;
        } else {

          var isTitleChange = $scope.editReward.title !== oldReward.title;
          var isPointsChange = $scope.editReward.points !== oldReward.points;
        }

        if (isNew || isTitleChange || isPointsChange) {
          $scope.disabledSave = true;

          var reward = {
            title: $scope.editReward.title,
            points: $scope.editReward.points
          };

          if ($scope.editReward._id) {
            reward._id = $scope.editReward._id
          }

          RewardsSrv.updateReward(reward, function (resp) {
            $scope.disabledSave = false;
            if ($scope.editReward._id) {
              LoggerSrv.success($translate.instant('REWARD_HAS_BEEN_CHANGED'));
            } else {
              LoggerSrv.success($translate.instant('ADD_NEW_REWARD'));
            }

            if (!reward._id) {
              $scope.rewards.push(resp);
            } else {

              for (var i = 0; i < $scope.rewards.length; i++) {
                if (reward._id === $scope.rewards[i]._id) {
                  $scope.rewards[i] = resp;
                  break;
                }
              }

            }

            $scope.editAddReward();
          });
        }
      } else {
        $scope.tryToSave = true;
      }
    };
  }
})();
