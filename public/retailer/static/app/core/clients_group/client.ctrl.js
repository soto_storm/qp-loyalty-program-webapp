(function () {
  'use strict';

  angular.module('app.core.clientsGroup').controller('app.core.clientsGroup.clientCtrl',
    ['$scope', '$uibModal', clientCtrl]);

  function clientCtrl($scope, $uibModal) {

    $scope.openClient = function () {

      $uibModal.open({
        templateUrl: 'app/core/clients_group/client_modal.tpl.html',
        controller: 'app.core.clientsGroup.clientModalCtrl',
        windowClass: 'client-modal',
        animation: true,
        resolve: {
          client: $scope.client,
          disableSendEmail: $scope.disableSendEmail
        }
      });
    };
  }

})();
