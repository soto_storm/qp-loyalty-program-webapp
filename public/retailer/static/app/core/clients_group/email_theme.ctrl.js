(function () {
  'use strict';

  angular.module('app.core.clientsGroup').controller('app.core.clientsGroup.emailThemeCtrl',
    ['$scope', '$stateParams', 'ClientsSrv', 'LoggerSrv', '$translate', '$state', emailThemeCtrl]);

  function emailThemeCtrl($scope, $stateParams, ClientsSrv, LoggerSrv, $translate, $state) {

    if (!$stateParams.reward || !$stateParams.clients) {
      $state.go('clientsGroup');
      LoggerSrv.warning($translate.instant('YOU_MUST_CHOOSE_REWARD_OR_CLIENT_FIRST'));
      return;
    }

    $scope.clients = $stateParams.clients;

    $scope.chooseEmailTheme = function (theme) {
      $scope.emailTheme = theme;
    };

    $scope.send = function () {

      var clients = [];

      for (var i = 0, len = $scope.clients.length; i < len; i++) {
        clients.push({_id: $scope.clients[i]._id})
      }

      $scope.disabledSave = true;

      var emailOption = {
        template: $scope.emailTheme,
        clients: clients,
        reward: $stateParams.reward
      };

      ClientsSrv.sendEmailToClient(emailOption, function () {
        $scope._loadCurrentRetailer(function () {
          $scope.disabledSave = false;
          LoggerSrv.success($translate.instant('EMAIL_HAS_BEEN_SENT'));
          $state.go('clientsGroup');
        });
      });
    };

  }

})();
