"use strict";
(function () {
  angular.module('app.common.directives').directive("lazyScroll",
    ['PlacesSrv', lazyScroll]);

  function lazyScroll(PlacesSrv) {
    return {
      restrict: "A",
      link: function (scope, element) {
        var allActivities;

        scope.activities = [];

        var visibleHeight = $(element).height();
        var threshold = 100;

        PlacesSrv.getPlacesByRetailer(scope.currentRetailer._id, function (data) {
          allActivities = data.sort(function (a, b) {
            if (a.createdAt < b.createdAt) {
              return 1;
            }
            if (a.createdAt > b.createdAt) {
              return -1;
            }

            return 0;
          });
          loadMore();
        });

        $(element).scroll(function () {
          var scrollableHeight = $(element).prop('scrollHeight');
          var hiddenContentHeight = scrollableHeight - visibleHeight;

          var isNeedLoad = scope.activities.length < allActivities.length;

          if ((hiddenContentHeight - $(element).scrollTop() <= threshold) && isNeedLoad) {
            loadMore();
            scope.$apply()
          }
        });

        function loadMore() {
          var newActivities = [];
          var currentLength = scope.activities.length;
          for (var i = currentLength; i < currentLength + 20; i++) {
            if (i >= allActivities.length) {
              break;
            }

            newActivities.push(allActivities[i])
          }

          scope.activities = scope.activities.concat(newActivities)
        }

      }
    };
  }

}());
