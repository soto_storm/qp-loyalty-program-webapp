(function () {
  'use strict';

  angular.module('app')
    .config(['toastr', toastrConfig])
    .config(['$translateProvider', translate])
    .config(['$stateProvider', '$urlRouterProvider', config]);

  function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/reports');

    $stateProvider
      .state('clientsGroup', waitForServices({
        url: '/clientsGroup',
        templateUrl: 'app/core/clients_group/clients_group.tpl.html',
        controller: 'app.core.clientsGroup.clientsGroupCtrl',
        params: {
          emit: null
        }
      }))
      .state('support', waitForServices({
        url: '/support',
        templateUrl: 'app/core/support/support.tpl.html',
        controller: 'app.core.support.supportCtrl'
      }))
      .state('rewards', waitForServices({
        url: '/rewards',
        templateUrl: 'app/core/rewards/rewards.tpl.html',
        controller: 'app.core.rewards.rewardsCtrl'
      }))
      .state('profile', waitForServices({
        url: '/profile',
        templateUrl: 'app/core/profile/profile.tpl.html',
        controller: 'app.core.profile.profileCtrl'
      }))
      .state('rechargeDots', waitForServices({
        url: '/rechargeDots',
        templateUrl: 'app/core/recharge_dots/recharge_dots.tpl.html',
        controller: 'app.core.rechargeDots.rechargeDotsCtrl'
      }))
      .state('reports', waitForServices({
        url: '/reports',
        templateUrl: 'app/core/reports/reports.tpl.html',
        controller: 'app.core.reports.reportsCtrl'
      }))
      .state('stores', waitForServices({
        url: '/stores',
        templateUrl: 'app/core/stores/stores.tpl.html',
        controller: 'app.core.stores.storesCtrl'
      }))
      .state('emailTheme', waitForServices({
        url: '/emailTheme',
        templateUrl: 'app/core/clients_group/email_theme.tpl.html',
        controller: 'app.core.clientsGroup.emailThemeCtrl',
        params: {
          groupClients: null,
          oneClient: null,
          clients: null,
          reward: null
        }
      }));

    function waitForServices(conf) {
      if (!conf.resolve) {
        conf.resolve = {};
      }

      conf.resolve.waitForServicesInitialization = ['$q', 'RetailersSrv', function ($q, RetailersSrv) {

        var def = $q.defer();
        RetailersSrv._addInitListener(function () {
          def.resolve();
        });

        return def.promise;
      }];

      return conf;
    }
  }

  function toastrConfig(toastr) {
    toastr.options = {
      progressBar: true,
      timeOut: 10000,
      positionClass: 'toast-top-right'
    };
  }

  function translate($translateProvider) {

    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('escape');

    $translateProvider.useStaticFilesLoader({
      prefix: 'app/i18n/locale-',
      suffix: '.json'
    });

  }

})();
