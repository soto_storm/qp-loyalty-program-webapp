/**
 * Created by Jesus Soto on 10/27/2016.
 */
(function(){
  angular.module('retailerApp').controller('supportCtrl',
    function($rootScope, $scope, $mdDialog, ManagersSrv, RetailersSrv){
      $scope.message='';
      $scope.topic='';
      $scope.subject='';
      $scope.options=[
        'Erro de servidor',
        'Se os pontos não recarregar',
        'Validação de e-mail'
      ];
      $scope.emptyTopic=false;

      ManagersSrv.getManagerByRetailerId($rootScope.currentRetailer.manager, function (resp) {
        $scope.manager = resp;
      });

      $scope.showAlert = function(message) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog
        $mdDialog.show(
          $mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Suporte Contato')
            .textContent(message)
            .ariaLabel('')
            .ok('Entendi!')
        );
      };

      $scope.send = function () {

          var message = {
            message: $scope.message,
            topic: $scope.topic,
            subject: $scope.subject,
            type: 'SUPPORT_MESSAGE'
          };

        if($scope.topic===''){
          $scope.emptyTopic=true;
        }else{
          $scope.emptyTopic=false;
          RetailersSrv.supportEmail(message, function (resp) {
            if(resp.status===-1){
              $scope.showAlert('Falha de conexão Internet. :(');
            }else{
              $scope.showAlert('E-mail enviado com sucesso. :D');
            }
          });
        }
      };
    });
}());
