/**
 * Created by Kevin on 27-Oct-16.
 */
(function(){
  angular.module('retailerApp').controller('minhaContaCtrl',
    function($rootScope, $scope, RetailersSrv, ManagersSrv, $mdToast){
      $scope.editRetailer = angular.copy($scope.currentRetailer);
      ManagersSrv.getManagerByRetailerId($rootScope.currentRetailer.manager,function (resp) {
        $scope.respc = angular.copy(resp);
        $scope.editRetailer.manager = resp.name;
      });

      //console.log($scope.managers);
      //console.log($scope.managerName);

      $scope.saveRetailer = function (form) {
        if (form.$valid) {
          var oldRetailer = $scope.currentRetailer;

          var isNameChange = $scope.editRetailer.name !== oldRetailer.name;
          var isEmailChange = $scope.editRetailer.email !== oldRetailer.email;
          var isManagerChange = $scope.editRetailer.manager !== oldRetailer.manager;

          if (isNameChange || isEmailChange || isManagerChange) {

            $scope.disabledSaveRetailer = true;

            var retailer = {
              _id: $scope.editRetailer._id,
              name: $scope.editRetailer.name,
              email: $scope.editRetailer.email,
              manager: $scope.editRetailer.manager._id
            };

            RetailersSrv.saveRetailer(retailer, function (resp) {
              $scope._loadCurrentRetailer();
              var toast = $mdToast.simple()
                .content('Your account has been updated successfully!')
                .action('OK')
                .highlightAction(false)
                .position('left top right');
              $mdToast.show(toast);
              $scope.disabledSaveRetailer = false;
              $scope.tryToSaveRetailer = false;
            });
          }
        } else {
          $scope.tryToSaveRetailer = true;
        }
      };

      $scope.setNewPassword = function (form) {

        if($scope.passwords.newPasswordVerify && $scope.passwords.newPassword && $scope.passwords.password) {
          if($scope.passwords.newPassword === $scope.passwords.newPasswordVerify) { // check if passwords are the same
            if($scope.passwords.newPassword.length >= 8) { // check if the password is 8 chars long
              $scope.validPasswords = true;
            } else {
              console.log('password length error');
              $scope.validPasswords = false;
              $scope.error2 = true;
              $scope.errorMsg = 'Your new password needs to be at least 8 characters long...';
              $scope.passwords.newPassword = '';
              $scope.passwords.newPasswordVerify = '';
            }
          } else {
            console.log('password are not the same error');
            $scope.validPasswords = false;
            $scope.error2 = true;
            $scope.errorMsg = 'Your new password should match on both fields...';
            $scope.passwords.newPassword = '';
            $scope.passwords.newPasswordVerify = '';
          }
        } else {
          console.log('empty inputs error');
          $scope.validPasswords = false;
          $scope.error2 = true;
          $scope.errorMsg = 'All the fields are required...';
          $scope.passwords.newPassword = '';
          $scope.passwords.newPasswordVerify = '';
        }

        if ($scope.validPasswords) {
          $scope.disabledSave = true;

          var retailerPasswords = {
            password: $scope.passwords.password,
            newPassword: $scope.passwords.newPassword
          };

          RetailersSrv.changePassword($scope.editRetailer._id, retailerPasswords, function (resp) {

            if (resp.error) {
              isWrongPass = true;
              $scope.error2 = true;
              $scope.errorMsg = "Your old password is wrong!";
              form.password.$setValidity("wrongPassword", false);
            } else {
              var toast = $mdToast.simple()
                .content('Your password has been changed successfully!')
                .action('OK')
                .highlightAction(false)
                .position('left top right');
              $mdToast.show(toast);
              $scope.showrp = false;
              $scope.passwords = {};
              form.$setUntouched();
              $scope.tryToSavePass = false;
              $scope.error2 = false;
              $scope.errorMsg = '';
            }
          });
        } else {
          $scope.tryToSavePass = true;
        }
      };

      $scope.showResetPassword = function() {
        if(!$scope.showrp) {
          $scope.showrp = true;
        } else {
          $scope.showrp = false;
        }
      }

    });
}());
