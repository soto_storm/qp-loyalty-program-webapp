(function () {
  'use strict';

  angular.module('retailerApp').controller('storeCtrl',
    ['$scope', 'StoresSrv', storeCtrl]);

  function storeCtrl($scope, StoresSrv) {

    StoresSrv.getUniqueToken($scope.store._id, function (data) {
      if(!data.token){
        $scope.uniqueLink = false;
      } else {
        $scope.uniqueLink = '/goToCheckout?token=' + data.token
      }
    });

    $scope.goToCheckout = function(){
      window.location.href =$scope.uniqueLink;
    };
  }
})();
