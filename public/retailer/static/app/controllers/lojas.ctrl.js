/**
 * Created by Kevin on 28-Oct-16.
 */
(function(){
  angular.module('retailerApp').controller('lojasCtrl',
    function($rootScope, $scope, $mdToast, StoresSrv, ManagersSrv){
      $scope.editStoreId = '';
      $scope.editableStore={};
      $scope.position=$rootScope.currentPosition;
      $scope.managers=[];
      $scope.cnpjError=false;
      $scope.editableStore.address='';
      $scope.message='';
      $scope.final={
        address:''
      };
      StoresSrv.getStoreByRetailer($scope.currentRetailer._id, function (data) {
        $scope.stores = data;
      });

      ManagersSrv.getAllManagers(function(resp){
        for(var manager in resp){
          $scope.managers.push(resp[manager]);
        }
        $scope.chosenManager=$scope.managers[0];
      });

      $scope.editAddStore = function (store) {
        $scope.editStoreId = store;
      };

      $scope.loadboxes=function(store){
        $scope.editableStore=angular.copy(store);
        for(var storeManager in $scope.managers){
          if(store.manager===$scope.managers[storeManager]._id){
            $scope.chosenManager=$scope.managers[storeManager];
          }
        }
        $scope.position= '['+store.address.locationLat+','
        +store.address.locationLng+']';
        $scope.final.address=store.address;
      };

      $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
      };

      $scope.clean=function(){
        $scope.editableStore={};
      };

      $scope.showToast = function(message) {
        var toast = $mdToast.simple()
          .content(message)
          .action('OK')
          .highlightAction(false)
          .position('left top right');
        $mdToast.show(toast);
      };

      $scope.save=function(form){
        if(form.cnpj){
          $scope.cnpjError=true;
          $scope.message='Invalid CNPJ';
        }else{
          $scope.cnpjError=false;
        }

        if($scope.editableStore.address.address===''){
          $scope.final.address={};
        }else{
          $scope.editableStore.address=$scope.final.address;
          $scope.editableStore.manager=$scope.chosenManager._id;
        }

        if($scope.editableStore.storeName && $scope.editableStore.phone
          && $scope.editableStore.description && $scope.editableStore.cnpj
          && $scope.editableStore.address.country){

             StoresSrv.updateStore($scope.editableStore, function (resp) {
               if(resp.status===-1){
                 $scope.showToast('No internet connection D:');
                 return;
               }
               if ($scope.editableStore._id) {
                 for(var store in $scope.stores){
                   if($scope.stores[store]._id===$scope.editableStore._id){
                     $scope.stores[store]=$scope.editableStore;
                   }
                 }
                 $scope.showToast('Good!, you edited a store :)');
                 return;
               } else {
                 if(resp.data){
                   $scope.cnpjError=true;
                   $scope.message=resp.data.message;
                   return;
                 }
                 $scope.stores.push(resp);
                 $scope.showToast('Good!, you have registered a new shop :)');
                 $scope.editableStore={};
                 return;
               }
             });
        }
      };

      $rootScope.$on("addressChosen", function(){
        $scope.temp=$rootScope.googleAddress.split(';');
        $scope.position= '['+$scope.temp[1]+','
          +$scope.temp[2]+']';
        $scope.country=$scope.temp[3].split(',');
        $scope.final.address={
          address:$scope.temp[3],
          country:$scope.country[$scope.country.length-1],
          locationLat: $scope.temp[1],
          locationLng: $scope.temp[2]
        };
      });
    });
}());
