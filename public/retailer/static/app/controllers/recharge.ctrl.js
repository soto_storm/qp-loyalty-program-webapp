(function(){
  angular.module('retailerApp').controller('rechargeCtrl',
    function($rootScope, $scope,RetailersSrv){
      $scope.sentForm = false;

      $scope.send = function (form) {

        if($scope.pointsAlt && $scope.pointsAlt !== 0) {
          $scope.points = $scope.pointsAlt;
          console.log('Points were set manually.' + $scope.message);
        }

        if (form.$valid && $scope.points && $scope.message) {
          console.log($scope.message);
          $scope.disableToSave = true;

          // Verifies if the points were set manually and takes that value instead of the select dropdown.


          var message = {
            message: $scope.message,
            rechargePoints: $scope.points,
            type: 'RECHARGE_DOTS'
          };

          RetailersSrv.supportEmail(message, function (data) {
            console.log(data);
            $scope.pointsF = $scope.addSeparator($scope.currentRetailer.points);
            console.log($scope.pointsF);
            $scope.message = '';
            $scope.points = '';
            $scope.sentForm = true;
            //form.message.$setUntouched();
            //form.points.$setUntouched();

            $scope.disableToSave = false;
            $scope.tryToSave = false;
            //LoggerSrv.success($translate.instant('DOTS_SUCCESSFULLY_RECHARGED'));
          });
        } else {
          $scope.tryToSave = true;
          $scope.error = true;
          $scope.errorMsg = 'Plese select an amount of points and make sure you write a message.';
        }
      };

      $scope.reload = function() {
        $scope.sentForm = false;
        $scope.error = false;
      };

    });
}());
