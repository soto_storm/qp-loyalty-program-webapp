/**
 * Created by Jesus Soto on 10/27/2016.
 */
(function(){
  angular.module('retailerApp').controller('clientCtrl',
    function($rootScope, $scope, $state, $mdDialog, set_clientSrv, RewardsSrv, rewardIdentService){
      $scope.client=set_clientSrv.getClient();
      $scope.chosenReward='';
      $scope.emptyRewards=false;
      $scope.error=false;

      RewardsSrv.getRewardsByRetailerId($rootScope.currentRetailer._id,function(resp){
        if(resp.length===0){
          $scope.emptyRewards=true;
          return;
        }
        $scope.rewards=resp;
      });

      $scope.chooseReward=function(reward){
        $scope.chosenReward=reward;
      };

      $scope.next=function(){
        if($scope.chosenReward===''){
          $scope.error=true;
        }else{
          $scope.error=false;
          rewardIdentService.setReward($scope.chosenReward);
          $mdDialog.hide();
          $state.go('emailTheme', {
            reward: $scope.chosenReward,
            clients: $scope.client
          });
        }
      };
    });
}());
