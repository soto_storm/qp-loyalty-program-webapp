/**
 * Created by Jesus Soto on 10/28/2016.
 */
(function(){
  angular.module('retailerApp').controller('theme_emailCtrl',
    function($rootScope, $scope, $state, $mdToast, set_clientSrv, ClientsSrv, RewardsSrv, rewardIdentService){
      $scope.clients=set_clientSrv.getClient();
      $scope.chosen = {};
      $scope.chosenTemplate={
        christmas:'',
        birthday:'',
        reward:''
      };
      var template='';
      if($scope.clients.length){
        $scope.chosenClient=$scope.clients[0];
        $scope.chosen=$scope.clients[0];
      }else{
        $scope.clients=[];
        $scope.clients.push(set_clientSrv.getClient());
        $scope.chosenClient=$scope.clients[0];
        $scope.chosen=$scope.clients[0];
      }

      $scope.setData=function(chosen){
        $scope.chosen=chosen
      };

      $scope.selectTemplate=function(chosen){
        switch(chosen){
          case 'BIRTHDAY':
                $scope.chosenTemplate.birthday='chosen_template';
                $scope.chosenTemplate.christmas='';
                $scope.chosenTemplate.reward='';
                break;
          case 'CHRISTMAS':
                $scope.chosenTemplate.birthday='';
                $scope.chosenTemplate.christmas='chosen_template';
                $scope.chosenTemplate.reward='';
                break;
          case 'REWARD':
                $scope.chosenTemplate.birthday='';
                $scope.chosenTemplate.christmas='';
                $scope.chosenTemplate.reward='chosen_template';
                break;
        }
        template=chosen;
      };

      $scope.reward=rewardIdentService.getReward();

      $scope.showToast = function(message) {
        var toast = $mdToast.simple()
          .content(message)
          .action('OK')
          .highlightAction(false)
          .position('left bottom right');
        $mdToast.show(toast);
      };

      $scope.cancel = function() {
        $state.go('clientsGroup');
      };

      $scope.sendEmail=function(){
        var clientsId=[];
        var emailOption = {
          template: template,
          clients: $scope.clients,
          reward: $scope.reward._id
        };

        ClientsSrv.sendEmailToClient(emailOption, function (data) {
          console.log(data);
            if(data.sendClients){
              $state.go('clientsGroup');
              $scope.showToast('Good!, your rewards has been sent :D');
            }else if(data.status===500){
              $state.go('clientsGroup');
              $scope.showToast('You have exceeded your emails. Max send is 2 emails per month.');
            }else if(data.status===-1){
              $scope.showToast('You do not have internet connection. :(');
            }
        });
      };

      if(!$scope.clients[0].name){
        $state.go('clientsGroup');
      }
    });
}());
