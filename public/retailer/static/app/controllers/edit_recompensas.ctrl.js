/**
 * Created by Kevin on 28-Oct-16.
 */
/**
 * Created by Kevin on 28-Oct-16.
 */
(function(){
  angular.module('retailerApp').controller('editRecompensasCtrl',
    function($rootScope, $scope,RewardsSrv, $mdDialog, editRewardService, updateRewardService){
      $scope.rewardEditing = angular.copy(editRewardService.getReward());
      $scope.rewards = updateRewardService.getRewards();

      $scope.saveReward = function() {


        RewardsSrv.updateReward($scope.rewardEditing, function (resp) {
          $mdDialog.hide();
          console.log($scope.rewards);
          for(i=0; i < $scope.rewards.length; i++) {
            if($scope.rewards[i]._id === $scope.rewardEditing._id) {
              $scope.rewards[i].title = $scope.rewardEditing.title;
              $scope.rewards[i].points = $scope.rewardEditing.points;
            }
          }
          $scope.editAddReward();
        });

      };

      $scope.editAddReward = function (rewardId) {
        $scope.editRewardId = rewardId;
      };

      $scope.closeModal = function() {
        $mdDialog.hide();
      };

    });
}());
