/**
 * Created by Jesus Soto on 10/28/2016.
 */
(function(){
  angular.module('retailerApp').controller('reportsCtrl',
    function($rootScope, $scope, ClientsSrv, RetailersSrv){
      $scope.birthdayClients=[];
      $scope.filter = {
        ageGroup: {}
      };

      $scope.statusMap = {
        '1': 'Issued',
        '2': 'Concluded',
        '3': 'Expired'
      };

      RetailersSrv.exchangedRewardsToVouchers(function (resp) {
          $scope.rewardsInVouchers = resp;
          for(var voucher in $scope.rewardsInVouchers){
            $scope.rewardsInVouchers[voucher].voucher.class='btn btn-blue';
            switch($scope.rewardsInVouchers[voucher].voucher.status){
              case 1:
                $scope.rewardsInVouchers[voucher].voucher.status='Issued';
                break;
              case 2:
                $scope.rewardsInVouchers[voucher].voucher.status='Concluded';
                $scope.rewardsInVouchers[voucher].voucher.class='btn btn-greenalt';
                break;
              case 3:
                $scope.rewardsInVouchers[voucher].voucher.status='Expired';
                $scope.rewardsInVouchers[voucher].voucher.class='btn btn-yellow';
                break;
            }
          }
        }
      );

      getFilterClients1($scope.filter);
      function getFilterClients1(filter) {
        ClientsSrv.getFilterClients(filter, function (resp) {
          $scope.clients = resp;
          for(var client in $scope.clients){
            var y = new Date($scope.clients[client].birthday);
            var j = new Date();
            if(y.getMonth()===j.getMonth()){
              $scope.birthdayClients.push($scope.clients[client]);
            }
          }
        });
      }

      $scope.genderLabels = ["Male", "Female"];
      $scope.ageGroupLabels = [
        "Age Group 17 or less",
        "Age Group from 18 to 25",
        "Age Group from 26 to 30",
        "Age Group from 30 to 40",
        "Age Group from 41 or more"
      ];

      getFilterClients({'onlyLength': true});
      getFilterClients({'profitable': 10});
      getFilterClients({'birthday': true});
      getFilterClients({'genderGraphData': true});
      getFilterClients({'ageGroupGraphData': true});

      function getFilterClients(option) {
        ClientsSrv.getFilterClients(option, function (resp) {
          if (option.onlyLength) {
            $scope.numberOfClients = resp.value;
          } else if (option.profitable) {
            $scope.mostProfitableClients = resp;
          } else if (option.birthday) {
            $scope.clientsBirthday = resp || [];

            $scope.clientsBirthday.sort(function (a, b) {
              if (new Date(a.birthday).getMonth() > new Date(b.birthday).getMonth()) {
                return 1;
              }
              if (new Date(a.birthday).getMonth() === new Date(b.birthday).getMonth()) {
                if (new Date(a.birthday).getDate() > new Date(b.birthday).getDate()) {
                  return 1;
                } else {
                  return -1;
                }
              }
              if (new Date(a.birthday).getMonth() < new Date(b.birthday).getMonth()) {
                return -1;
              }
              return 0;
            });
          } else if (option.genderGraphData) {
            if ((+resp.male + +resp.female) === 0) {
              $scope.gender = [];
            } else {
              $scope.gender = [resp.male, resp.female];
            }
          } else if (option.ageGroupGraphData) {
            if (!+resp.TO_17 && !+resp.FROM_18_TO_25 && !+resp.FROM_26_TO_30 && !+resp.FROM_31_TO_40 && !+resp.FROM_41) {
              $scope.ageGroup = [];
            } else {
              $scope.ageGroup = [resp.TO_17, resp.FROM_18_TO_25, resp.FROM_26_TO_30, resp.FROM_31_TO_40, resp.FROM_41];
            }
          }
        });
      }

      $scope.options =  { legend: { display: true } };

    });
}());
