(function () {
  'use strict';

  angular.module('retailerApp').controller('clientsGroupCtrl',
    ['$scope', '$rootScope', '$mdDialog', 'ClientsSrv', 'set_clientSrv', '$stateParams', clientsGroupCtrl]);

  function clientsGroupCtrl($scope, $rootScope, $mdDialog, ClientsSrv, set_clientSrv) {

    $scope.filter = {
      ageGroup: {}
    };

    $scope.chosen={
      birthday:{
        background: 'white'
      },
      Inactive:{
        background: 'white'
      },
      prof:{
        background: 'white'
      },
      _17:{
        background: 'white'
      },
      _18:{
        background: 'white'
      },
      _26:{
        background: 'white'
      },
      _30:{
        background: 'white'
      },
      _41:{
        background: 'white'
      },
      male:{
        background: 'white'
      },
      female:{
        background: 'white'
      }
    };

    var sendEmailMap = {
      'ALL': {all: true},
      'PROFITABLE': {profitable: true},
      'BIRTHDAY': {birthday: true},
      'INACTIVE': {inactive: true},
      'PRESELECTION': {preselection: []},
      'GENDER_M': {gender: 'M'},
      'GENDER_F': {gender: 'F'},
      'AGE_GROUP_17': {ageGroup: {ageFrom: '', ageTo: 17}},
      'AGE_GROUP_18_25': {ageGroup: {ageFrom: 18, ageTo: 25}},
      'AGE_GROUP_26_30': {ageGroup: {ageFrom: 26, ageTo: 30}},
      'AGE_GROUP_30_40': {ageGroup: {ageFrom: 30, ageTo: 40}},
      'AGE_GROUP_41': {ageGroup: {ageFrom: 41, ageTo: ''}}
    };

    $scope.selectedClients = {};

    getFilterClients($scope.filter);

    $scope.disableSendEmail = (function(){
      return $rootScope.currentRetailer.sendEmailStatistic && $rootScope.currentRetailer.sendEmailStatistic.quantity > 1
    }());

    $scope.selectAll = function () {

      $scope.selectedAll = $scope.selectedAll ? true : false;

      angular.forEach($scope.selectedClients, function (item, i, arr) {
        arr[i] = $scope.selectedAll;
      });
    };

    $scope.orderClient = {
      predicate: 'name',
      reverse: false
    };

    $scope.sortClients = function (predicate) {
      $scope.orderClient.reverse = ($scope.orderClient.predicate === predicate) ? !$scope.orderClient.reverse : false;
      $scope.orderClient.predicate = predicate;
    };

    $scope.ifAllSelected = function () {

      for (var key in $scope.selectedClients) {
        if (!$scope.selectedClients[key]) {
          $scope.selectedAll = false;
          return
        }
      }
      $scope.selectedAll = true;
    };

    $scope.showAdvanced = function() {
      $mdDialog.show({
        templateUrl: 'app/views/clients/client_modal.html',
        clickOutsideToClose:true
      });
    };

    $scope.openClient=function(client){
      $scope.showAdvanced();
      set_clientSrv.setClient(client);
    };

    $scope.close = function () {
      $mdDialog.hide();
    };

    $scope.chooseGender = function (val) {

      switch(val) {
        case 'GENDER_M':
          if($scope.chosen.male.background==='white'){
            $scope.chosen.male.background='#F7F7F7';
          }else{
            $scope.chosen.male.background='white';
          }
          $scope.chosen.female.background='white';
          break;
        case 'GENDER_F':
          if($scope.chosen.female.background==='white'){
            $scope.chosen.female.background='#F7F7F7';
          }else{
            $scope.chosen.female.background='white';
          }
          $scope.chosen.male.background='white';
          break;
      }

      if ($scope.filter.gender === sendEmailMap[val].gender) {
        $scope.filter.gender = '';
        getFilterClients($scope.filter);
      } else {
        $scope.filter.gender = sendEmailMap[val].gender;
        getFilterClients($scope.filter);
      }
    };

    $scope.chooseAge = function (val) {

      switch(val){
        case 'AGE_GROUP_17':
          $scope.chosen._41.background='white';
          $scope.chosen._30.background='white';
          $scope.chosen._26.background='white';
          $scope.chosen._18.background='white';
          if($scope.chosen._17.background==='white'){
            $scope.chosen._17.background='#F7F7F7';
          }else{
            $scope.chosen._17.background='white';
          }
          break;
        case 'AGE_GROUP_18_25':
          $scope.chosen._41.background='white';
          $scope.chosen._30.background='white';
          $scope.chosen._26.background='white';
          if($scope.chosen._18.background==='white'){
            $scope.chosen._18.background='#F7F7F7';
          }else{
            $scope.chosen._18.background='white';
          }
          $scope.chosen._17.background='white';
          break;
        case 'AGE_GROUP_26_30':
          $scope.chosen._41.background='white';
          $scope.chosen._30.background='white';
          if($scope.chosen._26.background==='white'){
            $scope.chosen._26.background='#F7F7F7';
          }else{
            $scope.chosen._26.background='white';
          }
          $scope.chosen._18.background='white';
          $scope.chosen._17.background='white';
          break;
        case 'AGE_GROUP_30_40':
          $scope.chosen._41.background='white';
          if($scope.chosen._30.background==='white'){
            $scope.chosen._30.background='#F7F7F7';
          }else{
            $scope.chosen._30.background='white';
          }
          $scope.chosen._26.background='white';
          $scope.chosen._18.background='white';
          $scope.chosen._17.background='white';
          break;
        case 'AGE_GROUP_41':
          if($scope.chosen._41.background==='white'){
            $scope.chosen._41.background='#F7F7F7';
          }else{
            $scope.chosen._41.background='white';
          }
          $scope.chosen._30.background='white';
          $scope.chosen._26.background='white';
          $scope.chosen._18.background='white';
          $scope.chosen._17.background='white';
          break;
      }

      if ($scope.filter.ageGroup.ageFrom === sendEmailMap[val].ageGroup.ageFrom
        && $scope.filter.ageGroup.ageTo === sendEmailMap[val].ageGroup.ageTo) {

        $scope.filter.ageGroup = {};
        getFilterClients($scope.filter);
      } else {
        $scope.filter.ageGroup = angular.copy(sendEmailMap[val].ageGroup);
        getFilterClients($scope.filter);
      }
    };

    $scope.chooseBirthday = function () {
      if($scope.chosen.birthday.background==='white'){
        $scope.chosen.birthday.background='#F7F7F7';
      }else{
        $scope.chosen.birthday.background='white';
      }

      $scope.filter.birthday = !$scope.filter.birthday;
      getFilterClients($scope.filter);
    };

    $scope.chooseInactive = function () {
      if($scope.chosen.Inactive.background==='white'){
        $scope.chosen.Inactive.background='#F7F7F7';
      }else{
        $scope.chosen.Inactive.background='white';
      }
      $scope.filter.inactive = !$scope.filter.inactive;
      getFilterClients($scope.filter);
    };

    $scope.chooseProfitable = function () {
      if($scope.chosen.prof.background==='white'){
        $scope.chosen.prof.background='#F7F7F7';
      }else{
        $scope.chosen.prof.background='white';
      }
      $scope.filter.profitable = !$scope.filter.profitable;
      getFilterClients($scope.filter);
    };

    $scope.sendingEmail = function () {
      var selectedClientsToMessage = [];
      for (var key in $scope.selectedClients) {
        loop:
          if ($scope.selectedClients[key]) {
            for (var k = 0; k < $scope.clients.length; k++) {
              if ($scope.clients[k]._id === key) {
                selectedClientsToMessage.push($scope.clients[k]);
                break loop;
              }
            }
          }
      }
      $scope.chosen = selectedClientsToMessage.length;
      if (selectedClientsToMessage.length === 0) {
        return;
      }
      $scope.showAdvanced();
      set_clientSrv.setClient(selectedClientsToMessage);
    };

    function getFilterClients(filter) {
      ClientsSrv.getFilterClients(filter, function (resp) {
        $scope.clients = resp;
        for(var client in $scope.clients){
          $scope.clients[client].actualPoints = resp[client].points + resp[client].pointsInVouchers;
        }
        if ($scope.clients) {
          $scope.selectedAll = false;
          $scope.selectedClients = {};

          for (var k = 0; k < $scope.clients.length; k++) {
            $scope.selectedClients[$scope.clients[k]._id] = false
          }
        }
      });
    }
  }

})();

