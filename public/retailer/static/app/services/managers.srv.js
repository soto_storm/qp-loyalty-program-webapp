(function () {
  'use strict';

  angular.module('retailerApp').factory('ManagersSrv',
    ['$http', ManagersSrv]);

  function ManagersSrv($http) {

    var initialized = false,
      initListeners;

    function getAllManagers(callback) {
      $http({
        url: '/api/manager',
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function (err) {

        callback(err);

      });
    }

    function getManagerByRetailerId(managerId, callback) {

      $http({
        url: '/api/manager?managerId=' + managerId,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }

        initialized = true;
        if (initListeners) {
          initListeners();
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getAllManagers: getAllManagers,
      getManagerByRetailerId: getManagerByRetailerId
    }

  }

}());
