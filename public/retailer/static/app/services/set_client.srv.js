/**
 * Created by Jesus Soto on 10/27/2016.
 */
(function(){
  angular.module('retailerApp').service('set_clientSrv',
    function(){
      var client = [];
      return{
        getClient: function(){
          return client;
        },
        setClient: function(value){
          client = value;
        }
      };
    });
}());
