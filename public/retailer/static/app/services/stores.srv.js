(function () {
  'use strict';

  angular.module('retailerApp').factory('StoresSrv',
    ['$rootScope', '$http', StoresSrv]);

  function StoresSrv($rootScope, $http) {

    function updateStore(store, callback) {
      if (!store) {
        throw 'store is required (StoresSrv.updateStore)'
      }

      if (store._id) {

        $http({
          url: '/api/store/' + store._id,
          method: 'PUT',
          data: store
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function (err) {

          callback(err);

        });

      } else {

        store.retailer = $rootScope.currentRetailer._id;

        $http({
          url: '/api/store',
          method: 'POST',
          data: store
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function (err) {

          callback(err);

        });
      }
    }


    function getStoreByRetailer(retailerId, callback) {
      if (!retailerId) {
        throw 'retailerId is required (StoresSrv.getStoreByRetailer)'
      }

      $http({
        url: '/api/store?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    function getUniqueToken(storeId, callback) {
      if (!storeId) {
        throw 'storeId is required (StoresSrv.getUniqueToken)'
      }

      $http({
        url: '/api/store/getUniqueToken?storeId=' + storeId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);
      });
    }

    function getRetailerRevenue(callback) {

      $http({
        url: '/api/store/revenue?retailerId=' + $rootScope.currentRetailer._id,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getStoreByRetailer: getStoreByRetailer,
      getUniqueToken: getUniqueToken,
      updateStore: updateStore,
      getRetailerRevenue: getRetailerRevenue
    }

  }

}());
