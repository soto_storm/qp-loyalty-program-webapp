/**
 * Created by Kevin on 28-Oct-16.
 */
(function(){
  angular.module('retailerApp').service('editRewardService',
    function(){
      var reward = '';
      return{
        getReward: function(){
          return reward;
        },
        setReward: function(value){
          reward = value;
        }
      };
    });
}());
