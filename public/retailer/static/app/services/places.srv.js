(function () {
  'use strict';

  angular.module('retailerApp').factory('PlacesSrv',
    ['$http', PlacesSrv]);

  function PlacesSrv($http) {

    function getPlacesByRetailer(retailerId, callback) {
      if (!retailerId) {
        throw 'RetailerId is required (PlacesSrv.getPlacesByRetailer)'
      }

      $http({
        url: '/api/place?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {

        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    return {
      getPlacesByRetailer: getPlacesByRetailer
    }

  }
}());
