(function () {
  'use strict';

  angular.module('retailerApp').factory('RewardsSrv',
    ['$http', '$rootScope', RewardsSrv]);

  function RewardsSrv($http, $rootScope) {

    function getRewardsByRetailerId(retailerId, callback) {
      if (!retailerId) {
        throw 'retailerId is required (RewardsSrv.getRewardsByRetailerId)'
      }

      $http({
        url: '/api/reward?retailerId=' + retailerId,
        method: 'GET'
      }).then(function (resp) {
        if (callback) {
          callback(resp.data);
        }
      }, function (err) {

        callback(err);

      });
    }

    function updateReward(reward, callback) {
      if (!reward) {
        throw 'reward is required (StoresSrv.updateReward)'
      }

      if (reward._id) {

        $http({
          url: '/api/reward/' + reward._id,
          method: 'PUT',
          data: reward
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function (err) {

          callback(err);

        });

      } else {

        reward.retailer = $rootScope.currentRetailer._id;

        $http({
          url: '/api/reward',
          method: 'POST',
          data: reward
        }).then(function (resp) {

          if (callback) {
            callback(resp.data);
          }
        }, function (err) {

          callback(err);

        });
      }
    }

    return {
      updateReward: updateReward,
      getRewardsByRetailerId: getRewardsByRetailerId
    }
  }

}());
