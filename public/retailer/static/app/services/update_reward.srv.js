/**
 * Created by Kevin on 28-Oct-16.
 */
(function(){
  angular.module('retailerApp').service('updateRewardService',
    function(){
      var rewards = '';
      return{
        getRewards: function(){
          return rewards;
        },
        setRewards: function(value){
          rewards = value;
        }
      };
    });
}());
