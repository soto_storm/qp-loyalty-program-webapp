/**
 * Created by Jesus Soto on 10/26/2016.
 */
var app=angular.module('retailerApp',['ui.router',
                                      'ngMaterial',
                                      'ui.utils.masks',
                                      'ui.mask',
                                      'ngMap',
                                      'geolocation',
                                      'chart.js'])
  .run(function($rootScope, RetailersSrv, geolocation){
    $rootScope._loadCurrentRetailer = function (callback) {
      RetailersSrv.getCurrentRetailer(function (data) {
        $rootScope.currentRetailer = angular.copy(data);
        $rootScope.addSeparator = function(number) {
          return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        };

        if($rootScope.currentRetailer.points) { // add a comma to the retailer points so it looks cleaner.
          $rootScope.currentRetailer.points = $rootScope.addSeparator($rootScope.currentRetailer.points);
        }
        if (callback) {
          callback($rootScope.currentRetailer);
        }
      });
    };
    $rootScope._loadCurrentRetailer();
    $rootScope.currentPosition ='[-10.698900799999999,-60.6273539]';
    $rootScope.getDistance=function() {
      geolocation.getLocation().then(function (data) {
        $rootScope.coords = {lat: data.coords.latitude, long: data.coords.longitude};
        $rootScope.currentPosition ='['+$rootScope.coords.lat+','+$rootScope.coords.long+']';
      });
    };
    $rootScope.getDistance();
    $rootScope.logout = function () {
      RetailersSrv.logoutRetailer(function (data) {
        window.location.href = data.redirectTo ? data.redirectTo : "/authentication";
      })
    };
});


  app.config(function config($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/reports');

    $stateProvider.state('reports', waitForServices({
      url: '/reports',
      templateUrl: 'app/views/reports/reports.html',
      controller: 'reportsCtrl'
    }))
      .state('clientsGroup', waitForServices({
        url: '/clientsGroup',
        templateUrl: 'app/views/clients/clients.html',
        controller: 'clientsGroupCtrl',
        params: {
          emit: null
        }
      }))
      .state('support', waitForServices({
        url: '/support',
        templateUrl: 'app/views/support/support.html',
        controller: 'supportCtrl'
      })).state('recharge', waitForServices({
      url: '/recharge',
      templateUrl: 'app/views/recharge/recharge.html',
      controller: 'rechargeCtrl'
      })).state('minha-conta', waitForServices({
        url: '/minha-conta',
        templateUrl: 'app/views/minha-conta/minha-conta.html',
        controller: 'minhaContaCtrl'
      })).state('recompensas', waitForServices({
      url: '/recompensas',
      templateUrl: 'app/views/recompensas/recompensas.html',
      controller: 'recompensasCtrl'
    })).state('lojas', waitForServices({
      url: '/lojas',
      templateUrl: 'app/views/lojas/lojas.html',
      controller: 'lojasCtrl'
    })).state('emailTheme', waitForServices({
     /* .state('rewards', waitForServices({
        url: '/rewards',
        templateUrl: 'app/core/rewards/rewards.tpl.html',
        controller: 'app.core.rewards.rewardsCtrl'
      }))
      .state('profile', waitForServices({
        url: '/profile',
        templateUrl: 'app/core/profile/profile.tpl.html',
        controller: 'app.core.profile.profileCtrl'
      }))
      .state('rechargeDots', waitForServices({
        url: '/rechargeDots',
        templateUrl: 'app/core/recharge_dots/recharge_dots.tpl.html',
        controller: 'app.core.rechargeDots.rechargeDotsCtrl'
      }))*/

  /*   .state('stores', waitForServices({
        url: '/stores',
        templateUrl: 'app/core/stores/stores.tpl.html',
        controller: 'app.core.stores.storesCtrl'
      }))*/
        url: '/emailTheme',
        templateUrl: 'app/views/clients/theme_email.html',
        controller: 'theme_emailCtrl',
        params: {
          groupClients: null,
          oneClient: null,
          clients: null,
          reward: null
        }
      }));

    function waitForServices(conf) {
      if (!conf.resolve) {
        conf.resolve = {};
      }

      conf.resolve.waitForServicesInitialization = ['$q', 'RetailersSrv', function ($q, RetailersSrv) {

        var def = $q.defer();
        RetailersSrv._addInitListener(function () {
          def.resolve();
        });

        return def.promise;
      }];

      return conf;
    }
  });

app.controller('googleMaps', function($scope){
  $scope.gPlace;
});
