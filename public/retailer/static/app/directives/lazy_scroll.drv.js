"use strict";
(function () {
  angular.module('retailerApp').directive("lazyScroll",
    ['PlacesSrv', 'RetailersSrv', lazyScroll]);

  function lazyScroll(PlacesSrv, RetailersSrv) {
    return {
      restrict: "A",
      link: function (scope, element) {
        var allActivities;

        scope.activities = [];

        var visibleHeight = $(element).height();
        var threshold = 100;

        RetailersSrv.getCurrentRetailer(function(data) {
          PlacesSrv.getPlacesByRetailer(data._id, function (data) {
            allActivities = data.sort(function (a, b) {
              if (a.createdAt < b.createdAt) {
                return 1;
              }
              if (a.createdAt > b.createdAt) {
                return -1;
              }

              return 0;
            });
            loadMore();
          });
        });

        $(element).scroll(function () {
          var scrollableHeight = $(element).prop('scrollHeight');
          var hiddenContentHeight = scrollableHeight - visibleHeight;

          var isNeedLoad = scope.activities.length < allActivities.length;

          if ((hiddenContentHeight - $(element).scrollTop() <= threshold) && isNeedLoad) {
            loadMore();
            scope.$apply()
          }
        });

        function loadMore() {
          var newActivities = [];
          var currentLength = scope.activities.length;
          for (var i = currentLength; i < currentLength + 20; i++) {
            if (i >= allActivities.length) {
              break;
            }

            newActivities.push(allActivities[i])
          }

          scope.activities = scope.activities.concat(newActivities);
          for(var i=0; i<scope.activities.length; i++){
            switch(scope.activities[i].type){
              case 'USE_VOUCHER':scope.activities[i].type='Trocados';
                break;
              case 'UPDATE_DOTS':scope.activities[i].type='Acumulados';
                break;
            }
          }
        }
      }
    };
  }

}());
