/**
 * Created by Jesus Soto on 10/28/2016.
 */
app.directive('googleplace', function($rootScope){
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, model) {
      var options = {
        types: [],
        componentRestrictions: {}
      };
      scope.gPlace = new google.maps.places.Autocomplete(element[0], options);
      google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
        scope.$apply(function() {
          var place = scope.gPlace.getPlace();
          $rootScope.googleAddress=place.name+';'+place.geometry.location.lat()+
            ';'+place.geometry.location.lng()+";"+place.formatted_address;
          model.$setViewValue(element.val());
          $rootScope.$emit("addressChosen", {});
        });
      });
    }
  };
});
