(function () {
  'use strict';

  var Checkout = function () {
    this._storage = {
      client: {},
      voucher: {},
      points: 0,
      store: {},
      retailer: {}
    };
    this._currentForm = null;
    this._dontInitForm = false
  };

  Checkout.prototype.runForms = {};
  Checkout.prototype.forms = {
    _initClientForm: $('.container > div.chm-init'),
    _notClientForm: $('.container > div.chm-not-client'),
    _hasClientForm: $('.container > div.chm-has-client'),
    _showDotsForm: $('.container > div.chm-add-dots'),
    _exchangeForm: $('.container > div.chm-change'),
    _changeFinishForm: $('.container > div.chm-change-finish')
  };

  Checkout.prototype.goToInitClientForm = function () {
    this._renderForm(this.forms._initClientForm);
    // this.cookie.remove('client');
    // this.cookie.remove('form');
    this.runForms._destroyFunc = this.runForms._runInitClientForm();
  };

  Checkout.prototype.goToNotClientForm = function () {
    this._renderForm(this.forms._notClientForm);
    // this.cookie.set('form', 'notClientForm');
    this.runForms._destroyFunc = this.runForms._runNotClientForm();
  };

  Checkout.prototype.goToHasClientForm = function () {
    this._renderForm(this.forms._hasClientForm);
    // this.cookie.set('form', 'hasClientForm');
    this.runForms._destroyFunc = this.runForms._runHasClientForm();
  };

  Checkout.prototype.goToShowDotsForm = function () {
    this._renderForm(this.forms._showDotsForm);
    // this.cookie.set('form', 'showDotsForm');
    this.runForms._destroyFunc = this.runForms._runShowDotsForm();
  };

  Checkout.prototype.goToExchangeForm = function () {
    this._renderForm(this.forms._exchangeForm);
    // this.cookie.set('form', 'exchangeForm');
    this.runForms._destroyFunc = this.runForms._runExchangeForm();
  };

  Checkout.prototype.goToChangeFinishForm = function () {
    this._renderForm(this.forms._changeFinishForm);
    // this.cookie.set('form', 'changeFinishForm');
    this.runForms._destroyFunc = this.runForms._runChangeFinishForm();
  };

  Checkout.prototype.showServerError = function () {
    var serverError = $('.header > .error');
    this.goToInitClientForm();
    serverError.addClass("active");

    setTimeout(function () {
      serverError.removeClass("active");
    }, 3000);
  };

  Checkout.prototype.setClient = function (client) {
    if (!client || (Object.keys(client).length === 0)) {
      throw 'client is required argument for setClient method and must have one or more filds';
    }
    // this.cookie.set('client', client.cpf);
    this._storage.client = client;
  };

  Checkout.prototype.getClient = function () {
    if (Object.keys(this._storage.client).length === 0) {
      console.warn('client is not set yet');
    }
    return this._storage.client
  };

  Checkout.prototype.setVoucher = function (voucher) {
    if (!voucher || (Object.keys(voucher).length === 0)) {
      throw 'Voucher is required argument for setVoucher method and must have one or more filds';
    }
    this._storage.voucher = voucher;
  };

  Checkout.prototype.getVoucher = function () {
    if (Object.keys(this._storage.voucher).length === 0) {
      console.warn('Voucher is not set yet')
    }
    return this._storage.voucher
  };

  Checkout.prototype.setStore = function (store) {
    if (!store || (Object.keys(store).length === 0)) {
      throw 'Store is required argument for setStore method and must have one or more filds';
    }

    // var lastStoreId = this.cookie.get('store');
    // if (lastStoreId && (lastStoreId != store._id)) {
    //   this.cookie.remove(true);
    //   this._dontInitForm = false
    // } else {
    //   this.cookie.set('store', store._id);
    // }

    this._storage.store = store;
  };

  Checkout.prototype.getStore = function () {
    if (Object.keys(this._storage.store).length === 0) {
      console.warn('Store is not set yet')
    }
    return this._storage.store
  };

  Checkout.prototype.setRetailer = function (retailer) {
    if (!retailer || (Object.keys(retailer).length === 0)) {
      throw 'Retailer is required argument for setRetailer method and must have one or more filds';
    }

    if (+retailer.points <= 1000) {
      $('.danger-dots').text(retailer.points);
      $('.danger').addClass("active");
      $('.submit.score').hide();
      $('.enough-message').show()
    } else {
      $('.danger').removeClass("active");
      $('.submit.score').show();
      $('.enough-message').hide();
    }

    this._storage.retailer = retailer;
  };

  Checkout.prototype.getRetailer = function () {
    if (Object.keys(this._storage.retailer).length === 0) {
      console.warn('Retailer is not set yet')
    }
    return this._storage.retailer
  };

  Checkout.prototype.setCurrentForm = function (form) {
    if (!form) {
      throw 'Form is required argument for setCurrentForm method';
    }
    this._currentForm = form;
  };

  Checkout.prototype.updateRetailer = function () {
    var retailerId = this.getRetailer()._id;
    $.ajax({
      url: "/api/retailer?id=" + retailerId + '&t=' + +(new Date()),
      type: 'GET',
      success: function (data) {

        window._checkout.setRetailer(data);

      },
      error: function () {
        window._checkout.showServerError()
      }
    });
  };

  Checkout.prototype._renderForm = function (form) {
    var destroyFunc = this.runForms._destroyFunc;
    if (destroyFunc && Object.create({}).toString.call(destroyFunc) === '[object Function]') {
      this.runForms._destroyFunc();
    }

    if (this._currentForm) {
      this._currentForm.removeClass('active');
    }

    form.find('input[type=text]').focus();
    form.addClass('active');
    this.setCurrentForm(form)
  };

  $.ajax({
    url: "/goToCheckout/getData?token=" + urlParam('token'),
    type: 'GET',
    success: function (data) {
      if (!data.storeId || !data.retailerId) {
        window._checkout.showServerError()
      } else {
        initStoreAndRetailer(data);
      }
    },
    error: function () {
      window._checkout.showServerError()
    }
  });

  function initStoreAndRetailer(data) {
    var storeId = data.storeId;
    var retailerId = data.retailerId;

    var loadStatus = {
      store: false,
      retailer: false
    };

    $.ajax({
      url: "/api/store?storeId=" + storeId + '&t=' + +(new Date()),
      type: 'GET',
      success: function (data) {

        window._checkout.setStore(data);
        loadStatus.store = true;

        if (loadStatus.store && loadStatus.retailer) {
          startCheckout()
        }
      },
      error: function () {
        window._checkout.showServerError()
      }
    });

    $.ajax({
      url: "/api/retailer?id=" + retailerId + '&t=' + +(new Date()),
      type: 'GET',
      success: function (data) {

        window._checkout.setRetailer(data);
        loadStatus.retailer = true;

        if (loadStatus.store && loadStatus.retailer) {
          startCheckout()
        }
      },
      error: function () {
        window._checkout.showServerError()
      }
    });

    function startCheckout() {
      var store = window._checkout.getStore();
      var retailer = window._checkout.getRetailer();
      var storeName = $('.checkout-info .store-name');
      var retailerName = $('.checkout-info .retailer-name');

      storeName.text(store.storeName);
      retailerName.text(retailer.name);

      var checkout = window._checkout;

      if (!checkout._dontInitForm) {
        if (checkout._currentForm) {
          checkout._currentForm.removeClass('active');
        }
        checkout.setCurrentForm(checkout.forms._initClientForm);
        checkout.goToInitClientForm();
      }

    }
  }

  function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if(!results){
      return ''
    }
    return results[1] || 0;
  }

  if (!window._checkoutConstructor) {
    window._checkoutConstructor = Checkout;
    window._checkout = new Checkout();
  }

})();
