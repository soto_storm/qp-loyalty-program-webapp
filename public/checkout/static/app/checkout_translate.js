(function () {
  'use strict';

  var checkout = window._checkout;
  var langContainer = $('.lang-container');
  var EnLangObj;
  var PtLangObj;

  setLanguage('en');

  langContainer.find("input:radio[value='en']").attr('checked', 'checked');

  langContainer.find('input:radio[name="language"]').click(function () {
    var lang = $(this).context.value;

    if (!EnLangObj || !PtLangObj) {
      setLanguage(lang);
    } else {
      renderLang(lang === 'en' ? EnLangObj : PtLangObj);
    }
  });

  function setLanguage(lang) {
    $.ajax({
      url: 'app/i18n/locale-' + lang + '.json',
      type: 'GET',
      success: function (response) {
        lang === 'en' ? EnLangObj = response : PtLangObj = response;
        renderLang(response);
      },
      error: function (error) {
        checkout.showServerError()
      }
    });
  }

  function renderLang(response) {
    $('body').find("[lng]").each(function () {
      var lng = response[$(this).attr('lng')];
      var tag = $(this)[0].tagName.toLowerCase();

      switch (tag) {

        case "input":

          if (this.hasAttribute("lng-placeholder")) {
            $(this).attr('placeholder', lng);
          } else {
            $(this).val(lng);
          }
          break;

        default:
          $(this).html(lng);
          break;
      }
    });
  }
})();
