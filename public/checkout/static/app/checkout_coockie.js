(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;

  checkoutProto.cookie = {

    set: function (name, value) {
      $.cookie(name, value)
    },

    get: function (name) {
      return $.cookie(name)
    },

    remove: function (name) {
      if ((typeof name === "boolean") && name) {
        $.removeCookie('form');
        $.removeCookie('client');
        $.removeCookie('store');
      } else {
        $.removeCookie(name);
      }

    }

  };

  (function () {
    var lastForm = checkout.cookie.get('form');
    var client = checkout.cookie.get('client');
    if (client) {

      $.ajax({
        url: "/api/client?cpf=" + client + '&t=' + +(new Date()),
        type: 'GET',
        success: function (data) {
          if (data && data._id) {
            checkout.setClient(data);
          } else {
            checkout.setClient({cpf: client});
          }

          if (lastForm) {
            checkout._dontInitForm = true;
            switch (lastForm) {

              case 'hasClientForm' :
                checkout.goToHasClientForm();
                break;

              case 'notClientForm':
                checkout.goToNotClientForm();
                break;

              case 'exchangeForm':
                checkout.goToExchangeForm();
                break;
            }

          }

        },
        error: function () {
          checkout.showServerError()
        }
      });

    }
  }())

})();
