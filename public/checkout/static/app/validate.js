(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;

  var oldVoucher;

  checkoutProto.validate = {
    setErrorToForm: setErrorToForm,
    validatePoints: validatePoints,
    validateCpf: validateCpf,
    validateVoucher: validateVoucher
  };

  function setErrorToForm(errorKey, form, errorObj) {
    var submitButton = form.find('input[type=button].submit');

    var errorZero = errorObj.zero && form.find('.error-label[checkout-zero]');
    var errorRequired = errorObj.required && form.find('.error-label[checkout-required]');
    var errorPattern = errorObj.pattern && form.find('.error-label[checkout-pattern]');
    var errorInvalid = errorObj.invalid && form.find('.error-label[checkout-invalid]');
    var errorLengthMax = errorObj.lengthMax && form.find('.error-label[checkout-length-max]');
    var errorVoucher = errorObj.voucherError && form.find('.error-label[checkout-voucher-error]');
    var errorVoucherPattern = errorObj.length && form.find('.error-label[checkout-voucher-pattern]');
    var errorRetailerPoints = errorObj.retailerPoints && form.find('.error-label[checkout-retailer-points]');

    if (errorKey) {
      form.addClass('has-error');
      submitButton.attr("disabled", true);
    } else {
      submitButton.attr("disabled", false);
      form.removeClass('has-error');
    }

    errorPattern && errorPattern.css({display: errorKey === 'pattern' ? 'block' : 'none'});
    errorRequired && errorRequired.css({display: errorKey === 'required' ? 'block' : 'none'});
    errorInvalid && errorInvalid.css({display: errorKey === 'invalid' ? 'block' : 'none'});
    errorZero && errorZero.css({display: errorKey === 'zero' ? 'block' : 'none'});
    errorVoucherPattern && errorVoucherPattern.css({display: errorKey === 'checkout-voucher-pattern' ? 'block' : 'none'});
    errorLengthMax && errorLengthMax.css({display: errorKey === 'length-max' ? 'block' : 'none'});
    errorVoucher && errorVoucher.css({display: errorKey === 'checkout-voucher-error' ? 'block' : 'none'});
    errorRetailerPoints && errorRetailerPoints.css({display: errorKey === 'checkout-retailer-points' ? 'block' : 'none'});

    return errorKey;
  }

  function validatePoints(points) {
    if (points == undefined || points === '') {
      return {error: 'required'}
    } else if (('' + points).length > 12) {
      return {error: 'length-max'}
    } else if ('' + points === '0,00') {
      return {error: 'zero'}
    } else {
      return {error: false}
    }
  }

  function validateCpf(cpf) {
    cpf.replace(/[^\d]+/g, '');

    if (!cpf) return {error: 'required'};
    if (cpf.length != 11 ||
      cpf == "00000000000" ||
      cpf == "11111111111" ||
      cpf == "22222222222" ||
      cpf == "33333333333" ||
      cpf == "44444444444" ||
      cpf == "55555555555" ||
      cpf == "66666666666" ||
      cpf == "77777777777" ||
      cpf == "88888888888" ||
      cpf == "99999999999")
      return {error: 'pattern'};

    var add = 0, i, rev;

    for (i = 0; i < 9; i++) {
      add += parseInt(cpf.charAt(i)) * (10 - i);
    }

    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11) rev = 0;
    if (rev != parseInt(cpf.charAt(9))) return {error: 'pattern'};

    add = 0;
    for (i = 0; i < 10; i++) {
      add += parseInt(cpf.charAt(i)) * (11 - i);
    }

    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)rev = 0;

    if (rev != parseInt(cpf.charAt(10))) {
      return {error: 'pattern'}
    }

    return {error: false};
  }

  function validateVoucher(voucher, clientId, isSend, callback) {
    var pattern = /([0-9]{3})\-([0-9]{3})\-([0-9]{3})/;
    if (!voucher) {
      callback('required');
      return;
    }

    if (!voucher.match(pattern)) {
      callback('checkout-voucher-pattern');
      return;
    }

    var retailerId = checkout.getRetailer()._id;

    if(oldVoucher !== voucher || isSend){
      $.ajax({
          url: '/api/voucher/isCanBeUsed/' + voucher + '/' + clientId + '/' + retailerId + '?t=' + +(new Date()),
          type: 'GET'
        })
        .done(function (data) {
          callback(data.value ? false : 'checkout-voucher-error');
        })
        .fail(function () {
          checkout.showServerError()
        })
        .always(function(){
          oldVoucher = '';
        });

      oldVoucher = voucher;
    }


  }

})();
