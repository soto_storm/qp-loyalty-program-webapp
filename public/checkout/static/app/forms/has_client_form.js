(function () {
	'use strict';

	var checkoutProto = window._checkoutConstructor.prototype;
	var checkout = window._checkout;
	var checkoutValidate = checkout.validate;

	checkoutProto.runForms._runHasClientForm = function () {
		var hasClientForm = checkout.forms._hasClientForm;
		var client = checkout.getClient();

		var submitButton = hasClientForm.find('.submit');
		var input = hasClientForm.find('.form-control');
		var dischargeDots = hasClientForm.find('.exchange-dots');
		var cancelButton = hasClientForm.find('.cancel');

		var errorObj = {
			zero: true,
			required: true,
			invalid: true,
			lengthMax: true,
			retailerPoints: true
		};

		var isFocusOut = false;
		var moneySettings = {
			thousands: ".",
			decimal: ","
		};

		var nameLabel = hasClientForm.find('.title .name');
		var dotsLabel = hasClientForm.find('.title .dots');

		input.val('');
		input.maskMoney(moneySettings);

		hasClientForm.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
			input.focus()
		});

		nameLabel.text(client.name);
		dotsLabel.text(client.points || 0);

		input.bind('focusout', function () {
			if (event.relatedTarget) {
				var element = $(event.relatedTarget);

				if (element.hasClass("cancel") || element.hasClass("exchange-dots")) {
					return
				}
			}

			isFocusOut = true;
			input.trigger('keyup');
		});

		input.on('keyup', function () {
			var inputVal = input.val();
			var clearVal = input.maskMoney('unmasked')[0];

			var validError = checkoutValidate.validatePoints(inputVal);

			if (+clearVal > checkout.getRetailer().points) {
				checkoutValidate.setErrorToForm('checkout-retailer-points', hasClientForm, errorObj);
				return
			}

			if (validError.error && isFocusOut) {
				checkoutValidate.setErrorToForm(validError.error, hasClientForm, errorObj);
			} else {
				checkoutValidate.setErrorToForm(false, hasClientForm, errorObj);
			}

		});

		dischargeDots.on('click', function () {
			checkout.goToExchangeForm();
		});
		cancelButton.on('click', function () {
			checkout.goToInitClientForm();
		});

		submitButton.on('click', function () {
			if (!isFocusOut) {
				isFocusOut = true
			}

			var inputVal = input.maskMoney('unmasked')[0];

			var validError = checkoutValidate.validatePoints(inputVal);

			if (validError.error) {
				checkoutValidate.setErrorToForm(validError.error, hasClientForm, errorObj);
				return;
			}

			var store = checkout.getStore();
			var retailer = checkout.getRetailer();

			var data = {
				points: +inputVal,
				store: store._id,
				retailer: retailer._id
			};

			if (+inputVal > checkout.getRetailer().points) {
				return
			}

			$.ajax({
				url: '/api/client/' + client.cpf + '/points/update?t=' + +(new Date()),
				data: JSON.stringify(data),
				type: 'PATCH',
				contentType: 'application/json',
				processData: false,
				dataType: 'json'
			})
				.done(function (data) {
					checkout.setClient(data);
					checkout.goToShowDotsForm();
					checkout.updateRetailer();

				})
				.fail(function () {
					checkout.showServerError()
				});

		});

		return function destroy() {
			input.unbind('focusout');
			input.off('input');
			dischargeDots.off('click');
			cancelButton.off('click');
			submitButton.off('click');
			checkoutValidate.setErrorToForm(false, hasClientForm, errorObj);
		}

	}

})();
