(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;

  checkoutProto.runForms._runChangeFinishForm = function () {
    var changeFinishForm = checkout.forms._changeFinishForm;
    var endButton = changeFinishForm.find('.end');
    var client = checkout.getClient();
    var voucher = checkout.getVoucher();

    (function setTextToForm() {
      var nameLabel = changeFinishForm.find('.title .name');
      var dotsLabel = changeFinishForm.find('.title .dots');

      nameLabel.text(client.name);
      dotsLabel.text(client.points);
    })();

    var timeout = setTimeout(function () {
      checkout.goToInitClientForm();
    }, 10000);

    endButton.on('click', function () {
      checkout.goToInitClientForm();
    });

    return function destroy(){
      clearTimeout(timeout);
      endButton.off('click');
    }

  }

})();
