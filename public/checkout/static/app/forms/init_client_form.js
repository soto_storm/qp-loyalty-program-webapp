(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;
  var checkoutUtils = checkout.utils;
  var checkoutValidate = checkout.validate;

  checkoutProto.runForms._runInitClientForm = function () {
    var initClientForm = checkout.forms._initClientForm;
    var submitButton = initClientForm.find('.submit');
    var notEnoughPointsBlock = initClientForm.find('.not-enough-points');
    var input = initClientForm.find('input[type=text]');
    var isFocusOut = false;
    var errorObj = {
      pattern: true,
      required: true
    };

    input.val('');

    initClientForm.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
        input.focus()
      });

    input.bind('focusout', function () {
      isFocusOut = true;
      input.trigger('input');
    });

    input.on('input', function () {
      var inputVal = input.val();

      var filterInputVal = checkoutUtils.convertCpf(inputVal);
      var clearInputValue = checkoutUtils.deConvertCpf(inputVal);

      input.val(filterInputVal);

      var validError = checkoutValidate.validateCpf(clearInputValue);

      if (validError.error && isFocusOut) {
        checkoutValidate.setErrorToForm(validError.error, initClientForm, errorObj);
      } else {
        checkoutValidate.setErrorToForm(false, initClientForm, errorObj);
      }

    });

    submitButton.on('click', function () {
      if (!isFocusOut) {
        isFocusOut = true
      }

      var inputVal = input.val();
      var clearInputValue = checkoutUtils.deConvertCpf(inputVal);

      var validError = checkoutValidate.validateCpf(clearInputValue);

      if (validError.error) {
        checkoutValidate.setErrorToForm(validError.error, initClientForm, errorObj);
        return;
      }

      $.ajax({
        url: "/api/client?cpf=" + clearInputValue + '&t=' + +(new Date()),
        type: 'GET',
        success: function (data) {
          if (data && data._id) {
            var client = data;
            checkout.setClient(client);

            if (client.vouchers.emitted && client.vouchers.emitted.length > 0) {
              checkout.goToHasClientForm();
            } else {
              checkout.goToNotClientForm();
            }

          } else {
            checkout.setClient({cpf: clearInputValue});
            checkout.goToNotClientForm();
          }
        },
        error: function () {
          checkout.showServerError()
        }
      });

    });

    return function destroy(){
      input.off('input');
      submitButton.off('click');
      input.unbind('focusout');
      checkoutValidate.setErrorToForm(false, initClientForm, errorObj);
    }

  }

})();
