(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;
  var checkoutValidate = checkout.validate;
  var checkoutUtils = checkout.utils;

  checkoutProto.runForms._runExchangeForm = function () {
    var exchangeForm = checkout.forms._exchangeForm;
    var client = checkout.getClient();

    var submitButton = exchangeForm.find('.submit');
    var input = exchangeForm.find('input[type=text]');
    var backButton = exchangeForm.find('.back');
    var endButton = exchangeForm.find('.end');

    var errorObj = {
      required: true,
      length: true,
      voucherError: true
    };
    var isFocusOut = false;

    var nameLabel = exchangeForm.find('.title .name');
    var dotsLabel = exchangeForm.find('.title .dots');

    input.val('');

    exchangeForm.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
      input.focus()
    });

    nameLabel.text(client.name);
    dotsLabel.text(client.points || 0);

    input.bind('focusout', function () {
      if (event.relatedTarget) {
        var element = $(event.relatedTarget);

        if (element.hasClass("back") || element.hasClass("end")) {
          return
        }
      }

      isFocusOut = true;
      input.trigger('input');
    });

    input.on('input', function () {
      var convertInput = checkoutUtils.convertVoucher(input.val());
      input.val(convertInput);

      checkoutValidate.validateVoucher(convertInput, client._id, false, function (err) {
        if (err && isFocusOut) {
          checkoutValidate.setErrorToForm(err, exchangeForm, errorObj);
        } else {
          checkoutValidate.setErrorToForm(false, exchangeForm, errorObj);
        }
      });

    });

    backButton.on('click', function () {
      checkout.goToHasClientForm();
    });
    endButton.on('click', function () {
      checkout.goToInitClientForm();
    });

    submitButton.on('click', function () {
      if (!isFocusOut) {
        isFocusOut = true
      }

      var inputVal = input.val();

      checkoutValidate.validateVoucher(inputVal, client._id, true, function (err) {
        if (err) {
          checkoutValidate.setErrorToForm(err, exchangeForm, errorObj);
          return;
        }

        var store = checkout.getStore();
        var retailer = checkout.getRetailer();

        var data = {
          points: +inputVal,
          store: store._id,
          retailer: retailer._id,
          status: 2,
          client: client._id
        };

        $.ajax({
            url: '/api/voucher/' + inputVal + '?t=' + +(new Date()),
            data: JSON.stringify(data),
            type: 'PATCH',
            contentType: 'application/json',
            processData: false,
            dataType: 'json'
          })
          .done(function (data) {
            checkout.setVoucher(data);
            checkout.goToChangeFinishForm();
          })
          .fail(function () {
            checkout.showServerError()
          });

      });

    });

    return function destroy() {
      input.unbind('focusout');
      input.off('input');
      backButton.off('click');
      endButton.off('click');
      submitButton.off('click');
      checkoutValidate.setErrorToForm(false, exchangeForm, errorObj);
    }

  }

})();
