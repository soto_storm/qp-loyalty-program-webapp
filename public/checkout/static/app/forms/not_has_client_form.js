(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;
  var checkoutValidate = checkout.validate;
  var checkoutUtils = checkout.utils;

  checkoutProto.runForms._runNotClientForm = function () {
    var notClientForm = checkout.forms._notClientForm;
    var client = checkout.getClient();

    var submitButton = notClientForm.find('.submit');
    var cancelButton = notClientForm.find('.cancel');
    var input = notClientForm.find('.form-control');

    var errorObj = {
      zero: true,
      required: true,
      invalid: true,
      lengthMax: true,
      retailerPoints: true
    };
    var moneySettings = {
      thousands: ".",
      decimal: ","
    };

    var nameLabel = notClientForm.find('.name');
    var cpfLabel = notClientForm.find('.cpf');
    var convertCpf = checkoutUtils.convertCpf(client.cpf);

    input.val('');
    input.maskMoney(moneySettings);

    notClientForm.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
      input.focus()
    });

    if (client._id && client.name) {
      nameLabel.text(client.name);
      cpfLabel.text(convertCpf);
    } else if (client._id && !client.name) {
      nameLabel.text('');
      cpfLabel.text(convertCpf);
    } else {
      nameLabel.text('É sua primeira vez!');
      cpfLabel.text(convertCpf);
    }

    var isFocusOut = false;

    input.bind('focusout', function () {
      if (event.relatedTarget) {
        var element = $(event.relatedTarget);

        if (element.hasClass("cancel")) {
          return
        }
      }

      isFocusOut = true;
      input.trigger('keyup');
    });

    cancelButton.on('click', function () {
      checkout.goToInitClientForm();
    });

    input.on('keyup', function () {
      var inputVal = input.val();
      var clearVal = input.maskMoney('unmasked')[0];

      var validError = checkoutValidate.validatePoints(inputVal);

      if (+clearVal > checkout.getRetailer().points) {
        checkoutValidate.setErrorToForm('checkout-retailer-points', notClientForm, errorObj);
        return
      }

      if (validError.error && isFocusOut) {
        checkoutValidate.setErrorToForm(validError.error, notClientForm, errorObj);
      } else {
        checkoutValidate.setErrorToForm(false, notClientForm, errorObj);
      }

    });

    submitButton.on('click', function () {
      if (!isFocusOut) {
        isFocusOut = true
      }

      var inputVal = input.maskMoney('unmasked')[0];

      var validError = checkoutValidate.validatePoints(inputVal);

      if (validError.error) {
        checkoutValidate.setErrorToForm(validError.error, notClientForm, errorObj);
        return;
      }

      var store = checkout.getStore();
      var retailer = checkout.getRetailer();

      var data = {
        points: +inputVal,
        store: store._id,
        retailer: retailer._id
      };

      if (+inputVal > checkout.getRetailer().points) {
        return
      }

      $.ajax({
          url: '/api/client/' + client.cpf + '/points/update' + '?t=' + +(new Date()),
          data: JSON.stringify(data),
          type: 'PATCH',
          contentType: 'application/json',
          processData: false,
          dataType: 'json'
        })
        .done(function (data) {
          checkout.setClient(data);
          checkout.goToShowDotsForm();
          checkout.updateRetailer();
        })
        .fail(function () {
          checkout.showServerError()
        });

    });

    return function destroy() {
      input.off('input');
      cancelButton.off('click');
      submitButton.off('click');
      input.unbind('focusout');
      checkoutValidate.setErrorToForm(false, notClientForm, errorObj);
    }

  }

})();
