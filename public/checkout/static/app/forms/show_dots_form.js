(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;
  var checkout = window._checkout;

  checkoutProto.runForms._runShowDotsForm = function () {
    var showDotsForm = checkout.forms._showDotsForm;
    var endButton = showDotsForm.find('.end');
    var client = checkout.getClient();

    (function setTextToForm() {
      showDotsForm.find('.dots').text(client.points);
    })();

    var timeout = setTimeout(function () {
      checkout.goToInitClientForm();
    }, 10000);

    endButton.on('click', function () {
      checkout.goToInitClientForm();
    });

    return function destroy(){
      clearTimeout(timeout);
      endButton.off('click');
    }

  }

})();
