(function () {
  'use strict';

  key.filter = function(event){
    var tagName = (event.target || event.srcElement).tagName;
    key.setScope(/^(INPUT|TEXTAREA|SELECT)$/.test(tagName) ? 'input' : 'other');
    return true;
  };

  var langContainer = $('.lang-container');

  key('enter', function (e) {
    e.preventDefault();
    var formContainer = $('.active');

    formContainer.find('input[type=text]').focusout();

    if (formContainer.hasClass('chm-add-dots') || formContainer.hasClass('chm-change-finish')) {
      formContainer.find('.end').click();
    } else {
      formContainer.find('.submit').click();
    }
  });

  key('shift+enter', function (e) {
    e.preventDefault();
    var formContainer = $('.active');

    formContainer.find('input[type=text]').focusout();
    formContainer.find('.exchange-dots').click();
  });

  key('shift+backspace', function (e) {
    e.preventDefault();
    var formContainer = $('.active');
    formContainer.find('input[type=text]').focusout();

    if (formContainer.hasClass('chm-change')) {
      formContainer.find('.back').click();
    } else if (formContainer.hasClass('chm-not-client') || formContainer.hasClass('chm-has-client')) {
      formContainer.find('.cancel').click();
    } else {
      formContainer.find('.end').click();
    }
  });

  key('esc', function (e) {
    e.preventDefault();
    var formContainer = $('.active');

    formContainer.find('input[type=text]').focusout();

    if (formContainer.hasClass('chm-not-client') || formContainer.hasClass('chm-has-client')) {
      formContainer.find('.cancel').click();
    } else {
      formContainer.find('.end').click();
    }
  });

  key('shift+alt+e', function () {
    langContainer.find('input[value="en"]').click();
  });

  key('shift+alt+p', function () {
    langContainer.find('input[value="pt"]').click();
  });

  key('alt+h', function () {
    var tipMassages = $('.tip > p');

    tipMassages.each(function(index, tip){
      $(tip).toggleClass('tip-visible');
    });

    $('.show-tips').toggleClass('hide');
  });

})();
