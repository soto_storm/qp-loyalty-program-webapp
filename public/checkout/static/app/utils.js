(function () {
  'use strict';

  var checkoutProto = window._checkoutConstructor.prototype;

  checkoutProto.utils = {
    convertCpf: convertCpf,
    deConvertCpf: deConvertCpf,
    convertVoucher: convertVoucher
  };

  function convertCpf(cpf) {
    return (('' + cpf || '')
      .replace(/\D/g, '')
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d{1,2})$/, '$1-$2'))
  }

  function deConvertCpf(cpf) {
    return (('' + cpf || '')
      .replace(/\D/g, '')
      .replace(/(\d{3})(\d)/, '$1$2')
      .replace(/(\d{3})(\d)/, '$1$2')
      .replace(/(\d{3})(\d{1,2})$/, '$1$2'))
  }

  function convertVoucher(val) {
    var formatVal = (('' + val || '')
      .replace(/\D/g, '')
      .replace(/(\d{3})(\d)/, '$1-$2')
      .replace(/(\d{3})(\d)/, '$1-$2'));

    var rightVal = formatVal.substr(0, 11);

    return rightVal;
  }

})();
