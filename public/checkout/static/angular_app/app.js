/**
 * Created by Jesus Soto on 10/12/2016.
 */
var app = angular.module('checkout',['ngMaterial','ui.mask','ui.utils.masks']).run(function($rootScope){
//declaring ng-show variables
  $rootScope.cpfStep = true;
  $rootScope.pontosStep = true;
  $rootScope.voucherStep = true;
  $rootScope.pointsSuccess = true;
  $rootScope.voucherSuccess = true;
  $rootScope.error = true;
  $rootScope.voucherExchangeAvialable='btn btn-blue';
  $rootScope.message='Required field';//error message showed on the points insertion form
  $rootScope.cupom = '-';
//declaration finished

  $rootScope.pontos=0;

});

app.controller('mainCtrl', function($scope,  $window, $mdToast, $http, $rootScope, pointsService,
                                    cpfService,nameService){
  $scope.points='';//variable for points validation on insertion
  $scope.cpf='';//variable for client consultation after cpf validation

  function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if(!results){
      return ''
    }
    return results[1] || 0;
  }//function to take parameters from the URL

  $http.get("/goToCheckout/getData?token=" + urlParam('token')).success(function(data){
    window._checkout.setStore(data);
    window._checkout.setRetailer(data);
  }).error(function(err){
    console.log(err);
  });

  $scope.showToast = function(message) {
    var toast = $mdToast.simple()
      .content(message)
      .action('OK')
      .highlightAction(false)
      .position('left top right');
    $mdToast.show(toast);
  };

  $scope.goToInsertPoints=function(){
    var clean_cpf = $scope.cpf.replace('.','').replace('-','').replace('.','');
    $http.get("/api/client?cpf=" + clean_cpf+ '&t=' + +(new Date())).success(function(data){
      if(data) {
        $rootScope.cpfStep=false;
        $rootScope.pontosStep=false;
        $scope.clientName = data.name;
        $rootScope.pontos = data.points;
        cpfService.setCPF(data.cpf);
        nameService.setName(data.name);
        if(data.vouchers.emitted.length===0){
          $rootScope.voucherExchangeAvialable='btn btn-gray';
        }
      }else{
        $rootScope.cpfStep=false;
        $rootScope.pontosStep=false;
        $scope.clientName = 'É sua primeira vez!';
        $rootScope.pontos=0;
        cpfService.setCPF(clean_cpf);
        nameService.setName('É sua primeira vez!');
        $rootScope.voucherExchangeAvialable='btn btn-gray';
      }
    });
  };

  //goes back to cpf input
  $scope.goBack=function(){
    $rootScope.cpfStep = true;
    $rootScope.pontosStep = true;
    $rootScope.voucherStep = true;
    $rootScope.pointsSuccess = true;
    $rootScope.voucherSuccess = true;
    $window.location.reload();
  };

  //go back to points addition
  $scope.goBack1=function(){
    $rootScope.pontosStep = false;
    $rootScope.voucherStep = true;
    $rootScope.error=false;
  };

  $scope.validatePoints=function(points){
    var decimalOnly = /^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/;
    if(points!==''){
      if(decimalOnly.test(points)){
        if(points.includes('.')){
          $rootScope.error = false;
        }else{
          $rootScope.error = false;
        }
      }else{
        $rootScope.error = true;
        $rootScope.message='Invalid Input';
        $scope.points='';

        if(points==='0.0'){
          $rootScope.message='Must be greater than 0';
        }
      }
      if(points.length>=13){
        $rootScope.error = true;
        $rootScope.message='Maxi length reached.';
      }
    }else{
      $rootScope.message='Required Field';
      $rootScope.error = true;
    }
    pointsService.setPoints(points);
  };

});
