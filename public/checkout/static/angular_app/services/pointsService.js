(function(){
  angular.module('checkout').service('pointsService',
    function(){
        var insertedPoints = '';
        return{
          getPoints: function(){
           return insertedPoints;
         },
          setPoints: function(value){
            insertedPoints = value;
          }
        };
    });
}());
