/**
 * Created by Jesus Soto on 10/13/2016.
 */
(function(){
  angular.module('checkout').service('nameService',
    function(){
      var name = '';
      return{
        getName: function(){
          return name;
        },
        setName: function(value){
          name = value;
        }
      };
    });
}());
