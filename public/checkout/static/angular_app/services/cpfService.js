/**
 * Created by Jesus Soto on 10/13/2016.
 */
(function(){
  angular.module('checkout').service('cpfService',
    function(){
      var cpf = '';
      return{
        getCPF: function(){
          return cpf;
        },
        setCPF: function(value){
          cpf = value;
        }
      };
    });
}());
