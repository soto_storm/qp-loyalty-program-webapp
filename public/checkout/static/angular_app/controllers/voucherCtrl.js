/**
 * Created by Jesus Soto on 10/13/2016.
 */
(function(){
  angular.module('checkout').controller('voucherCtrl',
    function($rootScope, $scope, $http){
      $scope.num='';
      $rootScope.error=false;

      $scope.uploadVoucher=function(){
        $scope.num=validateCupon($scope.num);
        var store = window._checkout.getStore();
        var retailer = window._checkout.getRetailer();
        var client = window._checkout.getClient();
        var info={};

        info = {
          points: client.points,
          store: store.storeId,
          retailer: store.retailerId,
          status: 2,
          client: client._id
        };

        if(info.store===undefined){
          info = {
            points: client.points,
            store: store._id,
            retailer: store.retailer,
            status: 2,
            client: client._id
          };
        }

        if(!$rootScope.error){
          $http.get( '/api/voucher/isCanBeUsed/' + $scope.num + '/' + info.client + '/' +
                      info.retailer + '?t=' + +(new Date())).success(function(data){
             if(data.value){
               $http.patch('/api/voucher/' + $scope.num + '?t=' + +(new Date()), info).success(function(result){
                 $rootScope.error=false;
                 $rootScope.voucherStep=true;
                 $rootScope.voucherSuccess=false;
                 $rootScope.cupom=$scope.num;
               }).error(function(err){
                 $rootScope.error=true;
                 $rootScope.message='Connection failure, check your internet.';
               });
             }else{
               $rootScope.error=true;
               $rootScope.message='This voucher does not exists or can not be used.';
             }
           }).error(function(err){
             $rootScope.error=true;
             $rootScope.message='Connection failure, check your internet.';
           });
         }
      };

      function validateCupon(number){
        try{
          var charArray=number.split('');
          number='';
          for(var i=0; i<=charArray.length+1; i++){
            if(i<=2) {
              number += charArray[i];
            }else if(i===3){
              number += '-';
            }else if(i<7){
              number += charArray[i-1];
            }else if(i===7){
              number += '-';
            }else{
              number += charArray[i-2];
            }
          }
          return number;
        }catch(error){
          $rootScope.error=true;
          $rootScope.message='Invalid voucher number entered.';
        }
      }
    });
}());
