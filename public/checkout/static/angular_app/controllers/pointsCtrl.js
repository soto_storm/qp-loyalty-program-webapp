/**
 * Created by Jesus Soto on 10/13/2016.
 */
(function(){
  angular.module('checkout').controller('pointsCtrl',
    function($rootScope, $scope, $http, pointsService, cpfService){
      var client = cpfService.getCPF();
      $rootScope.error=false;
      $http.get("/api/client?cpf=" + client + '&t=' + +(new Date())).success(function(data){
        window._checkout.setClient(data);
      });

        $scope.goToVoucher = function(){
          if($rootScope.voucherExchangeAvialable==='btn btn-blue'){
            $rootScope.voucherStep = false;
            $rootScope.pontosStep = true;
          }
        };

        $scope.clearError = function() {
          $rootScope.error = false;
          console.debug($scope.points);
        };

        $scope.insertPoints = function(){
          var store={};
          var retailer={};
          store=window._checkout.getStore();
          retailer=window._checkout.getRetailer();
          if($rootScope.error || $scope.points===''){
            $rootScope.error=true;
            $rootScope.message='Please insert the points to give away.';
          }else{
            var points = {
              points:parseInt($scope.points),
              store: store.storeId,
              retailer: store.retailerId
            };

            if(points.store===undefined){
              points = {
                points:parseInt($scope.points),
                store: store._id,
                retailer: store.retailer
              };
            }

            if(retailer.points===undefined){
              $http.get('/api/retailer?id='+store.retailer).success(function(result){
                retailer=result;
              });
            }

            if(parseFloat($scope.points)>parseFloat(retailer.points)) {
              $rootScope.error=true;
              $rootScope.message='Not enough points. You have '+retailer.points+' points.';
              return;
            }

            var apiUrl = '/api/client/' + cpfService.getCPF() + '/points/update?t=' + +(new Date());
            console.debug('API URL IS: \n' + apiUrl);

            $http.patch(apiUrl, points)
              .success(function(data){
                console.log(data);
                $rootScope.pointsSuccess = false;
                $rootScope.pontosStep = true;
                $rootScope.pontos= parseInt($scope.points)+parseInt($rootScope.pontos);
              }).error(function(err){
                console.error(err.message);
                $rootScope.error=true;
                $rootScope.message="You have internet issues. Checkout your connection and try again."
              });
            }
        };
    });
}());
