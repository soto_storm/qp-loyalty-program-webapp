# Quero Pontos

# How to run
- Install nodejs and npm https://nodejs.org/en/download/
- ```npm install -g gulp bower node-inspector nodemon pm2```
- Install dependencies ```npm install``` and ```npm run bower-install```(or ```npm run bower-install-win``` for windows)
- For correct working with server must run MongoDB and Redis
- Start server ```npm run start-dev```(or ```npm run start-dev-win``` for windows)
- Open http://localhost:3000
- If you want to run production version of application use ```npm run build-dist```.
After that use command ```npm run start-prod``` and ```npm run stop-prod``` for start or stop prod application.
Production version run on http://localhost:80
- - -
# Tutorial
The client application is divided into separate modules

### Client module
You can init test date in database using command ```npm run init-test-data```
(after that you can see all database in this file https://bitbucket.org/queropontos/qp/src/d258a183b1ef023d359d38675e2a8c9637dc0a6e/server/init_test_db/test_data.json?at=master&fileviewer=file-view-default).
Open http://localhost:3000/client. You must sign in(use clients from test database) or sign up.

### Checkout module
Open http://localhost:3000/checkout(this link will change in the development of retailer module).
You can create a new test user with the help of Postman.
This user will have the following fields:

	cpf: 32859770860
	name: Franklin
	email: franklinjalves@gmail.com
	points: 3500
	password: franklin

Also you can create a voucher with the help of Postman(voucher will be linked with the test user). All vouchers are cost 200 dots.
Number of this voucher you can see in Postman after creating.
Use this number for testing vouchers in Checkout module for test user(Franklin).
The test user can get to the form that allows you to enter the voucher number only if user have an active vouchers.

### Retailer module

Before testing Retailer module don't remember install npm modules and bower components(see in "How to run")

For init database use next commands:
  ```npm run create-test-data```  - for creating rest data in [test_data.json][testDataJsonLink]
  ```npm run init-test-data```  - for init new database from [test_data.json][testDataJsonLink]

Now start app using ```npm run start-dev```(or ```npm run start-dev-win``` for windows).

Login as retailer with

	email: oliver-r@mail.com
	password: oliver

Or use another retailer from [test_data.json][testDataJsonLink].
Also you can create retailer in SignUp retailer orm

[testDataJsonLink]: https://bitbucket.org/queropontos/qp/src/419cd3b62797e2dae0822a5a57fddb05488cf7a7/server/init_test_db/test_data.json?at=master&fileviewer=file-view-default
