var gulp          = require('gulp'),
    gutil         = require('gulp-util'),
    concat        = require('gulp-concat'),
    cssnano       = require('gulp-cssnano'),
    htmlreplace   = require('gulp-html-replace'),
    uglify        = require('gulp-uglify'),
    eslint        = require('gulp-eslint'),
    livereload    = require('gulp-livereload'),
    autoprefixer  = require('autoprefixer-stylus'),
    wiredep       = require('wiredep'),
    stylus        = require('gulp-stylus'),
    ngAnnotate    = require('gulp-ng-annotate'),
    bootstrap     = require('bootstrap-styl'),
    changed       = require('gulp-changed');

const WIREDEP_CONFIG = require('./wiredep');
const DEPENDENCIES = wiredep(WIREDEP_CONFIG);
const JS_DESTINY_DIR = 'dist/public/app/';
const CSS_SOURCE_DIR = 'public/assets/styles/';
const CSS_DESTINY_DIR = 'dist/public/assets/styles/';
const REV = `${new Date().getTime()}`;

gulp.task('bower', function () {
  return gulp.src('public/index.html')
    // .pipe(changed('public'))
    .pipe(wiredep.stream(WIREDEP_CONFIG))
    .pipe(gulp.dest('public'));
});

gulp.task('stylus', function () {
  var config = {
    use: [autoprefixer()]
  };
  return gulp.src('public/assets/styles/app.styl')
    .pipe(changed(CSS_SOURCE_DIR + '**/*.*'))
    .pipe(stylus(config))
    .pipe(gulp.dest(CSS_SOURCE_DIR))
    .pipe(livereload());
});

gulp.task('bootstrap', function () {
  var config = {
    use: [bootstrap(), autoprefixer()]
  };
  return gulp.src('public/assets/styles/bootstrap.styl')
    .pipe(stylus(config))
    .pipe(gulp.dest(CSS_SOURCE_DIR))
    .pipe(livereload());
});

gulp.task('htmlreplace', ['bower'], function () {
  return gulp.src('public/index.html')
    .pipe(changed('dist/public'))
    .pipe(htmlreplace({
      js: `/app/vendors.min.js?${REV}`,
      angular: `/app/app.min.js?${REV}`,
      css: `/assets/styles/app.min.css?${REV}`,
      cssVendors: `/assets/styles/vendors.min.css?${REV}`,
      livereload: ``
    }))
    .pipe(gulp.dest('dist/public'));
});

gulp.task('js:app', function () {
    return gulp.src([
      'public/app/**/*.module.js',
      'public/app/*.js',
      'public/app/**/*!(module).js'
    ])
    .pipe(changed(JS_DESTINY_DIR))
    .pipe(concat('app.min.js', {newLine: ';'}).on('error', gutil.log))
    .pipe(ngAnnotate())
    .pipe(uglify({ mangle: true }).on('error', gutil.log))
    .pipe(gulp.dest(JS_DESTINY_DIR))
    .pipe(livereload());
});

gulp.task('css', ['stylus', 'bootstrap'], function () {
  return gulp.src([
      'public/assets/styles/bootstrap.css',
      'public/assets/styles/app.css'
    ])
    .pipe(changed(CSS_DESTINY_DIR))
    .pipe(cssnano().on('error', gutil.log))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest(CSS_DESTINY_DIR));
});

gulp.task('css:vendors', function () {
  return gulp.src(DEPENDENCIES.css)
    .pipe(changed(CSS_DESTINY_DIR))
    .pipe(concat('vendors.min.css'))
    .pipe(cssnano().on('error', gutil.log))
    .pipe(gulp.dest(CSS_DESTINY_DIR));
});

gulp.task('js:angular', ['htmlreplace'], function () {
  return gulp.src(DEPENDENCIES.js)
    .pipe(changed(JS_DESTINY_DIR))
    .pipe(concat('vendors.min.js'))
    .pipe(uglify({ mangle: true }).on('error', gutil.log))
    .pipe(gulp.dest(JS_DESTINY_DIR))
    .pipe(livereload());
});

gulp.task('views', function () {
  return gulp.src([
      'public/app/**/*.html'
    ])
    .on('error', gutil.log)
    .pipe(livereload());
});

gulp.task('eslint:angular', ['htmlreplace'], function () {
  var lint = require('./eslint.json').angular;
  return gulp.src(['public/app/*.js','!node_modules/**'])
    .pipe(eslint(lint))
    .pipe(eslint.formatEach('compact', process.stderr));
});

gulp.task('eslint:nodejs', function () {
  var lint = require('./eslint.json').nodejs;
  return gulp.src(['server/*.js','!node_modules/**'])
    .pipe(eslint(lint))
    .pipe(eslint.formatEach('compact', process.stderr));
});

gulp.task('copy:build', function () {
  gulp.src('package.json').pipe(gulp.dest('dist'));
  gulp.src('node_modules/**').pipe(gulp.dest('dist/node_modules/'));
  gulp.src('server/**').pipe(gulp.dest('dist/server/'));


  gulp.src('public/app/**/*.html').pipe(gulp.dest('dist/public/app'));
  gulp.src('public/app/**/*.json').pipe(gulp.dest('dist/public/app'));
  gulp.src('public/assets/images/**').pipe(gulp.dest('dist/public/assets/images/'));
  gulp.src('public/favicon.ico').pipe(gulp.dest('dist/public/'));

  gulp.src('public/bower_components/font-awesome/fonts/**').pipe(gulp.dest('dist/public/assets/fonts/'));
  gulp.src('public/bower_components/bootstrap/fonts/**').pipe(gulp.dest('dist/public/assets/fonts/'));

});

gulp.task('watch', ['stylus', 'bootstrap'], function () {
  livereload.listen();
  gulp.watch('public/assets/styles/**/*.styl', ['stylus']);
  gulp.watch(['public/assets/styles/bootstrap.styl', 'public/assets/styles/variables.styl'], ['bootstrap']);
  gulp.watch('public/app/**/*.js', ['js:app']);
  gulp.watch('public/app/**/*.html', ['views']);
});


gulp.task('eslint', ['eslint:angular', 'eslint:nodejs']);
gulp.task('copy', ['copy:build']);
gulp.task('js', ['js:app', 'js:angular']);

gulp.task('default', ['htmlreplace', 'css', 'css:vendors', 'js', 'copy']);
